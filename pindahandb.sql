-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2016 at 09:10 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pindahandb`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fullname` varchar(250) NOT NULL,
  `is_super` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrators`
--

INSERT INTO `administrators` (`id`, `username`, `password`, `fullname`, `is_super`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'vdanny', 'asdasd', 'Vinsensius Danny', 1, '2016-12-14 18:31:43', '0000-00-00 00:00:00', NULL),
(2, 'test1', 'asdasd', 'Asep Syaifudin', 0, '2016-12-15 16:28:56', '2016-12-21 17:37:20', '2016-12-15 16:42:54'),
(3, 'test2', 'asdasd', 'Firman Kurniawan', 0, '2016-12-15 16:40:52', '2016-12-21 17:37:30', NULL),
(4, 'worker1', 'asdasd', 'Arief Muhammad', 0, '2016-12-20 18:28:20', '2016-12-21 17:36:49', NULL),
(5, 'worker2', 'asdasd', 'Brian Jonathan', 0, '2016-12-20 18:28:44', '2016-12-21 17:37:00', NULL),
(6, 'worker3', 'asdasd', 'Marco Brando', 0, '2016-12-20 18:28:59', '2016-12-21 17:37:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE IF NOT EXISTS `banks` (
  `id` int(11) unsigned NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `account_no` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `bank_name`, `account_name`, `account_no`, `created_at`, `updated_at`, `deleted_at`, `modified_by`) VALUES
(1, 'Bank BCA', 'PT. Pindahan Indonesia', '5271231231', '2016-05-11 17:00:00', '2016-05-11 17:53:59', NULL, NULL),
(2, 'Bank Mandiri', 'PT. Pindahan Indonesia', '1024123123', '2016-05-11 17:00:00', '2016-05-11 17:53:59', NULL, NULL),
(3, 'Bank BNI', 'PT. Pindahan Indonesia', '123123123', NULL, '2016-12-08 11:03:51', NULL, NULL),
(4, 'Test12', 'Test12', '123456', '2016-12-15 16:51:13', '2016-12-15 16:55:23', '2016-12-15 16:55:23', 'Vinsensius Danny');

-- --------------------------------------------------------

--
-- Table structure for table `confirmations`
--

CREATE TABLE IF NOT EXISTS `confirmations` (
  `id` int(11) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `bank_id` int(10) unsigned NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `account_name` varchar(100) NOT NULL,
  `amount` int(11) NOT NULL,
  `note` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `confirmations`
--

INSERT INTO `confirmations` (`id`, `order_id`, `bank_id`, `bank_name`, `account_name`, `amount`, `note`, `created_at`, `updated_at`, `deleted_at`, `modified_by`) VALUES
(5, 13, 1, 'BCA', 'Test', 187500, 'testtest', '2016-12-08 17:00:00', '0000-00-00 00:00:00', NULL, NULL),
(6, 6, 2, 'BRI', 'Johnny Smith', 363900, 'testtest', '2016-12-09 09:03:50', '0000-00-00 00:00:00', NULL, NULL),
(7, 21, 1, 'BCA', 'Danny', 150000, 'Tes', '2016-12-20 16:55:52', '0000-00-00 00:00:00', NULL, NULL),
(8, 19, 1, 'BCA', 'Vinsensius Danny', 232100, 'Test', '2016-12-20 19:54:53', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customs`
--

CREATE TABLE IF NOT EXISTS `customs` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `volume` decimal(10,3) DEFAULT NULL,
  `description` varchar(250) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `max_qty` int(11) NOT NULL,
  `is_vehicle` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customs`
--

INSERT INTO `customs` (`id`, `name`, `volume`, `description`, `price`, `max_qty`, `is_vehicle`, `created_at`, `updated_at`, `deleted_at`, `modified_by`) VALUES
(1, 'Mobil Box', '1.000', '', '200000', 5, 1, '2016-11-29 04:53:47', '2016-11-29 10:56:00', NULL, ''),
(2, 'Truk', '2.000', '', '300000', 3, 1, '2016-11-29 04:54:10', '2016-11-29 10:56:03', NULL, ''),
(3, 'Tenaga Kerja', NULL, '', '50000', 5, 0, '2016-11-29 04:57:18', '2016-11-29 04:57:18', NULL, ''),
(4, 'Test Edit', NULL, '', '250000', 5, 0, '2016-12-17 17:03:35', '2016-12-17 17:11:45', '2016-12-17 17:11:45', '');

-- --------------------------------------------------------

--
-- Table structure for table `extras`
--

CREATE TABLE IF NOT EXISTS `extras` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `extras`
--

INSERT INTO `extras` (`id`, `name`, `price`, `created_at`, `updated_at`, `deleted_at`, `modified_by`) VALUES
(1, 'Kardus', '10000', '2016-11-29 09:50:13', '2016-12-08 09:37:36', NULL, ''),
(2, 'Styrofoam', '10000', '2016-11-29 09:50:13', '2016-12-08 09:37:39', NULL, ''),
(3, 'Bubble Wrap', '5000', '2016-11-29 09:50:27', '2016-12-08 09:37:43', NULL, ''),
(4, 'Test', '12312', '2016-12-17 17:18:31', '2016-12-17 17:19:40', '2016-12-17 17:19:40', '');

-- --------------------------------------------------------

--
-- Table structure for table `furnitures`
--

CREATE TABLE IF NOT EXISTS `furnitures` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `furnitures`
--

INSERT INTO `furnitures` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Kulkas', '2016-11-29 10:04:05', '2016-11-29 10:04:05', NULL),
(2, 'Lemari', '2016-11-29 10:04:05', '2016-11-29 10:04:05', NULL),
(3, 'Sofa', '2016-11-29 10:04:32', '2016-11-29 10:04:32', NULL),
(4, 'Meja', '2016-11-29 10:04:32', '2016-11-29 10:04:32', NULL),
(5, 'Test123', '2016-12-15 18:02:53', '2016-12-15 18:05:45', '2016-12-15 18:05:45');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fullname` varchar(250) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `move_datetime` datetime NOT NULL,
  `distance` float NOT NULL,
  `duration` float NOT NULL,
  `start_address` varchar(500) NOT NULL,
  `end_address` varchar(500) NOT NULL,
  `start_location` varchar(500) NOT NULL,
  `end_location` varchar(500) NOT NULL,
  `package_id` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `has_reviewed` tinyint(1) NOT NULL,
  `note` varchar(250) DEFAULT NULL,
  `blueprint` varchar(250) DEFAULT NULL,
  `scheduled` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `fullname`, `phone`, `move_datetime`, `distance`, `duration`, `start_address`, `end_address`, `start_location`, `end_location`, `package_id`, `price`, `payment_status`, `has_reviewed`, `note`, `blueprint`, `scheduled`, `created_at`, `updated_at`, `deleted_at`, `modified_by`) VALUES
(6, 4, 'Vinsensius Danny', '08777123123', '2016-12-09 16:30:00', 9780, 4.5, 'Menteng Barat', 'Binus Anggrek', '(-6.194030999999999, 106.83258720000003)', '(-6.2017374, 106.78152709999995)', 0, '747800', 3, 1, NULL, NULL, NULL, '2016-12-08 10:51:07', '2016-12-20 16:12:43', NULL, ''),
(7, 4, 'Vinsensius Danny', '08777123', '2016-12-10 16:30:00', 9780, 4.5, 'menteng', 'binus', '(-6.194030999999999, 106.83258720000003)', '(-6.2017374, 106.78152709999995)', 0, '727800', 4, 0, NULL, NULL, NULL, '2016-12-08 10:52:56', '2016-12-09 08:56:29', NULL, ''),
(8, 4, 'Vinsensius Danny', '08777123', '2016-12-11 16:30:00', 9780, 4.5, 'menteng', 'binus', '(-6.194030999999999, 106.83258720000003)', '(-6.2017374, 106.78152709999995)', 0, '727800', 4, 0, NULL, NULL, NULL, '2016-12-08 11:03:02', '2016-12-09 09:01:56', NULL, ''),
(9, 4, 'Vinsensius Danny', '08777123', '2016-12-12 16:30:00', 9780, 4.5, 'menteng', 'binus', '(-6.194030999999999, 106.83258720000003)', '(-6.2017374, 106.78152709999995)', 0, '727800', 0, 0, NULL, NULL, NULL, '2016-12-08 11:03:23', '2016-12-08 17:24:44', NULL, ''),
(10, 4, 'Vinsensius Danny', '08777123', '2016-12-13 16:30:00', 9780, 4.5, 'menteng', 'binus', '(-6.194030999999999, 106.83258720000003)', '(-6.2017374, 106.78152709999995)', 0, '727800', 0, 0, NULL, NULL, NULL, '2016-12-08 11:03:58', '2016-12-08 17:24:49', NULL, ''),
(11, 4, 'Vinsensius Danny', '08777123', '2016-12-14 16:30:00', 9780, 4.5, 'menteng', 'binus', '(-6.194030999999999, 106.83258720000003)', '(-6.2017374, 106.78152709999995)', 0, '727800', 0, 0, NULL, NULL, NULL, '2016-12-08 11:04:15', '2016-12-08 17:24:53', NULL, ''),
(12, 4, 'Vinsensius Danny', '08777123', '2016-12-09 09:30:00', 4972, 4, 'CP', 'Binus', '(-6.1764969, 106.79163160000007)', '(-6.2017374, 106.78152709999995)', 3, '400000', 0, 0, NULL, NULL, NULL, '2016-12-08 16:41:58', '2016-12-09 08:24:36', NULL, ''),
(13, 4, 'Vinsensius Danny', '08777123', '2016-12-18 07:00:00', 4665, 4.5, 'Binus', 'TA', '(-6.2017374, 106.78152709999995)', '(-6.178411700000002, 106.79127199999994)', 2, '375000', 3, 1, NULL, NULL, NULL, '2016-12-08 17:57:18', '2016-12-09 09:22:21', NULL, ''),
(14, 25, 'Test3', '123123', '2016-12-29 12:30:00', 7673, 4.5, 'Grand Indonesia', 'Central Park', '(-6.195458299999999, 106.81972719999999)', '(-6.1764969, 106.79163160000007)', 1, '276730', 0, 0, NULL, NULL, NULL, '2016-12-13 17:43:39', '2016-12-19 18:10:40', NULL, ''),
(15, 25, 'Test3', '123124', '2016-12-29 20:00:00', 9478, 4, 'Puri Indah', 'Central Park', '(-6.188258999999999, 106.73372749999999)', '(-6.1764969, 106.79163160000007)', 3, '444780', 0, 0, NULL, NULL, NULL, '2016-12-13 17:47:52', '0000-00-00 00:00:00', NULL, ''),
(16, 25, 'Test3', '12123', '2016-12-30 07:00:00', 9478, 4, 'Test1 ', 'Test2', '(-6.188258999999999, 106.73372749999999)', '(-6.1764969, 106.79163160000007)', 2, '294780', 0, 0, NULL, NULL, NULL, '2016-12-13 17:51:42', '2016-12-19 18:12:05', NULL, ''),
(17, 4, 'Vinsensius Danny', '08777123', '2016-12-21 08:00:00', 8188, 4, 'Test1', 'Test2', '(-6.2017374, 106.78152709999995)', '(-6.195458299999999, 106.81972719999999)', 3, '631880', 0, 0, NULL, NULL, NULL, '2016-12-14 12:50:43', '0000-00-00 00:00:00', NULL, ''),
(18, 4, 'Vinsensius Danny', '08777123', '2017-01-04 14:00:00', 4420, 4.5, 'BINUS Kampus Anggrek, Kebon Jeruk, West Jakarta City, Special Capital Region of Jakarta, Indonesia', 'Central Park Mall, South Tanjung Duren, West Jakarta City, Special Capital Region of Jakarta, Indonesia', '(-6.2017374, 106.78152709999995)', '(-6.1764969, 106.79163160000007)', 2, '250000', 0, 0, NULL, NULL, NULL, '2016-12-19 18:57:14', '0000-00-00 00:00:00', NULL, ''),
(19, 4, 'Vinsensius Danny', '08777123', '2016-12-21 16:30:00', 4420, 4.5, 'BINUS Kampus Anggrek, Kebon Jeruk, West Jakarta City, Special Capital Region of Jakarta, Indonesia', 'Central Park Mall, South Tanjung Duren, West Jakarta City, Special Capital Region of Jakarta, Indonesia', '(-6.2017374, 106.78152709999995)', '(-6.1764969, 106.79163160000007)', 0, '464200', 2, 0, NULL, NULL, 1, '2016-12-20 16:37:24', '2016-12-20 19:57:08', NULL, ''),
(20, 4, 'Vinsensius Danny', '08777123', '2016-12-22 07:30:00', 4420, 4.5, 'BINUS Kampus Anggrek, Kebon Jeruk, West Jakarta City, Special Capital Region of Jakarta, Indonesia', 'Central Park Mall, South Tanjung Duren, West Jakarta City, Special Capital Region of Jakarta, Indonesia', '(-6.2017374, 106.78152709999995)', '(-6.1764969, 106.79163160000007)', 2, '300000', 0, 0, NULL, NULL, NULL, '2016-12-20 16:45:30', '2016-12-20 16:47:51', '2016-12-19 17:00:00', ''),
(21, 4, 'Vinsensius Danny', '08777123', '2016-12-22 07:30:00', 4420, 4.5, 'BINUS Kampus Anggrek, Kebon Jeruk, West Jakarta City, Special Capital Region of Jakarta, Indonesia', 'Central Park Mall, South Tanjung Duren, West Jakarta City, Special Capital Region of Jakarta, Indonesia', '(-6.2017374, 106.78152709999995)', '(-6.1764969, 106.79163160000007)', 2, '300000', 2, 0, 'Test', '1482252317index.png', 1, '2016-12-20 16:47:06', '2016-12-20 19:52:33', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `order_cancellations`
--

CREATE TABLE IF NOT EXISTS `order_cancellations` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reason` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_cancellations`
--

INSERT INTO `order_cancellations` (`id`, `order_id`, `reason`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, 'test batal', '2016-12-09 08:56:29', '2016-12-09 09:08:27', NULL),
(2, 8, 'Kepencet', '2016-12-09 09:01:56', '2016-12-09 09:08:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_customs`
--

CREATE TABLE IF NOT EXISTS `order_customs` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `custom_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_customs`
--

INSERT INTO `order_customs` (`id`, `order_id`, `custom_id`, `qty`, `created_at`, `updated_at`, `deleted_at`, `modified_by`) VALUES
(4, 6, 1, 1, '2016-12-08 10:51:08', '0000-00-00 00:00:00', NULL, ''),
(5, 6, 2, 1, '2016-12-08 10:51:08', '0000-00-00 00:00:00', NULL, ''),
(6, 6, 3, 2, '2016-12-08 10:51:08', '0000-00-00 00:00:00', NULL, ''),
(7, 7, 1, 1, '2016-12-08 10:52:56', '0000-00-00 00:00:00', NULL, ''),
(8, 7, 2, 1, '2016-12-08 10:52:56', '0000-00-00 00:00:00', NULL, ''),
(9, 7, 3, 2, '2016-12-08 10:52:56', '0000-00-00 00:00:00', NULL, ''),
(10, 8, 1, 1, '2016-12-08 11:03:02', '0000-00-00 00:00:00', NULL, ''),
(11, 8, 2, 1, '2016-12-08 11:03:02', '0000-00-00 00:00:00', NULL, ''),
(12, 8, 3, 2, '2016-12-08 11:03:03', '0000-00-00 00:00:00', NULL, ''),
(13, 9, 1, 1, '2016-12-08 11:03:23', '0000-00-00 00:00:00', NULL, ''),
(14, 9, 2, 1, '2016-12-08 11:03:23', '0000-00-00 00:00:00', NULL, ''),
(15, 9, 3, 2, '2016-12-08 11:03:23', '0000-00-00 00:00:00', NULL, ''),
(16, 10, 1, 1, '2016-12-08 11:03:58', '0000-00-00 00:00:00', NULL, ''),
(17, 10, 2, 1, '2016-12-08 11:03:58', '0000-00-00 00:00:00', NULL, ''),
(18, 10, 3, 2, '2016-12-08 11:03:58', '0000-00-00 00:00:00', NULL, ''),
(19, 11, 1, 1, '2016-12-08 11:04:15', '0000-00-00 00:00:00', NULL, ''),
(20, 11, 2, 1, '2016-12-08 11:04:15', '0000-00-00 00:00:00', NULL, ''),
(21, 11, 3, 2, '2016-12-08 11:04:15', '0000-00-00 00:00:00', NULL, ''),
(22, 19, 1, 1, '2016-12-20 16:37:24', '0000-00-00 00:00:00', NULL, ''),
(23, 19, 3, 2, '2016-12-20 16:37:24', '0000-00-00 00:00:00', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `order_extras`
--

CREATE TABLE IF NOT EXISTS `order_extras` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `extra_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_extras`
--

INSERT INTO `order_extras` (`id`, `order_id`, `extra_id`, `qty`, `created_at`, `updated_at`, `deleted_at`, `modified_by`) VALUES
(4, 6, 1, 3, '2016-12-08 10:51:08', '2016-12-20 16:12:43', NULL, ''),
(5, 6, 2, 1, '2016-12-08 10:51:08', '0000-00-00 00:00:00', NULL, ''),
(6, 6, 3, 2, '2016-12-08 10:51:08', '0000-00-00 00:00:00', NULL, ''),
(7, 7, 1, 1, '2016-12-08 10:52:56', '0000-00-00 00:00:00', NULL, ''),
(8, 7, 2, 1, '2016-12-08 10:52:56', '0000-00-00 00:00:00', NULL, ''),
(9, 7, 3, 2, '2016-12-08 10:52:56', '0000-00-00 00:00:00', NULL, ''),
(10, 8, 1, 1, '2016-12-08 11:03:03', '0000-00-00 00:00:00', NULL, ''),
(11, 8, 2, 1, '2016-12-08 11:03:03', '0000-00-00 00:00:00', NULL, ''),
(12, 8, 3, 2, '2016-12-08 11:03:03', '0000-00-00 00:00:00', NULL, ''),
(13, 9, 1, 1, '2016-12-08 11:03:23', '0000-00-00 00:00:00', NULL, ''),
(14, 9, 2, 1, '2016-12-08 11:03:23', '0000-00-00 00:00:00', NULL, ''),
(15, 9, 3, 2, '2016-12-08 11:03:23', '0000-00-00 00:00:00', NULL, ''),
(16, 10, 1, 1, '2016-12-08 11:03:58', '0000-00-00 00:00:00', NULL, ''),
(17, 10, 2, 1, '2016-12-08 11:03:58', '0000-00-00 00:00:00', NULL, ''),
(18, 10, 3, 2, '2016-12-08 11:03:58', '0000-00-00 00:00:00', NULL, ''),
(19, 11, 1, 1, '2016-12-08 11:04:15', '0000-00-00 00:00:00', NULL, ''),
(20, 11, 2, 1, '2016-12-08 11:04:16', '0000-00-00 00:00:00', NULL, ''),
(21, 11, 3, 2, '2016-12-08 11:04:16', '0000-00-00 00:00:00', NULL, ''),
(22, 19, 1, 5, '2016-12-20 16:37:24', '0000-00-00 00:00:00', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `order_furnitures`
--

CREATE TABLE IF NOT EXISTS `order_furnitures` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `furniture_id` int(11) NOT NULL,
  `size_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_furnitures`
--

INSERT INTO `order_furnitures` (`id`, `order_id`, `furniture_id`, `size_id`, `qty`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 1, 1, 2, NULL, '2016-12-08 10:51:08', '0000-00-00 00:00:00', NULL),
(2, 6, 3, 6, 3, NULL, '2016-12-08 10:51:08', '0000-00-00 00:00:00', NULL),
(3, 7, 1, 1, 2, NULL, '2016-12-08 10:52:56', '0000-00-00 00:00:00', NULL),
(4, 7, 3, 6, 3, NULL, '2016-12-08 10:52:56', '0000-00-00 00:00:00', NULL),
(5, 8, 1, 1, 2, NULL, '2016-12-08 11:03:03', '0000-00-00 00:00:00', NULL),
(6, 8, 3, 6, 3, NULL, '2016-12-08 11:03:03', '0000-00-00 00:00:00', NULL),
(7, 9, 1, 1, 2, NULL, '2016-12-08 11:03:23', '0000-00-00 00:00:00', NULL),
(8, 9, 3, 6, 3, NULL, '2016-12-08 11:03:23', '0000-00-00 00:00:00', NULL),
(9, 10, 1, 1, 2, NULL, '2016-12-08 11:03:58', '0000-00-00 00:00:00', NULL),
(10, 10, 3, 6, 3, NULL, '2016-12-08 11:03:58', '0000-00-00 00:00:00', NULL),
(11, 11, 1, 1, 2, NULL, '2016-12-08 11:04:16', '0000-00-00 00:00:00', NULL),
(12, 11, 3, 6, 3, NULL, '2016-12-08 11:04:16', '0000-00-00 00:00:00', NULL),
(13, 12, 1, 1, 1, NULL, '2016-12-08 16:41:58', '0000-00-00 00:00:00', NULL),
(14, 13, 1, 2, 2, NULL, '2016-12-08 17:57:19', '0000-00-00 00:00:00', NULL),
(15, 14, 2, 3, 2, NULL, '2016-12-13 17:43:39', '0000-00-00 00:00:00', NULL),
(16, 15, 4, 8, 2, NULL, '2016-12-13 17:47:52', '0000-00-00 00:00:00', NULL),
(17, 16, 3, 6, 2, NULL, '2016-12-13 17:51:42', '0000-00-00 00:00:00', NULL),
(18, 17, 1, 0, 1, '1m x 0.5m x 0.5m', '2016-12-14 12:50:43', '2016-12-19 16:17:48', NULL),
(19, 18, 2, 0, 2, '1m x 0.5m x 0.5m', '2016-12-19 18:57:14', '0000-00-00 00:00:00', NULL),
(20, 18, 3, 0, 1, '1m x 0.5m x 1m', '2016-12-19 18:57:14', '0000-00-00 00:00:00', NULL),
(21, 0, 2, 3, 3, NULL, '2016-12-19 19:01:39', '0000-00-00 00:00:00', NULL),
(22, 0, 4, 7, 3, NULL, '2016-12-19 19:01:56', '0000-00-00 00:00:00', NULL),
(23, 0, 4, 0, 3, NULL, '2016-12-19 19:02:45', '0000-00-00 00:00:00', NULL),
(24, 0, 2, 4, 3, '0.5', '2016-12-19 19:04:30', '0000-00-00 00:00:00', NULL),
(25, 16, 1, 1, 3, '0.25', '2016-12-19 19:05:47', '2016-12-19 19:14:46', '2016-12-19 19:14:46'),
(26, 19, 2, 0, 5, '1m x 0.1m x 1m', '2016-12-20 16:37:24', '0000-00-00 00:00:00', NULL),
(27, 19, 2, 0, 5, '1m x 0.5m x 0.2m', '2016-12-20 16:37:24', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `price` int(11) NOT NULL,
  `transportation_id` int(11) NOT NULL,
  `transportation` int(11) NOT NULL,
  `distance` decimal(10,0) NOT NULL,
  `employee` int(11) NOT NULL,
  `box` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `is_best_offer` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `price`, `transportation_id`, `transportation`, `distance`, `employee`, `box`, `description`, `is_best_offer`, `created_at`, `updated_at`, `deleted_at`, `modified_by`) VALUES
(1, 'Bronze', 180000, 1, 1, '5', 0, 0, 'Bronze package', 0, '2016-11-29 04:07:41', '2016-11-29 17:54:18', NULL, ''),
(2, 'Silver', 250000, 1, 1, '5', 2, 0, 'Silver Package', 0, '2016-11-29 04:07:41', '2016-11-29 17:54:21', NULL, ''),
(3, 'Gold', 400000, 1, 1, '5', 3, 5, 'Gold package', 1, '2016-11-29 04:11:20', '2016-11-29 17:54:24', NULL, ''),
(4, 'Platinum', 700000, 1, 1, '5', 5, 5, 'Platinum package', 0, '2016-11-29 04:11:20', '2016-12-17 23:45:56', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE IF NOT EXISTS `promotions` (
  `id` int(11) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `promotions`
--

INSERT INTO `promotions` (`id`, `image_path`, `created_at`, `updated_at`) VALUES
(3, '148260770941fc5c14b009ba63e643e1a34d606621.jpg', '2016-12-24 19:28:29', '0000-00-00 00:00:00'),
(4, '14826077177729cc8163fd3956840d5ef8f8edac9c.jpg', '2016-12-24 19:28:37', '0000-00-00 00:00:00'),
(5, '1482607725d294621e3d9fc90cd45ded338845ba5a.jpg', '2016-12-24 19:28:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `worker_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `order_id`, `worker_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 21, 3, '2016-12-20 19:50:27', '0000-00-00 00:00:00', NULL),
(3, 21, 5, '2016-12-20 19:50:27', '0000-00-00 00:00:00', NULL),
(4, 19, 3, '2016-12-20 19:57:08', '0000-00-00 00:00:00', NULL),
(5, 19, 4, '2016-12-20 19:57:08', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`, `deleted_at`, `modified_by`) VALUES
(1, 'working_time', '6', '2016-12-07 17:16:51', '2016-12-17 17:35:55', NULL, ''),
(3, 'transport_fee', '10000', '2016-12-08 10:09:29', '2016-12-17 17:36:01', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE IF NOT EXISTS `sizes` (
  `id` int(11) NOT NULL,
  `furniture_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `length` float NOT NULL,
  `width` float NOT NULL,
  `height` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `furniture_id`, `name`, `length`, `width`, `height`, `created_at`, `updated_at`, `deleted_at`) VALUES
(0, 0, 'Custom', 0, 0, 0, '2016-12-19 16:09:01', '2016-12-19 16:09:10', NULL),
(1, 1, 'Sedang', 0.5, 1, 0.5, '2016-11-29 10:05:50', '2016-12-17 05:30:50', NULL),
(2, 1, 'Besar', 1, 1, 0.5, '2016-11-29 10:05:50', '2016-11-29 10:05:50', NULL),
(3, 2, 'Sedang', 0.5, 1, 0.5, '2016-11-29 10:06:04', '2016-11-29 10:06:04', NULL),
(4, 2, 'Besar', 1, 1, 0.5, '2016-11-29 10:06:04', '2016-11-29 10:06:04', NULL),
(5, 3, 'Sedang', 0.5, 1, 0.5, '2016-11-29 10:06:16', '2016-11-29 10:06:16', NULL),
(6, 3, 'Besar', 1, 1, 0.5, '2016-11-29 10:06:16', '2016-11-29 10:06:16', NULL),
(7, 4, 'Sedang', 0.5, 1, 0.5, '2016-11-29 10:06:27', '2016-11-29 10:06:27', NULL),
(8, 4, 'Besar', 1, 1, 0.5, '2016-11-29 10:06:27', '2016-11-29 10:06:27', NULL),
(9, 1, 'Kecil', 0.2, 0.2, 0.5, '2016-12-15 18:48:01', '2016-12-15 18:48:01', NULL),
(10, 1, 'Test', 0.5, 0.6, 0.2, '2016-12-17 05:18:31', '2016-12-17 05:30:59', '2016-12-17 05:30:59');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE IF NOT EXISTS `testimonials` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `comment` varchar(500) NOT NULL,
  `is_show` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `order_id`, `rate`, `comment`, `is_show`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 5, 'Mantap jaya yes yes saya akan pakai lagi untuk pindahan', 1, '2016-12-09 09:20:14', '2016-12-21 18:12:51', NULL),
(2, 13, 4, 'Lumayan oke', 0, '2016-12-09 09:22:21', '2016-12-21 18:15:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(500) NOT NULL,
  `is_merchant` tinyint(1) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `email_confirm` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `fullname`, `gender`, `dob`, `phone`, `address`, `is_merchant`, `company_name`, `email_confirm`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'john@binusian.org', '4297f44b13955235245b2497399d7a93', 'John Smith', 1, '1993-06-01', '08123123', 'testing', 1, 'John Barbershop', 0, '2016-05-06 08:49:05', '2016-05-09 16:44:51', NULL),
(2, 'monica@binusian.org', '4297f44b13955235245b2497399d7a93', 'Monica Chan', 2, '1994-01-11', '12121212', 'test', 1, 'PT Breadtalk', 0, '2016-05-06 09:47:38', '2016-05-09 16:45:42', NULL),
(3, 'jchan@binusian.org', '4297f44b13955235245b2497399d7a93', 'Jackie Chan', 1, '1970-05-21', '123123123', 'Testing', 0, '', 0, '2016-05-06 09:50:55', '2016-05-18 13:06:59', '2016-05-18 13:06:59'),
(4, 'vdanny@binus.edu', 'a8f5f167f44f4964e6c998dee827110c', 'Vinsensius Danny', 1, '1994-04-19', '08777123', 'Jl K.H. Syahdan no 9', 1, 'Perusahaan Sendiri', 1, '2016-05-07 17:34:53', '2016-12-20 16:20:39', NULL),
(5, 'ferry2401@yahoo.com', '400d776e9b4d76c54a10cfb75e2b4154', 'Ferry', 1, '1999-07-28', '08978259932', 'green ville', 0, '', 1, '2016-05-12 05:02:39', '2016-09-13 07:50:37', NULL),
(6, 'klaudichin@yahoo.com', 'bdee3d4f679768d51b2b1c3d89370c52', 'Klaudiya', 2, '1995-08-23', '081287268353', 'jalan duri mas 2 blok U no 453', 0, '', 0, '2016-05-12 09:21:52', '2016-05-12 09:21:52', NULL),
(7, 'william@ifabula.com', 'fd820a2b4461bddd116c1518bc4b0f77', 'william', 1, '1993-09-21', '083807673782', 'Jalan Pal Merah barat Vi no 31F', 1, 'iFabula', 0, '2016-05-12 10:16:14', '2016-05-12 10:16:14', NULL),
(8, 'jay@kus.in', 'ccda5f3c6a71a0d5fb1b8c2f48e20700', 'gajibuta', 1, '1995-05-21', '1701306253', 'Bogor Kota Hujan', 0, '', 0, '2016-05-12 13:18:41', '2016-05-18 13:07:12', '2016-05-18 13:07:12'),
(9, 'stevanrenaldi94@gmail.com', '4297f44b13955235245b2497399d7a93', 'Stevan Renaldi Lingga', 1, '1994-05-07', '081317138130', 'Kota Wisata, Bogor', 1, 'Jillyshop', 0, '2016-05-12 16:25:03', '2016-05-22 16:59:42', '2016-05-22 16:59:42'),
(10, 'frangkyspn@yahoo.co.id', 'd41d8cd98f00b204e9800998ecf8427e', 'Frangky Edit', 1, '1993-09-09', '082124656784', 'Cibubur', 1, 'SJL', 0, '2016-05-12 16:53:38', '2016-12-15 17:53:47', NULL),
(11, 'juraganjagongentut@ogahahgila.co.id', '30150b4e7800311213c439deb0401140', 'Kusmantoro Yahud', 1, '1999-02-26', '087877552724', 'U18a 69sx', 0, '', 0, '2016-05-12 17:42:26', '2016-05-18 13:06:54', '2016-05-18 13:06:54'),
(12, 'Mohdazizbinjoharimamat@balairung.net.kr', '5b9aec960677a112cf58abc31398a3b1', 'Praveen Punjab Singh', 1, '2016-05-12', '+60113485597', '19 - 23 Lam Son Square, District 1 Ho Chi Minh City, Vietnam', 1, 'Parahyangan Padasuka Vietnam Trading Co., Ltd.', 0, '2016-05-12 21:34:04', '2016-05-18 13:06:27', '2016-05-18 13:06:27'),
(13, 'liemphoekie@koronatrading.net.vn', '6078b10b42177254a6334af1824a071c', 'Liem Phoe Kie', 1, '1973-06-12', '6765438945', '19-21 Nathan Road, Tsimshatsui, Kowloon, Hong Kong', 1, 'Indochina Satisfactory Trading Co.,Ltd.', 0, '2016-05-12 21:45:08', '2016-05-12 21:45:08', NULL),
(14, 'hanganteng@dimanaaja.com', '882cac2bd6f3291ce79fd5ee5dec970f', 'resdt', 1, '2016-05-10', '8482845849', 'Jamban belakang rumah', 1, 'jamban belang', 0, '2016-05-13 02:31:32', '2016-05-18 13:06:40', '2016-05-18 13:06:40'),
(15, 'marielseraphina_kimberley@yahoo.com', '1b8104531493293e377fffac0a56de5f', 'Mariel Seraphina', 2, '1999-07-05', '0219393939933', 'Jakarta', 0, '', 0, '2016-05-15 07:52:04', '2016-05-15 07:52:04', NULL),
(16, 'tepan@yahoo.co.id', '5bd0580107188870a8dddd4cc8995765', 'Stevan Renaldi Lingga', 1, '1994-05-07', '082299121592', 'Kota wisata salzburg', 0, '', 0, '2016-05-22 16:50:54', '2016-05-22 16:59:28', '2016-05-22 16:59:28'),
(17, 'stevan.renaldi94@gmail.com', '7803f097f9d3c63492846f202fd73171', 'stevan renaldi', 1, '1994-05-07', '081317138130', 'Jl. U no 6 Palmerah, Jakarta Barat', 0, '', 0, '2016-05-22 17:06:59', '2016-05-24 07:01:40', '2016-05-24 07:01:40'),
(18, 'pinuslingga16@gmail.com', '7803f097f9d3c63492846f202fd73171', 'Pinus Lingga', 1, '1965-11-16', '081317138130', 'Jl. U no 6 Palmerah, Jakarta Barat', 1, 'HELLO KITTY SHOP', 1, '2016-05-22 17:15:28', '2016-05-24 06:49:31', '2016-05-24 06:49:31'),
(19, 'gabriellmeidiana@gmail.com', '4b6aece8e3ede840bdc0eb2e6e17f723', 'gabriell', 1, '2016-05-28', '082123977296', 'puri beta cluster hujan mas no 18', 0, '', 0, '2016-05-24 03:05:45', '2016-05-24 03:05:45', NULL),
(20, 'stevanrenaldi94@yahoo.com', '7803f097f9d3c63492846f202fd73171', 'Stevan Renaldi', 1, '1994-05-07', '081317138130', 'Jl. U 6 no 6 Palmerah. Jakarta Barat', 0, '', 0, '2016-05-24 07:02:46', '2016-05-24 07:02:46', NULL),
(21, 'chrisinanattan@gmail.com', '3458b13d347b1240df8e669d4e03983b', 'Christina Natalia Tanuwijaya', 2, '1995-03-15', '08119593188', 'Modern Land Tangerang', 1, 'Markipul', 1, '2016-09-09 08:40:34', '2016-09-09 08:42:59', NULL),
(22, 'vdannytest@binus.edu', 'a8f5f167f44f4964e6c998dee827110c', 'Danny', 1, '2016-12-16', '', '', 0, NULL, 0, NULL, '2016-12-15 17:07:23', NULL),
(23, 'vdanny1@binus.edu', 'a8f5f167f44f4964e6c998dee827110c', 'Danny', 1, '2016-12-01', '', '', 0, NULL, 0, '2016-11-22 22:37:47', '2016-12-15 17:54:10', '2016-12-15 17:54:10'),
(24, 'test1@test.test', 'a8f5f167f44f4964e6c998dee827110c', 'Test2', 0, '2011-01-01', '', '', 0, NULL, 0, '2016-11-23 00:21:42', '2016-11-28 04:10:06', NULL),
(25, 'test3@test.test', 'a8f5f167f44f4964e6c998dee827110c', 'Test3', 1, '2011-02-01', '', '', 0, NULL, 1, '2016-11-23 00:30:35', '2016-12-13 17:27:37', NULL),
(26, 'tester1@gmail.com', 'a8f5f167f44f4964e6c998dee827110c', 'Tester 1', 1, '1994-01-01', '', '', 0, NULL, 0, '2016-11-29 21:45:42', '2016-11-30 03:45:42', NULL),
(27, 'tester2@gmail.com', 'a8f5f167f44f4964e6c998dee827110c', 'Tester 2', 1, '1994-02-01', '', '', 0, NULL, 0, '2016-11-29 21:50:10', '2016-11-30 03:50:10', NULL),
(28, 'tester3@gmail.com', 'a8f5f167f44f4964e6c998dee827110c', 'Tester 3 Test', 1, '1994-03-01', '123123', 'Testing', 0, NULL, 0, '2016-11-29 21:51:00', '2016-11-30 04:03:08', NULL),
(29, 'test4@test.com', 'a8f5f167f44f4964e6c998dee827110c', 'Test4', 1, '2001-01-08', '', '', 0, NULL, 1, '2016-12-13 17:18:55', '2016-12-13 17:19:37', NULL),
(30, 'testbe1@test.test', 'a8f5f167f44f4964e6c998dee827110c', 'Test BE 1', 1, '2016-12-01', '123123', 'Test', 0, 'asdasd', 0, '2016-12-15 17:31:22', '2016-12-15 17:37:02', NULL),
(31, 'testbe2@test.test', 'a8f5f167f44f4964e6c998dee827110c', 'Test BE 2', 1, '2016-12-05', '08123123', 'Test', 0, '', 0, '2016-12-15 17:36:10', '2016-12-15 17:37:06', NULL),
(32, 'testbe3@test.test', 'a8f5f167f44f4964e6c998dee827110c', 'Test BE 3', 1, '2016-12-12', '08123124', 'Test', 1, 'Wowow Inc', 0, '2016-12-15 17:38:14', '2016-12-15 17:38:14', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `confirmations`
--
ALTER TABLE `confirmations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `bank_id` (`bank_id`);

--
-- Indexes for table `customs`
--
ALTER TABLE `customs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `extras`
--
ALTER TABLE `extras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `furnitures`
--
ALTER TABLE `furnitures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_cancellations`
--
ALTER TABLE `order_cancellations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_customs`
--
ALTER TABLE `order_customs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_extras`
--
ALTER TABLE `order_extras`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_furnitures`
--
ALTER TABLE `order_furnitures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrators`
--
ALTER TABLE `administrators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `confirmations`
--
ALTER TABLE `confirmations`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `customs`
--
ALTER TABLE `customs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `extras`
--
ALTER TABLE `extras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `furnitures`
--
ALTER TABLE `furnitures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `order_cancellations`
--
ALTER TABLE `order_cancellations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_customs`
--
ALTER TABLE `order_customs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `order_extras`
--
ALTER TABLE `order_extras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `order_furnitures`
--
ALTER TABLE `order_furnitures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
