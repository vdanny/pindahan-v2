
/* Google Maps */

function initMap() {
    var origin_place_id = null;
    var destination_place_id = null;
    var origin_place_obj = null;
    var destination_place_obj = null;
    var travel_mode = google.maps.TravelMode.DRIVING;

    var map = new google.maps.Map(document.getElementById('map'), {
      mapTypeControl: false,
      scrollwheel: false,
      center: {lat: -33.8688, lng: 151.2195},
      zoom: 13
    });

    if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function (position) {
			initialLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			map.setCenter(initialLocation);
		});
	}

    var distanceMatrixService = new google.maps.DistanceMatrixService();
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    directionsDisplay.setMap(map);
    directionsDisplay.setOptions(
        {
            suppressMarkers: true
        }
    );

    var origin_input = document.getElementById('origin-input');
    var destination_input = document.getElementById('destination-input');
    var modes = document.getElementById('mode-selector');

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(origin_input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(destination_input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(modes);

    var origin_autocomplete = new google.maps.places.Autocomplete(origin_input);
    origin_autocomplete.bindTo('bounds', map);

    var destination_autocomplete = new google.maps.places.Autocomplete(destination_input);
    destination_autocomplete.bindTo('bounds', map);

    function expandViewportToFitPlace(map, place) {
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }
    }

    var startMarkerUrl = 'http://www.google.com/mapfiles/dd-start.png';
    var endMarkerUrl = 'http://www.google.com/mapfiles/dd-end.png';
    var markers = [];
    function makeMarker( position, icon, title ) {
        var marker = new google.maps.Marker({
            position: position,
            map: map,
            icon: icon,
            title: title
        });
        markers.push(marker);
    }

    function deleteAllMarkers()
    {
    	while(markers.length){
            markers.pop().setMap(null);
        }
    }


    origin_autocomplete.addListener('place_changed', function() {
		var place = origin_place_obj = origin_autocomplete.getPlace();
		if (!place.geometry) {
			window.alert("Autocomplete's returned place contains no geometry");
			return;
		}
        // set to hidden field
        document.getElementById("start_location").value = place.geometry.location;

		expandViewportToFitPlace(map, place);

		// If the place has a geometry, store its place ID and route if we have
		// the other place ID
		origin_place_id = place.place_id;
		route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay);
		getDistance(origin_place_obj, destination_place_obj);

		// change focus to destination-input
		$('#destination-input').focus();

        $('#start_address').val($('#origin-input').val());
    });

    destination_autocomplete.addListener('place_changed', function() {
		var place = destination_place_obj = destination_autocomplete.getPlace();
		if (!place.geometry) {
			window.alert("Autocomplete's returned place contains no geometry");
			return;
		}
        // set to hidden field
        document.getElementById("end_location").value = place.geometry.location;
        
		expandViewportToFitPlace(map, place);

		// If the place has a geometry, store its place ID and route if we have
		// the other place ID
		destination_place_id = place.place_id;
		route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay);
		getDistance(origin_place_obj, destination_place_obj);

        $('#end_address').val($('#destination-input').val());
    });

    function route(origin_place_id, destination_place_id, travel_mode, directionsService, directionsDisplay) {
		if (!origin_place_id || !destination_place_id) {
			return;
		}
		deleteAllMarkers();
		directionsService.route(
			{
				origin: {'placeId': origin_place_id},
				destination: {'placeId': destination_place_id},
				travelMode: travel_mode
			}, 
			function(response, status) {
				if (status === google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
					var leg = response.routes[ 0 ].legs[ 0 ];
					makeMarker( leg.start_location, startMarkerUrl, "Lokasi awal" );
					makeMarker( leg.end_location, endMarkerUrl, "Lokasi pindahan" );
				} else {
					window.alert('Directions request failed due to ' + status);
				}
			}
		);
    }

    function getDistance(origin_place, destination_place) {
        if(!origin_place_obj || !destination_place_obj) return;

        var origin = {lat: origin_place_obj.geometry.location.lat(), lng: origin_place_obj.geometry.location.lng()};
        var destination = {lat: destination_place_obj.geometry.location.lat(), lng: destination_place_obj.geometry.location.lng()};

        distanceMatrixService.getDistanceMatrix(
            {
                origins: [origin],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                avoidHighways: false,
                avoidTolls: true
            },
            getDistance_callback
        );
    }

    function getDistance_callback(response, status) {
        if(status == "OK"){
            console.log(response.rows[0].elements[0].distance);
            $('#distance-approx').html("Jarak yang akan ditempuh adalah " + response.rows[0].elements[0].distance.text);
            $('#distance').val(response.rows[0].elements[0].distance.value);
            // count distancePrice
			var distance = parseInt($('#distance').val());
			var distancePrice = evaluateDistancePrice(distance, 0);
			setTransportationPrice(distancePrice);
        }
        else {
            alert('status: ' + status);
        }
    }
}		