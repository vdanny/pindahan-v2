function countApplianceRow() {
	return $('#appliance-row tr').length;
}

function checkDuplicateAppliance(appliance, size) {
	var applianceExists = false, sizeExists = false;
	$.each($('#appliance-row tr'), function(key, tr) {
		var $tr = $(tr);
		applianceExists = $tr.children()[0].innerHTML == appliance;
		sizeExists = $tr.children()[1].innerHTML == size;
		if(applianceExists && sizeExists) return false;
	});
	return !(applianceExists && sizeExists);
}

$('#btn-add').click(function(){
	if($('#appliances').val() != '' && $('#sizes').val() == '0') {
		var appliance = $("#appliances option:selected").text();
		var description = $('#l').val() +"m x " + $('#w').val() +"m x " + $('#h').val() +"m";
		var size = "Custom: " + description;
		var appliance_id = $("#appliances").val();
		var size_id = 0;
		var volume = parseFloat($('#l').val()) * parseFloat($('#w').val()) * parseFloat($('#h').val());

		$('#table-appliances').show();
		$('#no-appliances').hide();
		$('#appliance-row')
		.append('<tr><td>' + appliance + '</td><td>' + size + '</td><td>' + volume + 'm<sup>3</sup></td><td>'+
			'<input type="hidden" name="appliances_id[]" value="' + appliance_id + '"/><input type="hidden" name="sizes_id[]" value="' + size_id + '"/>'+
			'<input type="hidden" name="description[]" value="' + description + '"/>'+
			'<input type="number" name="appliances[]" value="0" data-lastvalue="0" data-volume="'+ volume +'" min="0"></input></td>'+
			'<td><a class="btn btn-default btn-delete-row" title="Hapus"><i class="fa fa-trash"></i></button></td></tr>');

		bindNumber();

		// delete appliance row
		$('.btn-delete-row').click(function() {
			$(this).closest('tr').remove();
			if(countApplianceRow() == 0){
				$('#table-appliances').hide();
				$('#no-appliances').show();
			}
		});

		$('#l').val(0);
		$('#w').val(0);
		$('#h').val(0);
		$("#v").val("0 m³");
	}
	else if($('#appliances').val() != '' && $('#sizes').val() != '') {
		var appliance = $("#appliances option:selected").text();
		var size = $("#sizes option:selected").text();
		var appliance_id = $("#appliances").val();
		var temp = $("#sizes").val().split('-');
		var size_id = temp[0];
		var volume = temp[1];

		if(checkDuplicateAppliance(appliance, size)){
			$('#table-appliances').show();
			$('#no-appliances').hide();
			$('#appliance-row')
			.append('<tr><td>' + appliance + '</td><td>' + size + '</td><td>' + volume + 'm<sup>3</sup></td><td>'+
				'<input type="hidden" name="appliances_id[]" value="' + appliance_id + '"/><input type="hidden" name="sizes_id[]" value="' + size_id + '"/>'+
				'<input type="hidden" name="description[]" value="' + volume + '"/>'+
				'<input type="number" name="appliances[]" value="0" data-lastvalue="0" data-volume="'+ volume +'" min="0"></input></td>'+
				'<td><a class="btn btn-default btn-delete-row" title="Hapus"><i class="fa fa-trash"></i></button></td></tr>');

			bindNumber();

			// delete appliance row
			$('.btn-delete-row').click(function() {
				$(this).closest('tr').remove();
				if(countApplianceRow() == 0){
					$('#table-appliances').hide();
					$('#no-appliances').show();
				}
			});
		}
		else {
			alert('Pilihan perabotan dan ukuran tersebut sudah ada.');
		}
	}
	else {
		alert('Harap pilih perabotan dan ukuran terlebih dahulu.');
	}
});

$("#sizes").change(function() {
	$sizes = $(this);
	if($sizes.val() == '0') {
		$('#custom-volume').show();
	}
	else {
		$('#custom-volume').hide();
	}
});

$(".volume-compo").change(function() {
	var l = $("#l").val();
	var w = $("#w").val();
	var h = $("#h").val();

	var volume = Math.round(l*w*h * 1000) / 1000;
	$("#v").val(volume+" m³");
});

function bindNumber(){
	$('input[type="number"]').last().change(function () {

    	var v = parseFloat($(this).data('volume'));

        if(this.value < $(this).data('lastvalue')){
			countTotalVolume(v, 'add');
		}
		else if(this.value > $(this).data('lastvalue')){
			if(countTotalVolume(v, 'sub') == false){
				$(this).val($(this).data('lastvalue'));
			}
		}

        $(this).data('lastvalue', this.value);
	}).change();
}
