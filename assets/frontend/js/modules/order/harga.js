$(document).ready(function(){
    // show hide div
    $('input[type=radio]').change(function(){
        var $this = $(this);
        if($this.val() == 'y'){
            $('#package').show();
            $('#custom').hide();
            $('#no').prop('checked', false);
        }
        else{
            $('#package').hide();
            $('#custom').show();
            $('#yes').prop('checked', false);
        }
    });

    // filling custom price
    $('.custom-qty').change(function() {
        var id = $(this).attr('data-id');
        var price = parseInt($(this).attr('data-price'));
        var qty = parseInt($(this).val());
        if((price*qty) > 0){
            $('#subtotal-custom-' + id).text('Rp ' + (price*qty).formatMoney(2, ',', '.'));
            $('#subtotal-custom-hidden-' + id).val(price*qty);
        }
        else{
            $('#subtotal-custom-' + id).text('-');
            $('#subtotal-custom-hidden-' + id).val(0);
        }
        evaluateSubTotalCustom();
        evaluateGrandTotal();
        evaluateCustomVolume();
        evaluateEstimatedTime();
    });

    // filling extra price
    $('.extra-qty').change(function() {
        var id = $(this).attr('data-id');
        var price = parseInt($(this).attr('data-price'));
        var qty = parseInt($(this).val());
        if((price*qty) > 0){
            $('#subtotal-extra-' + id).text('Rp ' + (price*qty).formatMoney(2, ',', '.'));
            $('#subtotal-extra-hidden-' + id).val(price*qty);
        }
        else{
            $('#subtotal-extra-' + id).text('-');
            $('#subtotal-extra-hidden-' + id).val(0);
        }
        evaluateSubTotalExtra();
        evaluateGrandTotal();
    });
});

function evaluateSubTotalCustom()
{
    var subtotal = 0;
    $('.subtotal-custom').each(function(id, el){
        var $row = $(el);
        if($row.val() != ""){
            subtotal += parseInt($row.val());
        }
    });
    if(subtotal > 0){
        $('#grandtotal-custom').text('Rp ' + (subtotal).formatMoney(2, ',', '.'));
        $('#grandtotal-custom-hidden').val(subtotal);
    }
    else{
        $('#grandtotal-custom').text('-');
        $('#grandtotal-custom-hidden').val(0);
    }
}

function evaluateSubTotalExtra()
{
    var subtotal = 0;
    $('.subtotal-extra').each(function(id, el){
        var $row = $(el);
        if($row.val() != ""){
            subtotal += parseInt($row.val());
        }
    });
    if(subtotal > 0){
        $('#grandtotal-extra').text('Rp ' + (subtotal).formatMoney(2, ',', '.'));
        $('#grandtotal-extra-hidden').val(subtotal);
    }
    else{
        $('#grandtotal-extra').text('-');
        $('#grandtotal-extra-hidden').val(0);
    }
}

function evaluateGrandTotal()
{
    var custom = $('#grandtotal-custom-hidden').val() == '' ? 0 : parseInt($('#grandtotal-custom-hidden').val());
    var extra = $('#grandtotal-extra-hidden').val() == '' ? 0 : parseInt($('#grandtotal-extra-hidden').val());
    var grandtotal = custom + extra;

    grandtotal = higherPrice == 1? grandtotal + (grandtotal/5): grandtotal;

    if(grandtotal > 0){
        $('.grandtotal').text('Rp ' + (grandtotal).formatMoney(2, ',', '.'));
        $('#grandtotal-hidden').val(grandtotal);
    }
    else{
        $('.grandtotal').text('-');
        $('#grandtotal-hidden').val(0);
    }
}

function evaluateCustomVolume(){
    var volume = 0;
    $('.custom-qty').each(function(id, el) {
        var $this = $(this);
        if($this.data('volume')){
            var v = parseFloat($this.data('volume')),
                qty = parseInt($this.val());
            volume += v * qty;
        }
    });
    $('#total_volume_text').text(volume);
    $('#total_volume').val(volume);
    total_volume = volume;
}

function evaluateEstimatedTime(){
    var time = parseFloat($('#working_time').val());
    $('.custom-qty').each(function(id, el) {
        var $this = $(this);
        if($this.data('volume').length == 0) {
            var qty = $this.val() == "0"? 1 : parseInt($this.val());
            var newtime = (time / 2) + (time / 2) / qty;
            $('#estimated_time_text').text(newtime);
            $('#estimated_time').val(newtime);
            return;
        }
    });
}

// selecting package to hidden field
function selectPackage(pid) {
    $('#package_id').val(pid).trigger('change');
    $('.btn-template-main').removeClass('active');
    if(pid != 0 && pid != '') {
        $('#btn-' + pid).addClass('active');
    }
}