<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Home extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');
	}

	public function index()
	{
		$packages = $this->packages->all();
		$testimonials = $this->testimonials->show_home();
		$promotions = $this->promotions->get();
		$data = array('is_landing' => 'yes', 'packages' => $packages, 'testimonials' => $testimonials, 'promotions' => $promotions);
		$this->twig->display('home/index', $data);
	}

	public function custom()
	{

		$customs = $this->customs->all();
		$data['customs'] = $customs;

		$extras = $this->extras->all();
		$data['extras'] = $extras;

		$data['working_time'] = $this->settings->get('working_time');

		$this->twig->display('home/custom', $data);
	}
}

?>