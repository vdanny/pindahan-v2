<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Size extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index($furniture_id)
	{
		$furniture = $this->furnitures->get($furniture_id);
		$sizes = $this->sizes->find(array('furniture_id' => $furniture_id));
		$this->twig->display('backend/size/list', array('furniture' => $furniture, 'sizes' => $sizes));
	}

	function create($furniture_id)
	{
		$this->twig->display('backend/size/create', array('furniture_id' => $furniture_id));
	}

	function docreate()
	{
		$data = $this->input->post();
		$furniture_id = $data['furniture_id'];

		$success = $this->sizes->create($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully added new item.');
			redirect('backend/size/index/'.$furniture_id);
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to add new item.');
			redirect('backend/size/create/'.$furniture_id);
		}
	}

	function edit($furniture_id, $id)
	{
		$size = $this->sizes->get($id);
		$this->twig->display('backend/size/edit', array('size' => $size, 'furniture_id' => $furniture_id));
	}

	function doedit()
	{
		$data = $this->input->post();

		$furniture_id = $data['furniture_id'];
		unset($data['furniture_id']);


		$success = $this->sizes->update($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/size/index/'.$furniture_id);
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/size/edit/'.$data['id']);
		}
	}

	function delete($furniture_id, $id)
	{
		$success = $this->sizes->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/size/index/'.$furniture_id);
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/size/index/'.$furniture_id);
		}
	}
}

?>