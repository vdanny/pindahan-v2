<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Furniture extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$furnitures = $this->furnitures->get();
		$this->twig->display('backend/furniture/list', array('furnitures' => $furnitures));
	}

	function create()
	{
		$this->twig->display('backend/furniture/create');
	}

	function docreate()
	{
		$data = $this->input->post();
		$success = $this->furnitures->create($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully added new item.');
			redirect('backend/furniture');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to add new item.');
			redirect('backend/furniture/create');
		}
	}

	function edit($id)
	{
		$furniture = $this->furnitures->get($id);
		$this->twig->display('backend/furniture/edit', array('furniture' => $furniture));
	}

	function doedit()
	{
		$data = $this->input->post();
		$success = $this->furnitures->update($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/furniture');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/furniture/edit');
		}
	}

	function delete($id)
	{
		$success = $this->furnitures->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/furniture');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/furniture');
		}
	}
}

?>