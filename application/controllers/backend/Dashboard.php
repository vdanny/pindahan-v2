<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Dashboard extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$this->twig->display('backend/dashboard');
	}
}

?>