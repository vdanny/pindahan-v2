<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');
	}

	function index()
	{
		$this->twig->display('backend/login');
	}

	function dologin()
	{
		$admin = $this->administrators->login($this->input->post('username'), $this->input->post('password'));
		if($admin)
		{
			$this->session->set_userdata('admin', $admin);
			redirect('backend/order');
		}
		else
		{
			$this->session->set_flashdata('msg', 'Your authentication is invalid!');
			redirect('backend/login');
		}
	}
}

?>