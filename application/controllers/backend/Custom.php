<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Custom extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$customs = $this->customs->all();
		$this->twig->display('backend/custom/list', array('customs' => $customs));
	}

	function create()
	{
		$this->twig->display('backend/custom/create');
	}

	function docreate()
	{
		$data = $this->input->post();
		$data['is_vehicle'] = isset($data['is_vehicle'])? $data['is_vehicle'] : 0;
		if($data['is_vehicle'] == 0) $data['volume'] = null;
		$success = $this->customs->create($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully added new item.');
			redirect('backend/custom');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to add new item.');
			redirect('backend/custom/create');
		}
	}

	function edit($id)
	{
		$custom = $this->customs->get($id);
		$this->twig->display('backend/custom/edit', array('custom' => $custom));
	}

	function doedit()
	{
		$data = $this->input->post();
		$data['is_vehicle'] = isset($data['is_vehicle'])? $data['is_vehicle'] : 0;
		if($data['is_vehicle'] == 0) $data['volume'] = null;
		$success = $this->customs->update($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/custom');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/custom/edit');
		}
	}

	function delete($id)
	{
		$success = $this->customs->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/custom');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/custom');
		}
	}
}

?>