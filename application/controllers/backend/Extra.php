<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Extra extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$extras = $this->extras->get();
		$this->twig->display('backend/extra/list', array('extras' => $extras));
	}

	function create()
	{
		$this->twig->display('backend/extra/create');
	}

	function docreate()
	{
		$data = $this->input->post();
		$success = $this->extras->create($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully added new item.');
			redirect('backend/extra');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to add new item.');
			redirect('backend/extra/create');
		}
	}

	function edit($id)
	{
		$extra = $this->extras->get($id);
		$this->twig->display('backend/extra/edit', array('extra' => $extra));
	}

	function doedit()
	{
		$data = $this->input->post();
		$success = $this->extras->update($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/extra');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/extra/edit');
		}
	}

	function delete($id)
	{
		$success = $this->extras->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/extra');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/extra');
		}
	}
}

?>