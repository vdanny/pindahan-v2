<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Admin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$admins = $this->administrators->get();
		$this->twig->display('backend/admin/list', array('admins' => $admins));
	}

	function create()
	{
		$this->twig->display('backend/admin/create');
	}

	function docreate()
	{
		$data = $this->input->post();
		$success = $this->administrators->create($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully added new item.');
			redirect('backend/admin');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to add new item.');
			redirect('backend/admin/create');
		}
	}

	function edit($id)
	{
		$admin = $this->administrators->get($id);
		$this->twig->display('backend/admin/edit', array('admin' => $admin));
	}

	function doedit()
	{
		$data = $this->input->post();
		$success = $this->administrators->update($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/admin');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/admin/edit');
		}
	}

	function delete($id)
	{
		$success = $this->administrators->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/admin');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/admin');
		}
	}
}

?>