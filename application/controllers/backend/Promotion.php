<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Promotion
*/
class Promotion extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$promotions = $this->promotions->get();
		$this->twig->display('backend/promotion/list', array('promotions' => $promotions));
	}

	function create()
	{
		$this->twig->display('backend/promotion/create');
	}

	function docreate()
	{
		$data = $this->input->post();

		if(isset($_FILES["promoimg"]) && $_FILES["promoimg"]['size'] > 0)
		{
			$new_name = time().$_FILES["promoimg"]['name'];
			$config['file_name']      = $new_name;
			$config['upload_path']    = 'uploads/promo';
            $config['allowed_types']  = 'gif|jpg|png';
            $config['max_size']       = 5*1024;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('promoimg'))
            {
                print $this->upload->display_errors('','');
                die;
            }
            else
            {
                $order['promoimg'] = $this->upload->data();
            }
		}
		else{
			$this->session->set_flashdata('error', 'No file selected.');
			redirect('backend/promotion/create');	
		}

		$success = $this->promotions->create(array('image_path' => $new_name));
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully added new item.');
			redirect('backend/promotion');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to add new item.');
			redirect('backend/promotion/create');
		}
	}

	function edit($id)
	{
		$promotion = $this->promotions->get($id);
		$this->twig->display('backend/promotion/edit', array('promotion' => $promotion));
	}

	function doedit()
	{
		$data = $this->input->post();
		$success = $this->promotions->update($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/promotion');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/promotion/edit');
		}
	}

	function delete($id)
	{
		$success = $this->promotions->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/promotion');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/promotion');
		}
	}
}

?>