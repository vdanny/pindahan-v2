<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Package extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$packages = $this->packages->all();
		$this->twig->display('backend/package/list', array('packages' => $packages));
	}

	function create()
	{
		$vehicles = $this->vehicles->find(0);
		$this->twig->display('backend/package/create', array('vehicles' => $vehicles));
	}

	function docreate()
	{
		$data = $this->input->post();
		$data['is_best_offer'] = isset($data['is_best_offer'])? $data['is_best_offer'] : 0;
		$success = $this->packages->create($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully added new item.');
			redirect('backend/package');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to add new item.');
			redirect('backend/package/create');
		}
	}

	function edit($id)
	{
		$package = $this->packages->find(array('id' => $id))[0];
		$vehicles = $this->vehicles->find(0);
		$this->twig->display('backend/package/edit', array('package' => $package, 'vehicles' => $vehicles));
	}

	function doedit()
	{
		$data = $this->input->post();
		$data['is_best_offer'] = isset($data['is_best_offer'])? $data['is_best_offer'] : 0;
		$success = $this->packages->update($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/package');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/package/edit');
		}
	}

	function delete($id)
	{
		$success = $this->packages->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/package');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/package');
		}
	}
}

?>