<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Setting extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$settings = $this->settings->all();
		$this->twig->display('backend/setting/list', array('settings' => $settings));
	}

	function doedit()
	{
		$data = $this->input->post();
		foreach ($data as $key => $value) 
		{
			$this->settings->update($key, $value);
		}
		$this->session->set_flashdata('msg', 'Successfully updated settings.');
		redirect('backend/setting');
	}

}

?>