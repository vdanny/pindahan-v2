<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Member extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$members = $this->users->get();
		$this->twig->display('backend/member/list', array('members' => $members));
	}

	function create()
	{
		$this->twig->display('backend/member/create');
	}

	function docreate()
	{
		$data = $this->input->post();
		$success = $this->users->create($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully added new item.');
			redirect('backend/member');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to add new item.');
			redirect('backend/member/create');
		}
	}

	function edit($id)
	{
		$member = $this->users->get($id);
		$this->twig->display('backend/member/edit', array('member' => $member));
	}

	function doedit()
	{
		$data = $this->input->post();
		$success = $this->users->update_be($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/member');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/member/edit');
		}
	}

	function delete($id)
	{
		$success = $this->users->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/member');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/member');
		}
	}
}

?>