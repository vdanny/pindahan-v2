<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Bank extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$banks = $this->banks->get();
		$this->twig->display('backend/bank/list', array('banks' => $banks));
	}

	function create()
	{
		$this->twig->display('backend/bank/create');
	}

	function docreate()
	{
		$data = $this->input->post();
		$success = $this->banks->create($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully added new item.');
			redirect('backend/bank');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to add new item.');
			redirect('backend/bank/create');
		}
	}

	function edit($id)
	{
		$bank = $this->banks->get($id);
		$this->twig->display('backend/bank/edit', array('bank' => $bank));
	}

	function doedit()
	{
		$data = $this->input->post();
		$success = $this->banks->update($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/bank');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/bank/edit');
		}
	}

	function delete($id)
	{
		$success = $this->banks->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/bank');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/bank');
		}
	}
}

?>