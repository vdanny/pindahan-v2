<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Home
*/
class Order extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$orders = $this->orders->get();
		foreach($orders as $order){
			if($order->package_id != 0){
				$order->package = $this->packages->find(array('id' => $order->package_id))[0];
			}
			if($order->payment_status == 4){
				$order->cancellation = $this->cancellations->get($order->id);
			}
		}
		$this->twig->display('backend/order/list', array('orders' => $orders));
	}

	function detail($order_id)
	{
		// get order
		$order = $this->orders->find_id($order_id)[0];
		if($order->package_id == 0){
			$order->customs = $this->orders->get_customs($order->id);
			$order->extras = $this->orders->get_extras($order->id);

			$order->total_volume = 0;
			foreach ($order->customs as $c) {
				$v = $this->vehicles->find($c->custom_id);
				if($v) $order->total_volume += $v->volume;
			}
		}
		else{
			$order->package = $this->packages->find(array("id" => $order->package_id))[0];
			$order->total_volume = $this->vehicles->find($order->package->transportation_id)->volume;
		}
		$order->furnitures = $this->orders->get_furnitures($order->id);

		// get transport_fee
		$transport_fee = $this->settings->get('transport_fee');


		$data = array(
			"order"         => $order,
			"transport_fee" => $transport_fee
		);
		$this->twig->display('backend/order/detail', $data);
	}

	function edit_general($id)
	{
		$order = $this->orders->get($id);
		$packages = $this->packages->all();
		$this->twig->display('backend/order/edit_general', array("order" => $order, "packages" => $packages));

	}

	function doeditgeneral()
	{
		$data = $this->input->post();
		$data['move_datetime'] = date('Y-m-d H:i:s', strtotime($data['move_datetime']));

		// update price
		if(isset($data['package_id'])){
			$order = $this->orders->get($data['id']);
			$old_package = $this->packages->find(array('id' => $order->package_id))[0];
			$new_package = $this->packages->find(array('id' => $data['package_id']))[0];
			$data['price'] = $order->price - $old_package->price + $new_package->price;
		}

		$success = $this->orders->update($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/order');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/order/edit_general' . $data['id']);
		}
	}

	function edit_furniture($id)
	{
		$furnitures = $this->furnitures->all();
		$orderfurnitures = $this->orders->get_furnitures($id);
		$this->twig->display('backend/order/edit_furniture', array('order_id' => $id, 'furnitures' => $furnitures, 'orderfurnitures' => $orderfurnitures));
	}

	function doeditfurniture()
	{
		$data = $this->input->post();
		$temp = explode('-', $data['size_id']);
		$data['size_id'] = $temp[0];
		$data['description'] = $temp[1];
		$success = $this->orders->create_furniture($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/order/edit_furniture/'.$data['order_id']);
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/order/edit_furniture/'.$data['order_id']);
		}
	}

	function deletefurniture($o, $f)
	{
		$success = $this->orders->delete_furniture($o,$f);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted data.');
			redirect('backend/order/edit_furniture/'.$o);
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete data.');
			redirect('backend/order/edit_furniture/'.$o);	redirect('backend/furniture/edit');
		}	
	}

	function edit_custom($id)
	{
		$ordercustoms = $this->orders->get_customs($id);
		$orderextras = $this->orders->get_extras($id);
		$customs = $this->customs->all();
		$extras = $this->extras->all();

		$data = array(
			'order_id' => $id, 
			'customs' => $customs, 
			'extras' => $extras,
			'ordercustoms' => $ordercustoms,
			'orderextras' => $orderextras
		);
		$this->twig->display('backend/order/edit_custom', $data);
	}

	function doeditcustom()
	{
		$data = $this->input->post();
		// get order distance
		$order = $this->orders->get($data['order_id']);
		// get transport_fee
		$transport_fee = $this->settings->get('transport_fee');

		$price = $data['grandtotal'] + ($order->distance/1000 *$transport_fee);
		$dataorder = array('id' => $order->id, 'price' => $price);
		$this->orders->update($dataorder);

		for ($i=0; $i < count($data['custom_id']); $i++) { 
			$this->orders->update_custom($data['order_id'], $data['custom_id'][$i], $data['custom_qty'][$i]);
		}

		for ($i=0; $i < count($data['extra_id']); $i++) { 
			$this->orders->update_extra($data['order_id'], $data['extra_id'][$i], $data['extra_qty'][$i]);
		}

		$this->session->set_flashdata('msg', 'Successfully updated data.');
		redirect('backend/order/edit_custom/'.$data['order_id']);
	}

	function confirmation($id)
	{
		$confirm = $this->confirmations->get($id);
		$bank = $this->banks->get($confirm->bank_id);
		$this->twig->display('backend/order/confirmation', array('order_id' => $id, 'confirm' => $confirm, 'bank' => $bank));
	}

	function doconfirm()
	{
		$data = $this->input->post();
		$d['id'] = $data['order_id'];
		$d['payment_status'] = 2;
		$success = $this->orders->update($d);
		if($success)
		{
			$order = $this->orders->get($d['id']);
			$user = $this->users->get($order->user_id);
			$this->sendMail($user->email);
			$this->session->set_flashdata('msg', 'Successfully confirmed payment.');
			redirect('backend/order/');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to confirm payment.');
			redirect('backend/order/');
		}
	}

	function print_dp($id)
	{
		$order = $this->orders->get($id);
		$user = $this->users->get($order->user_id);
		$name = $user->fullname;
		$dp = $order->price/2;
		$terbilang = $this->terbilang($dp);

		$data = array(
			'name'      => strtoupper($name),
			'dp'        => $dp,
			'terbilang' => strtoupper($terbilang)
		);
		$this->twig->display('backend/print/dp', $data);
	}

	function print_complete($id)
	{
		$order = $this->orders->get($id);
		$user = $this->users->get($order->user_id);
		$name = $user->fullname;
		$price = $order->price;
		$terbilang = $this->terbilang($price);

		$data = array(
			'name'      => strtoupper($name),
			'price'     => $price,
			'terbilang' => strtoupper($terbilang)
		);
		$this->twig->display('backend/print/complete', $data);
	}

	function print_spk($id)
	{
		$workers = $this->orders->get_workers($id);

		$data = array(
			'order_id' => $id,
			'workers'  => $workers
		);
		$this->twig->display('backend/print/spk', $data);
	}

	function schedule($id)
	{
		$order = $this->orders->get($id);
		$start = date('Y-m-d H:i:s', strtotime($order->move_datetime));
		$end = date('Y-m-d H:i:s', strtotime($order->move_datetime) + 3600*6);
		$workers = $this->administrators->get_available_worker($start, $end);
		$worker_amount = $this->orders->get_worker_amount($order->id, $order->package_id);

		$data = array(
			'order' => $order, 
			'workers' => $workers,
			'start' => $start,
			'end' => $end,
			'worker_amount' => $worker_amount
		);
		$this->twig->display('backend/order/schedule', $data);
	}

	function saveschedule()
	{
		$data = $this->input->post();
		$d['order_id'] = $data['order_id'];
		for ($i=0; $i < count($data['worker_id']); $i++) { 
			$d['worker_id'] = $data['worker_id'][$i];
			$this->orders->create_schedule($d);
		}
		$this->orders->update(array('id' => $data['order_id'], 'scheduled' => 1));

		$this->session->set_flashdata('msg', 'Successfully saved schedule.');
		redirect('backend/order');
	}

	function delete($id)
	{
		$success = $this->orders->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/order');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/order');
		}
	}

	function finished($id)
	{
		$this->orders->update_status($id, 3);
		$this->session->set_flashdata('msg', 'Successfully finished order.');
		redirect('backend/order');
	}

	/**
	 * Send email confirmation
	 * @param  $email_to
	 * @return boolean         
	 */
	public function sendMail($email_to)
	{
		$config = Array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'cs.pindahan@gmail.com',
			'smtp_pass' => 'pindahan123',
			'mailtype'  => 'html',
			'charset'   => 'iso-8859-1',
			'wordwrap'  => TRUE
		);

		$message = read_file('./application/views/emails/confirmsuccess.html');

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('cs.pindahan@gmail.com'); // change it to yours
		// $this->email->to('vinsensius.danny@gmail.com'); // debugging purpose
		$this->email->to($email_to);
		$this->email->subject('Pindahan - Konfirmasi DP Berhasil');
		$this->email->message($message);

		if($this->email->send())
		{
			return TRUE;
		}
		else
		{
			echo $this->email->print_debugger();
			return FALSE;
		}

	}

	function terbilang($x)
	{
	  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	  if ($x < 12)
	    return " " . $abil[$x];
	  elseif ($x < 20)
	    return $this->terbilang($x - 10) . "belas";
	  elseif ($x < 100)
	    return $this->terbilang($x / 10) . " puluh" . $this->terbilang($x % 10);
	  elseif ($x < 200)
	    return " seratus" . $this->terbilang($x - 100);
	  elseif ($x < 1000)
	    return $this->terbilang($x / 100) . " ratus" . $this->terbilang($x % 100);
	  elseif ($x < 2000)
	    return " seribu" . $this->terbilang($x - 1000);
	  elseif ($x < 1000000)
	    return $this->terbilang($x / 1000) . " ribu" . $this->terbilang($x % 1000);
	  elseif ($x < 1000000000)
	    return $this->terbilang($x / 1000000) . " juta" . $this->terbilang($x % 1000000);
	}
}

?>