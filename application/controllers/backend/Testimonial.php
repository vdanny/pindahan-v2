<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Testimonial
*/
class Testimonial extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('bower_url');

		if($this->session->has_userdata('admin') == false) redirect('backend/login');
	}

	function index()
	{
		$testimonials = $this->testimonials->get();
		$this->twig->display('backend/testimonial/list', array('testimonials' => $testimonials));
	}

	function edit($id)
	{
		$testimonial = $this->testimonials->get($id);
		$this->twig->display('backend/testimonial/edit', array('testimonial' => $testimonial));
	}

	function doedit()
	{
		$data = $this->input->post();
		$success = $this->testimonials->update($data);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully updated data.');
			redirect('backend/testimonial');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to update data.');
			redirect('backend/testimonial/edit'.$data['id']);
		}
	}

	function delete($id)
	{
		$success = $this->testimonials->delete($id);
		if($success)
		{
			$this->session->set_flashdata('msg', 'Successfully deleted item.');
			redirect('backend/testimonial');
		}
		else
		{
			$this->session->set_flashdata('error', 'Failed to delete item.');
			redirect('backend/testimonial');
		}
	}
}

?>