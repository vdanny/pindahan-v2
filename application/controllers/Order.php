<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for order
*/
class Order extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if(!$this->session->has_userdata('user')) {
			$this->session->set_flashdata('msg', 'Sesi anda telah berakhir, harap login ulang.');
			redirect('account/login');
		}
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('site_url');
		$this->twig->add_function('bower_url');
	}

	public function order_list()
	{
		$this->session->unset_userdata('order');
		$orders = $this->orders->find_userid($this->session->userdata('user')->id, 'created_at DESC');
		$data = array("title_page" => "Pesanan saya", "has_menu" => "yes", "orders" => $orders);
		$this->twig->display('order/order_list', $data);
	}

	public function new_order()
	{
		$packages = $this->packages->all();
		$data['packages'] = $packages;

		$customs = $this->customs->all();
		$data['customs'] = $customs;

		$extras = $this->extras->all();
		$data['extras'] = $extras;

		$furnitures = $this->furnitures->all();
		$data['furnitures'] = $furnitures;
		$data['total_volume'] = 1.000;
		$data['working_time'] = $this->settings->get('working_time');

		$this->twig->display('order/new_order/order1', $data);
	}

	public function summary()
	{
		$working_time = $this->settings->get('working_time');
		$order = count($this->input->post()) == 0? $this->session->userdata('order') : $this->input->post();
		// print_r($order); die;
		// save file if exists
		if(isset($_FILES["blueprint"]) && $_FILES["blueprint"]['size'] > 0)
		{
			$new_name = time().$_FILES["blueprint"]['name'];
			$config['file_name']      = $new_name;
			$config['upload_path']    = 'uploads/temp';
            $config['allowed_types']  = 'gif|jpg|png';
            $config['max_size']       = 5*1024;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('blueprint'))
            {
                // $this->session->set_flashdata('msg', $this->upload->display_errors('',''));
                // redirect('order/new_order');
                print $this->upload->display_errors('','');
                die;
            }
            else
            {
                $order['blueprint'] = $this->upload->data();
            }
		}
		
		if($order['package_id'] == 0){
			$data['customs'] = $this->customs->find_id($order['custom_id']);
			$data['extras'] = $this->extras->find_id($order['extra_id']);
		}

		// show data for summary
		$data['order'] = $order;

		// if added furniture
		if(isset($data['order']['appliances_id'])){
			$data['furnitures'] = $this->furnitures->find_id($data['order']['appliances_id']);
			$data['sizes'] = $this->sizes->find_id($data['order']['sizes_id']);
			$data['descriptions'] = $data['order']['description'];
		}

		$data['transport_fee'] = $this->settings->get('transport_fee');

		// get package object
		if($data['order']['package_id'] != 0){
			$data['order']['package'] = $this->packages->find(array('id' => $data['order']['package_id']))[0];

			// pay more if order is rushing
			$order['price'] = $data['order']['price'] = $order['higherPrice'] == 1? $data['order']['package']->price + ($data['order']['package']->price/5) : $data['order']['package']->price;

			// count estimated time
			$emp = $data['order']['package']->employee;
			$emp = $emp > 0 ? $emp: 1;
			$order['duration'] = $data['order']['duration'] = ($working_time/2) + ($working_time/2/$emp);
		}

		$data['higherPrice'] = $order['higherPrice'];
		$this->session->set_userdata('order', $order);
		$this->twig->display('order/new_order/summary', $data);
	}

	public function checkout()
	{
		$order = $this->session->userdata('order');
		$transport_fee = $this->settings->get('transport_fee');

		// count and set grandtotal
		if($order['package_id'] == 0) {
			$grandtotal = $order['price'] + ($order['distance'] / 1000 * $transport_fee);
			$order['price'] = $grandtotal;
		}
		else{
			$package = $this->packages->find(array('id' => $order['package_id']))[0];
			$package_dist = $package->distance * 1000;
			$paid_transport = $order['distance'] > $package_dist ? (($order['distance'] - $package_dist) / 1000 * $transport_fee) : 0;

			$grandtotal = $order['price'] + $paid_transport;
			$order['price'] = $grandtotal;
		}

		// load banks
		$banks = $this->banks->all();

		// save order
		if(!isset($order['id'])){
			$order['id'] = $this->orders->save($order);
			$order['created_at'] = date('Y-m-d H:i:s');
			$this->session->set_userdata('order', $order);

			// send email dp reminder
			$user = $this->session->userdata('user');
			$this->sendMail($user->email, $user, $order, $banks);
		}

		// set max_pay_date
		$max_pay_date = new DateTime($order['created_at']);
		$max_pay_date->modify('+1 day');


		$data = array(
			"title_page"   => "Pembayaran", 
			"dp"           => $order['price']/2, 
			"max_pay_date" => $max_pay_date->format('H:i - j F Y'),
			"banks"        => $banks,
			"order_id"     => $order['id']
		);
		$this->twig->display('order/new_order/checkout', $data);
	}

	public function new_order1($tab = 'umum')
	{
		if($this->input->method() == 'post'){
			redirect('order/new_order/' . $tab);
		}
		$data = array("title_page" => "Pesan pindahan");
		switch($tab)
		{
			case 'umum':
				$this->twig->display('order/new_order/umum', $data);
				break;
			case 'lokasi':
				$this->twig->display('order/new_order/lokasi', $data);
				break;
			case 'harga':
				$packages = $this->packages->all();
				$customs = $this->customs->all();
				$extras = $this->extras->all();
				$data['packages'] = $packages;
				$data['customs'] = $customs;
				$data['extras'] = $extras;
				$this->twig->display('order/new_order/harga', $data);
				break;
			case 'furnitur':
				$furnitures = $this->furnitures->all();
				$data['furnitures'] = $furnitures;
				$data['total_volume'] = $this->session->userdata('order')['total_volume'];
				$this->twig->display('order/new_order/furnitur', $data);
				break;
			case 'ringkasan':
				//print_r($this->session->userdata('order'));die;
				$data['order'] = $this->session->userdata('order');
				$data['furnitures'] = $this->furnitures->find_id($data['order']['appliances_id']);
				$data['sizes'] = $this->sizes->find_id($data['order']['sizes_id']);

				//print_r($data);die;
				$this->twig->display('order/new_order/ringkasan', $data);
				break;
		}
	}

	public function nextstep($nextpage)
	{
		if($this->session->has_userdata("order")){
			$order = $this->session->userdata("order");
			$order = array_merge($order, $this->input->post());
		}
		else{
			$order = array_merge(array(), $this->input->post());
		}

		if($nextpage == 'lokasi' && $_FILES["blueprint"]['size'] > 0)
		{
			$new_name = time().$_FILES["blueprint"]['name'];
			$config['file_name']      = $new_name;
			$config['upload_path']    = 'uploads/temp';
            $config['allowed_types']  = 'gif|jpg|png';
            $config['max_size']       = 100;
            $config['max_width']      = 1024;
            $config['max_height']     = 768;

            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('blueprint'))
            {
                $this->session->set_flashdata('msg', $this->upload->display_errors('',''));
                redirect('order/new_order');
            }
            else
            {
                $order['blueprint'] = $this->upload->data();
            }
		}
		else if($nextpage == 'furnitur')
		{
			if($order['package_id'] == 0)
			{
				$volume = 0;
				$order_custom = $order['custom'];
				$customs = $this->customs->all('id, name, volume');
				foreach($customs as $i => $custom){
					$volume += $custom->volume * $order_custom[$i];
				}
				$order['total_volume'] = $volume;
			}
			else
			{
				$package = $this->packages->find(array('id' => $order['package_id']));
				$order['package'] = $package;
				$vehicle = $this->vehicles->find($package->transportation_id, '');
				$order['total_volume'] = $vehicle[0]->volume;
			}
		}

		$order['step'] = $nextpage;
		//print_r($order); die;
		$this->session->set_userdata("order", $order);
		redirect('order/new_order/' . $nextpage);
	}

	public function confirmation($order_id)
	{
		// get order
		$order = $this->orders->find_id($order_id)[0];
		// get dp
		$dp = $order->price/2;
		// get max pay date
		$max_pay_date = new DateTime($order->created_at);
		$max_pay_date->modify('+1 day');

		// load banks
		$banks = $this->banks->all();

		$data = array("order_id" => $order->id, "dp" => $dp, "max_pay_date" => $max_pay_date->format('H:i - j F Y'), "banks" => $banks);
		$this->twig->display('order/new_order/confirmation', $data);
	}

	public function do_confirm($order_id)
	{
		$data = $this->input->post();
		$data['order_id'] = $order_id;
		$this->confirmations->save($data);
		$this->orders->update_status($order_id, 1);
		redirect('order/success_confirm');
	}

	public function success_confirm()
	{
		$data = array();
		$this->twig->display('order/new_order/success_confirm', $data);
	}

	public function detail($order_id)
	{
		// get order
		$order = $this->orders->find_id($order_id)[0];
		if($order->package_id == 0){
			$order->customs = $this->orders->get_customs($order->id);
			$order->extras = $this->orders->get_extras($order->id);

			$order->total_volume = 0;
			foreach ($order->customs as $c) {
				$v = $this->vehicles->find($c->custom_id);
				if($v) $order->total_volume += $v->volume;
			}
		}
		else{
			$order->package = $this->packages->find(array("id" => $order->package_id))[0];
			$order->total_volume = $this->vehicles->find($order->package->transportation_id)->volume;
		}
		$order->furnitures = $this->orders->get_furnitures($order->id);

		// get transport_fee
		$transport_fee = $this->settings->get('transport_fee');
		// print_r($order);die;

		$data = array(
			"title_page"    => "Detail Pesanan", 
			"has_menu"      => "yes",
			"order"         => $order,
			"transport_fee" => $transport_fee
		);
		$this->twig->display('order/detail', $data);
	}

	public function cancel()
	{
		$data = $this->input->post();
		$this->cancellations->create($data['order_id'], $data['reason']);
		$this->orders->update_status($data['order_id'], 4); // change status to 4
		$this->session->set_flashdata('msg', "Anda telah membatalkan pesanan #".$data['order_id']. ".");
		redirect('order/order_list');
	}


	///
	/// AJAX METHODS
	///
	public function get_volume($package_id)
	{
		$volume = $this->packages->get_volume($package_id);
		header('Content-Type: application/json');
		echo json_encode(array('volume' => $volume));
	}

	public function get_size($furniture_id)
	{
		$sizes = $this->sizes->find(array('furniture_id' => $furniture_id), '', 0);

    	header('Content-Type: application/json');
    	echo json_encode($sizes);
	}

	public function check_moving_time()
	{
		$datetime = $this->input->post('datetime');
		$date = strtotime($datetime);

		$available = $this->orders->is_available_time($date);
    	header('Content-Type: application/json');
		echo json_encode(array('available' => $available));
	}

	///
	/// EMAIL METHODS
	///
	
	public function sendMail($email_to, $user, $order, $banks)
	{
		$config = Array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'cs.pindahan@gmail.com',
			'smtp_pass' => 'pindahan123',
			'mailtype'  => 'html',
			'charset'   => 'iso-8859-1',
			'wordwrap'  => TRUE
		);

		$message = read_file('./application/views/emails/dp_reminder.html');
		$message = str_replace('[link]', '<a href="'. site_url('order/order_list') .'" class="btn1 btn-blue-fill">Lihat pesanan saya</a>', $message);
		$message = str_replace('[price]', number_format($order['price']/2, 0, '', '.'), $message);
		$message = str_replace('[name]', $user->fullname, $message);

		// create bank table
		$strbank = '<table border="1">';
		foreach ($banks as $idx => $b) 
		{
			if($idx % 3 == 0) $strbank .= '<tr>';
			$strbank .= '<td>';
			$strbank .= '<h3>' . $b->bank_name . '</h3>';
			$strbank .= '<h4>' . $b->account_name . '</h4>';
			$strbank .= '<h4>' . $b->account_no . '</h4>';
			$strbank .= '</td>';
			if($idx % 3 == 2) $strbank .= '</tr>';
		}
		$strbank .= '</table>';
		$message = str_replace('[bank_table]', $strbank, $message);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('cs.pindahan@gmail.com'); // change it to yours
		// $this->email->to('vinsensius.danny@gmail.com'); // debugging purpose
		$this->email->to($email_to);
		$this->email->subject('Pindahan - Pembayaran DP');
		$this->email->message($message);

		if($this->email->send())
		{
			return TRUE;
		}
		else
		{
			echo $this->email->print_debugger(); //die;
			return FALSE;
		}

	}
}

?>