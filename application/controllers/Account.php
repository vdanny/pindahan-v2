<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for Account
*/
class Account extends CI_Controller
{
	protected $user_redir = 'order/order_list';
	protected $login_redir = 'account/login';

	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->load->library('form_validation');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('site_url');
		$this->twig->add_function('bower_url');
		$this->twig->add_function('set_value');
	}

	/**
	 * Redirect user to order list
	 * @return
	 */
	private function redirect_if_loggedin()
	{
		if($this->session->has_userdata('user')){
			redirect($this->user_redir);
		}
	}

	/**
	 * Redirect guest to login
	 * @return
	 */
	private function redirect_guest()
	{
		if($this->session->has_userdata('user') == FALSE) {
			redirect($this->login_redir);
		}
	}

	/**
	 * Open register page
	 * @return
	 */
	public function register()
	{
		$this->redirect_if_loggedin();

		$data = array();
		$old_value = $this->session->flashdata('old_value');
		if ($old_value){
			$data['old_value'] = $old_value;
		}
		$this->twig->display('account/register', $data);
	}

	/**
	 * Set messages with bahasa indonesia
	 */
	private function set_messages(){
		$this->form_validation->set_message('required', '{field} wajib diisi.');
		$this->form_validation->set_message('valid_email', 'Format {field} tidak valid.');
		$this->form_validation->set_message('is_unique', '{field} anda sudah pernah terdaftar.');
		$this->form_validation->set_message('min_length', '{field} tidak boleh kurang dari {param} karakter.');
		$this->form_validation->set_message('max_length', '{field} tidak boleh melebihi {param} karakter.');
		$this->form_validation->set_message('matches', '{field} harus sama dengan {param}.');
	}

	/**
	 * Processing register
	 * @return
	 */
	public function do_register()
	{
		$this->set_messages();
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[100]|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('dob', 'Tanggal lahir', 'trim|required|min_length[5]|max_length[50]');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]|max_length[100]');
		$this->form_validation->set_rules('password-confirm', 'Konfirmasi password', 'trim|required|matches[password]');
		$this->form_validation->set_rules('fullname', 'Nama lengkap', 'trim|required|min_length[3]|max_length[100]');

		if($this->form_validation->run() == FALSE) 
		{
			$errors = explode('<br/>', validation_errors());
			array_pop($errors);
			$this->session->set_flashdata('old_value', $this->input->post());
			$this->session->set_flashdata('msg_register', $errors);
			redirect('account/register');
		}
		else
		{
			$this->users->register();
			$user = $this->users->find(array('email' => $this->input->post('email')));
			if($this->sendMail($user->email, $user->id, $user->fullname)){
				$this->session->set_flashdata('success', 1);
				redirect('account/login');
			}
		}
	}

	/**
	 * Open profile page
	 * @return
	 */
	public function profile()
	{
		$this->redirect_guest();

		$userlogin = $this->session->userdata('user');
		$data = array('title_page' => 'Akun saya', "has_menu" => "yes", "user" => $userlogin);
		$this->twig->display('account/profile', $data);
	}

	/**
	 * Process update profile data
	 * @return
	 */
	public function update_profile()
	{
		$this->set_messages();
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules('fullname', 'Nama lengkap', 'trim|required|min_length[3]|max_length[100]');
		$this->form_validation->set_rules('phone', 'Nomor HP', 'trim|required|min_length[5]|max_length[20]');
		$this->form_validation->set_rules('address', 'Alamat', 'trim|required|min_length[5]|max_length[500]');
		if($this->input->post('is_merchant') == 1)
		{
			$this->form_validation->set_rules('company_name', 'Nama Toko', 'trim|min_length[5]|max_length[200]');
		}

		if($this->form_validation->run() == FALSE) 
		{
			$errors = explode('<br/>', validation_errors());
			array_pop($errors);
			$this->session->set_flashdata('msg', $errors);
		}
		else 
		{
			$this->users->update();
			$user = $this->users->find(array('id' => $this->input->post('id')));
			$this->session->set_userdata('user', $user);
			$this->session->set_flashdata('msg_success', 'Data anda berhasil diperbarui.');
		}
		redirect('account/profile');
	}

	/**
	 * Process change password
	 * @return
	 */
	public function change_password()
	{
		$this->set_messages();
		$this->form_validation->set_error_delimiters('', '<br/>');
		$this->form_validation->set_rules('password-old', 'Password lama', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('password', 'Password baru', 'trim|required|min_length[5]|max_length[100]');
		$this->form_validation->set_rules('password-confirm', 'Konfirmasi password baru', 'trim|required|matches[password]');

		if($this->form_validation->run() == FALSE)
		{
			$errors = explode('<br/>', validation_errors());
			array_pop($errors);
			$this->session->set_flashdata('msg', $errors);
		}
		else if (md5($this->input->post('password-old')) != $this->session->userdata('user')->password)
		{
			$errors = array("Password lama salah.");
			$this->session->set_flashdata('msg', $errors);
		}
		else 
		{
			$this->users->change_password();
			$user = $this->users->find(array('id' => $this->input->post('id')));
			$this->session->set_userdata('user', $user);
			$this->session->set_flashdata('msg_success', 'Password anda berhasil diperbarui.');
		}
		redirect('account/profile');
	}

	/**
	 * Open login page
	 * @return
	 */
	public function login()
	{
		$this->redirect_if_loggedin();
		$this->twig->display($this->login_redir);
	}

	/**
	 * Login user
	 * @return
	 */
	public function do_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user = $this->users->login($username, $password);
		if($user){
			$this->session->set_userdata('user', $user);
			redirect($this->user_redir);	
		}
		else{
			$this->session->set_flashdata('msg', 'Harap masukkan kombinasi email dan password yang benar.');
			redirect($this->login_redir);
		}
	}

	/**
	 * Logout user
	 * @return
	 */
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('');
	}

	/**
	 * Confirm email
	 * @param  $user_id     
	 * @param  $hashed_email
	 * @return              
	 */
	public function emailConfirmation($user_id, $hashed_email)
	{
		$user = $this->users->find(array('id' => $user_id));
		if($hashed_email == md5($user->email))
		{
			$this->users->email_confirm($user_id);
			$this->session->flashdata('msg', "Email anda berhasil dikonfirmasi.");

			$user = $this->users->find(array('id' => $user_id));
			$this->session->set_userdata('user', $user);
		}
		redirect('order/order_list');
	}

	public function resendEmailConfirmation()
	{
		$email = $this->session->userdata('user')->email;
		$fullname = $this->session->userdata('user')->fullname;
		$id = $this->session->userdata('user')->id;
		$this->sendMail($email, $id, $fullname);
		$this->session->flashdata('msg', "Permintaan email konfirmasi telah dikirim ulang.");
		redirect('order/order_list');
	}

	/**
	 * Send email confirmation
	 * @param  $email_to
	 * @param  $user_id 
	 * @param  $fullname
	 * @return boolean         
	 */
	public function sendMail($email_to, $user_id, $fullname)
	{
		$config = Array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'cs.pindahan@gmail.com',
			'smtp_pass' => 'pindahan123',
			'mailtype'  => 'html',
			'charset'   => 'iso-8859-1',
			'wordwrap'  => TRUE
		);

		$message = read_file('./application/views/emails/signup.html');
		$message = $message . '<a href="'. site_url('account/emailConfirmation/' . $user_id . '/' . md5($email_to)) .'" class="btn1 btn-blue-fill">Konfirmasi email saya</a>';
		$message = str_replace('[name]', $fullname, $message);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('cs.pindahan@gmail.com'); // change it to yours
		// $this->email->to('vinsensius.danny@gmail.com'); // debugging purpose
		$this->email->to($email_to);
		$this->email->subject('Pindahan - Konfirmasi Email');
		$this->email->message($message);

		if($this->email->send())
		{
			return TRUE;
		}
		else
		{
			echo $this->email->print_debugger();
			return FALSE;
		}

	}
}


?>