<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*  Controller for order
*/
class Testimonial extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('twig');
		$this->twig->add_function('asset_url');
		$this->twig->add_function('site_url');
		$this->twig->add_function('bower_url');
	}

	public function save()
	{
		$data = $this->input->post();
		$this->testimonials->create($data['order_id'], $data['rate'], $data['comment']);
		$this->orders->update_review_status($data['order_id'], 1);
		$this->session->set_flashdata('msg', "Terima kasih atas testimoni anda untuk pesanan #".$data['order_id']. ". Kami terus berusaha meningkatkan kinerja dan kualitas pelayanan kami ke depannya.");
		redirect('order/order_list');
	}

}