<?php

/* partials/footer.html */
class __TwigTemplate_4969aa0e14fdd14e74b02fc627c613eac21ebf609d10c90a50b1122515c1bc37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
        <!-- *** FOOTER ***
_________________________________________________________ -->

        <footer id=\"footer\">
            <div class=\"container\">
                <div class=\"col-md-6 col-sm-4\">
                    <h4>Tentang Kami</h4>

                    <p><b>Pindahan.com</b> adalah penyedia jasa pindahan berbasis web yang dapat dipesan dengan mudah dan cepat.</p>

                    <hr>

                </div>
                <!-- /.col-md-3 -->

                <div class=\"col-md-3 col-sm-4\">

                    <h4>Kontak</h4>

                    <p><strong>PT. Pindahan Indonesia</strong>
                        <br>Jalan U no. 9A
                        <br>Palmerah - 11480
                        <br>Jakarta Barat
                        <strong>Indonesia</strong>
                    </p>
                    <hr class=\"hidden-md hidden-lg hidden-sm\">

                </div>
                <div class=\"col-md-3 col-sm-4\">
                    <h4>Jam Kerja</h4>
                    <p><b>Senin - Jumat</b><br>
                    07:00 - 00:00</p>
                    <p><b>Sabtu - Minggu</b><br>
                    10:00 - 00:00</p>
                </div>
                <!-- /.col-md-3 -->
            </div>
            <!-- /.container -->
        </footer>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->

        <!-- *** COPYRIGHT ***
_________________________________________________________ -->

        <div id=\"copyright\">
            <div class=\"container\">
                <div class=\"col-md-12\">
                    <p class=\"pull-left\">&copy; 2016 - Pindahan.com developed by <a href=\"http://vdanny.com\">Vinsensius Danny</a></p>
                    <p class=\"pull-right\">Server process time: ";
        // line 52
        echo twig_escape_filter($this->env, (isset($context["elapsed_time"]) ? $context["elapsed_time"] : null), "html", null, true);
        echo " secs.
                         <!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
                    </p>

                </div>
            </div>
        </div>
        <!-- /#copyright -->

        <!-- *** COPYRIGHT END *** -->

";
    }

    public function getTemplateName()
    {
        return "partials/footer.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 52,  19 => 1,);
    }
}
