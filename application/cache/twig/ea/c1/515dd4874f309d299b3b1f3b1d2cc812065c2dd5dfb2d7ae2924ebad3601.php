<?php

/* order/new_order.html */
class __TwigTemplate_eac1515dd4874f309d299b3b1f3b1d2cc812065c2dd5dfb2d7ae2924ebad3601 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t\t\t\t\t<div class=\"col-md-9 clearfix\" id=\"checkout\">

                        <div class=\"box\">
                            <form method=\"post\" action=\"shop-checkout2.html\">

                                <ul class=\"nav nav-pills nav-justified\">
                                    <li class=\"active\"><a href=\"#\"><i class=\"fa fa-user\"></i><br>Info Umum</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-map-marker\"></i><br>Info Lokasi</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-money\"></i><br>Payment Method</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-eye\"></i><br>Order Review</a>
                                    </li>
                                </ul>

                                <div class=\"content\">
                                    <div class=\"row\">
                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"firstname\">Nama Lengkap</label>
                                                <input type=\"text\" class=\"form-control\" id=\"firstname\">
                                            </div>
                                        </div>

                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"city\">Tanggal Pindah</label>
                                                <input type=\"text\" class=\"form-control\" id=\"city\">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                    <div class=\"row\">
                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"phone\">Nomor Telepon/HP</label>
                                                <input type=\"text\" class=\"form-control\" id=\"phone\">
                                            </div>
                                        </div>

                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"city\">Waktu Pindah</label>
                                                <input type=\"text\" class=\"form-control\" id=\"city\">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <div class=\"box-footer\">
                                    <div class=\"pull-left\">
                                        <a href=\"";
        // line 57
        echo twig_escape_filter($this->env, site_url("order/order_list"), "html", null, true);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Batal pesan</a>
                                    </div>
                                    <div class=\"pull-right\">
                                        <button type=\"submit\" class=\"btn btn-template-main\">Lanjutkan ke Info Lokasi<i class=\"fa fa-chevron-right\"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->

                    <div class=\"col-md-3\">

                        <div class=\"box\" id=\"order-summary\">
                            <div class=\"box-header\">
                                <h3>Order summary</h3>
                            </div>
                            <p class=\"text-muted\">Shipping and additional costs are calculated based on the values you have entered.</p>

                            <div class=\"table-responsive\">
                                <table class=\"table\">
                                    <tbody>
                                        <tr>
                                            <td>Order subtotal</td>
                                            <th>\$446.00</th>
                                        </tr>
                                        <tr>
                                            <td>Shipping and handling</td>
                                            <th>\$10.00</th>
                                        </tr>
                                        <tr>
                                            <td>Tax</td>
                                            <th>\$0.00</th>
                                        </tr>
                                        <tr class=\"total\">
                                            <td>Total</td>
                                            <th>\$456.00</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                    <!-- /.col-md-3 -->
";
    }

    public function getTemplateName()
    {
        return "order/new_order.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 57,  39 => 3,  36 => 2,  11 => 1,);
    }
}
