<?php

/* order/new_order/harga.html */
class __TwigTemplate_fa741c991bc3de8264b1b104090ae9563666529ea6d1c36a4ea8a5f49acefdac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t\t\t\t\t<div class=\"col-md-10 col-md-offset-1\" id=\"checkout\">

                        <div class=\"box\">
                            <form method=\"post\" action=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("order/nextstep/furnitur"), "html", null, true);
        echo "\" id=\"formHarga\">

                                <ul class=\"nav nav-pills nav-justified\">
                                    <li><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("order/new_order/umum"), "html", null, true);
        echo "\"><i class=\"fa fa-user\"></i><br>Info Umum</a>
                                    </li>
                                    <li><a href=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("order/new_order/lokasi"), "html", null, true);
        echo "\"><i class=\"fa fa-map-marker\"></i><br>Lokasi</a>
                                    </li>
                                    <li class=\"active\"><a href=\"#\"><i class=\"fa fa-money\"></i><br>Harga</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-truck\"></i><br>Furnitur</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-list-alt\"></i><br>Ringkasan</a>
                                    </li>
                                </ul>

                                <div class=\"content\">
                                    <div class=\"row\">
                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"firstname\">Apakah anda ingin menggunakan paket?</label>
                                                <div class=\"radio\">
                                                  <label><input type=\"radio\" id=\"yes\" checked=\"\" value=\"y\" onclick=\"selectPackage('')\">Ya</label>
                                                </div>
                                                <div class=\"radio\">
                                                  <label><input type=\"radio\" id=\"no\" value=\"n\" onclick=\"selectPackage(0)\">Tidak</label>
                                                </div>
                                                <input type=\"hidden\" id=\"package_id\" name=\"package_id\" value=\"\" required=\"\">
                                            </div>
                                        </div>
                                    </div>
                                    <div class=\"row packages\" id=\"package\">

                                        ";
        // line 38
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packages"]) ? $context["packages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["package"]) {
            // line 39
            echo "                                            ";
            // line 40
            echo "                                            ";
            $context["divBest"] = "<div class=\"best-value\">";
            // line 41
            echo "                                            ";
            $context["divBestClose"] = "</div>";
            // line 42
            echo "                                            ";
            $context["metaBest"] = "<div class=\"meta-text\">Best Value</div>";
            // line 43
            echo "                                            ";
            $context["notavail"] = "<i class=\"fa fa-times\"></i>";
            // line 44
            echo "                                            <div class=\"col-md-3\">
                                                ";
            // line 45
            echo (($this->getAttribute($context["package"], "is_best_offer", array())) ? ((isset($context["divBest"]) ? $context["divBest"] : null)) : (""));
            echo "
                                                <div class=\"package \">
                                                    <div class=\"package-header\">
                                                        <h5>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "name", array()), "html", null, true);
            echo "</h5>
                                                        ";
            // line 49
            echo (($this->getAttribute($context["package"], "is_best_offer", array())) ? ((isset($context["metaBest"]) ? $context["metaBest"] : null)) : (""));
            echo "
                                                    </div>
                                                    <div class=\"price\">
                                                        <div class=\"price-container\">
                                                            <h4>Rp ";
            // line 53
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["package"], "price", array()), 0, "", "."), "html", null, true);
            echo "</h4>
                                                        </div>
                                                    </div>
                                                    <ul>
                                                        <li><i class=\"fa fa-check\"></i>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "transportation", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["package"], "vehicle", array()), "name", array()), "html", null, true);
            echo "</li>
                                                        <li><i class=\"fa fa-check\"></i>Gratis ";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "distance", array()), "html", null, true);
            echo " km pertama</li>
                                                        <li>";
            // line 59
            echo ((($this->getAttribute($context["package"], "employee", array()) > 0)) ? ($this->getAttribute($context["package"], "employee", array())) : ((isset($context["notavail"]) ? $context["notavail"] : null)));
            echo " Tenaga kerja</li>
                                                        <li>";
            // line 60
            echo ((($this->getAttribute($context["package"], "box", array()) > 0)) ? ($this->getAttribute($context["package"], "box", array())) : ((isset($context["notavail"]) ? $context["notavail"] : null)));
            echo " Kardus</li>
                                                    </ul>
                                                    <a class=\"btn btn-template-main\" id=\"btn-";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
            echo "\" onclick=\"selectPackage(";
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
            echo ")\">Pilih</a>
                                                </div>
                                                ";
            // line 64
            echo (($this->getAttribute($context["package"], "is_best_offer", array())) ? ((isset($context["divBestClose"]) ? $context["divBestClose"] : null)) : (""));
            echo "
                                            </div>
                                            ";
            // line 67
            echo "                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['package'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "                                    </div>
                                    <div class=\"row\" id=\"custom\" style=\"display: none;\">
                                        <table class=\"table\">
                                            <thead>
                                                <tr>
                                                    <th width=\"30%\">Barang/Jasa</th>
                                                    <th width=\"30%\">Harga Satuan (Rp)</th>
                                                    <th width=\"23%\">Pemesanan (Qty)</th>
                                                    <th>Subtotal</th>
                                                </tr>
                                            </thead>
                                            <tbody id=\"table-custom-price\">
                                                ";
        // line 80
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["customs"]) ? $context["customs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["custom"]) {
            // line 81
            echo "                                                ";
            // line 82
            echo "                                                ";
            $context["m3"] = "m<sup>3</sup>";
            // line 83
            echo "                                                <tr>
                                                    <th>";
            // line 84
            echo twig_escape_filter($this->env, $this->getAttribute($context["custom"], "name", array()), "html", null, true);
            echo " ";
            echo ((($this->getAttribute($context["custom"], "volume", array()) > 0)) ? ((((("(muatan: " . $this->getAttribute($context["custom"], "volume", array())) . " ") . (isset($context["m3"]) ? $context["m3"] : null)) . ")")) : (""));
            echo "</th>
                                                    <td>";
            // line 85
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["custom"], "price", array()), 0, "", "."), "html", null, true);
            echo "</td>
                                                    <td>
                                                        <input type=\"hidden\" name=\"custom_id[]\" value=\"";
            // line 87
            echo twig_escape_filter($this->env, $this->getAttribute($context["custom"], "id", array()), "html", null, true);
            echo "\" />
                                                        <input type=\"number\" class=\"custom-qty\" data-id=\"";
            // line 88
            echo twig_escape_filter($this->env, $this->getAttribute($context["custom"], "id", array()), "html", null, true);
            echo "\" data-price=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["custom"], "price", array()), "html", null, true);
            echo "\" name=\"custom[]\" value=\"0\" min=\"0\" max=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["custom"], "max_qty", array()), "html", null, true);
            echo "\"></input>&nbsp;/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["custom"], "max_qty", array()), "html", null, true);
            echo "</td>
                                                    <td><span id=\"subtotal-custom-";
            // line 89
            echo twig_escape_filter($this->env, $this->getAttribute($context["custom"], "id", array()), "html", null, true);
            echo "\">-</span><input type=\"hidden\" class=\"subtotal-custom\" id=\"subtotal-custom-hidden-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["custom"], "id", array()), "html", null, true);
            echo "\"></td>
                                                </tr> 
                                                ";
            // line 92
            echo "                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['custom'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "                                                <tr>
                                                    <td colspan=\"3\" style=\"text-align: right; font-size: 16px !important;\"><b>TOTAL</b></td>
                                                    <td><span id=\"grandtotal-custom\">-</span><input type=\"hidden\" id=\"grandtotal-custom-hidden\"></td>
                                                </tr>
                                                <tr>
                                                    <th><i class=\"fa fa-clock-o\"></i> Estimasi Pengerjaan</th>
                                                    <td colspan=\"3\">3 jam</td>
                                                </tr>
                                                <tr>
                                                    <td colspan=\"4\">
                                                        Catatan: 
                                                        <ul>
                                                            <li>Tenaga kerja dapat melakukan packing, angkut, dan penataan.</li>
                                                            <li>Harga tertera belum termasuk uang tambahan tips.</li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        <h4>Perlengkapan Tambahan</h4>
                                        <table class=\"table\">
                                            <thead>
                                                <tr>
                                                    <th width=\"30%\">Barang/Jasa</th>
                                                    <th width=\"30%\">Harga Satuan (Rp)</th>
                                                    <th width=\"23%\">Pemesanan (Qty)</th>
                                                    <th>Subtotal</th>
                                                </tr>
                                            </thead>
                                            <tbody id=\"table-extra-price\">
                                                ";
        // line 124
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["extras"]) ? $context["extras"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["extra"]) {
            // line 125
            echo "                                                <tr>
                                                    <th>";
            // line 126
            echo twig_escape_filter($this->env, $this->getAttribute($context["extra"], "name", array()), "html", null, true);
            echo "</th>
                                                    <td>";
            // line 127
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["extra"], "price", array()), 0, "", "."), "html", null, true);
            echo "</td>
                                                    <td>
                                                        <input type=\"hidden\" name=\"extra_id[]\" value=\"";
            // line 129
            echo twig_escape_filter($this->env, $this->getAttribute($context["extra"], "id", array()), "html", null, true);
            echo "\" />
                                                        <input type=\"number\" class=\"extra-qty\" data-id=\"";
            // line 130
            echo twig_escape_filter($this->env, $this->getAttribute($context["extra"], "id", array()), "html", null, true);
            echo "\" data-price=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["extra"], "price", array()), "html", null, true);
            echo "\" name=\"extra[]\" value=\"0\" min=\"0\" max=\"20\"></input></td>
                                                    <td><span id=\"subtotal-extra-";
            // line 131
            echo twig_escape_filter($this->env, $this->getAttribute($context["extra"], "id", array()), "html", null, true);
            echo "\">-</span><input type=\"hidden\" class=\"subtotal-extra\" id=\"subtotal-extra-hidden-";
            echo twig_escape_filter($this->env, $this->getAttribute($context["extra"], "id", array()), "html", null, true);
            echo "\"></td>
                                                </tr>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['extra'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 134
        echo "                                                <tr>
                                                    <td colspan=\"3\" style=\"text-align: right; font-size: 16px !important;\"><b>TOTAL</b></td>
                                                    <td><span id=\"grandtotal-extra\">-</span><input type=\"hidden\" id=\"grandtotal-extra-hidden\"></td>
                                                </tr> 
                                                <tr>
                                                    <td colspan=\"3\" style=\"text-align: right; font-size: 18px !important;\"><b>GRAND TOTAL</b></td>
                                                    <td><span id=\"grandtotal\">-</span><input type=\"hidden\" id=\"grandtotal-hidden\"></td>
                                                </tr> 
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <div class=\"box-footer\">
                                    <div class=\"pull-left\">
                                        <a href=\"";
        // line 150
        echo twig_escape_filter($this->env, site_url("order/new_order/lokasi"), "html", null, true);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Kembali ke Lokasi</a>
                                    </div>
                                    <div class=\"pull-right\">
                                        <button type=\"submit\" class=\"btn btn-template-main\">Lanjutkan ke Furnitur<i class=\"fa fa-chevron-right\"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->
";
    }

    // line 166
    public function block_scripts($context, array $blocks = array())
    {
        // line 167
        echo "    <script type=\"text/javascript\">
        \$(document).ready(function(){
            // show hide div
            \$('input[type=radio]').change(function(){
                var \$this = \$(this);
                if(\$this.val() == 'y'){
                    \$('#package').show();
                    \$('#custom').hide();
                    \$('#no').prop('checked', false);
                }
                else{
                    \$('#package').hide();
                    \$('#custom').show();
                    \$('#yes').prop('checked', false);
                }
            });

            // filling custom price
            \$('.custom-qty').change(function() {
                var id = \$(this).attr('data-id');
                var price = parseInt(\$(this).attr('data-price'));
                var qty = parseInt(\$(this).val());
                if((price*qty) > 0){
                    \$('#subtotal-custom-' + id).text('Rp ' + (price*qty).formatMoney(2, ',', '.'));
                    \$('#subtotal-custom-hidden-' + id).val(price*qty);
                }
                else{
                    \$('#subtotal-custom-' + id).text('-');
                    \$('#subtotal-custom-hidden-' + id).val(0);
                }
                evaluateSubTotalCustom();
                evaluateGrandTotal();
            });

            // filling extra price
            \$('.extra-qty').change(function() {
                var id = \$(this).attr('data-id');
                var price = parseInt(\$(this).attr('data-price'));
                var qty = parseInt(\$(this).val());
                if((price*qty) > 0){
                    \$('#subtotal-extra-' + id).text('Rp ' + (price*qty).formatMoney(2, ',', '.'));
                    \$('#subtotal-extra-hidden-' + id).val(price*qty);
                }
                else{
                    \$('#subtotal-extra-' + id).text('-');
                    \$('#subtotal-extra-hidden-' + id).val(0);
                }
                evaluateSubTotalExtra();
                evaluateGrandTotal();
            });
        });

        function evaluateSubTotalCustom()
        {
            var subtotal = 0;
            \$('.subtotal-custom').each(function(id, el){
                var \$row = \$(el);
                if(\$row.val() != \"\"){
                    subtotal += parseInt(\$row.val());
                }
            });
            if(subtotal > 0){
                \$('#grandtotal-custom').text('Rp ' + (subtotal).formatMoney(2, ',', '.'));
                \$('#grandtotal-custom-hidden').val(subtotal);
            }
            else{
                \$('#grandtotal-custom').text('-');
                \$('#grandtotal-custom-hidden').val(0);
            }
        }

        function evaluateSubTotalExtra()
        {
            var subtotal = 0;
            \$('.subtotal-extra').each(function(id, el){
                var \$row = \$(el);
                if(\$row.val() != \"\"){
                    subtotal += parseInt(\$row.val());
                }
            });
            if(subtotal > 0){
                \$('#grandtotal-extra').text('Rp ' + (subtotal).formatMoney(2, ',', '.'));
                \$('#grandtotal-extra-hidden').val(subtotal);
            }
            else{
                \$('#grandtotal-extra').text('-');
                \$('#grandtotal-extra-hidden').val(0);
            }
        }

        function evaluateGrandTotal()
        {
            var custom = \$('#grandtotal-custom-hidden').val() == '' ? 0 : parseInt(\$('#grandtotal-custom-hidden').val());
            var extra = \$('#grandtotal-extra-hidden').val() == '' ? 0 : parseInt(\$('#grandtotal-extra-hidden').val());
            var grandtotal = custom + extra;

            if(grandtotal > 0){
                \$('#grandtotal').text('Rp ' + (grandtotal).formatMoney(2, ',', '.'));
                \$('#grandtotal-hidden').val(grandtotal);
            }
            else{
                \$('#grandtotal').text('-');
                \$('#grandtotal-hidden').val(0);
            }
        }

        // selecting package to hidden field
        function selectPackage(pid) {
            \$('#package_id').val(pid);
            \$('.btn-template-main').removeClass('active');
            if(pid != 0 && pid != '') {
                \$('#btn-' + pid).addClass('active');
            }
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "order/new_order/harga.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  342 => 167,  339 => 166,  320 => 150,  302 => 134,  291 => 131,  285 => 130,  281 => 129,  276 => 127,  272 => 126,  269 => 125,  265 => 124,  232 => 93,  226 => 92,  219 => 89,  209 => 88,  205 => 87,  200 => 85,  194 => 84,  191 => 83,  188 => 82,  186 => 81,  182 => 80,  168 => 68,  162 => 67,  157 => 64,  150 => 62,  145 => 60,  141 => 59,  137 => 58,  131 => 57,  124 => 53,  117 => 49,  113 => 48,  107 => 45,  104 => 44,  101 => 43,  98 => 42,  95 => 41,  92 => 40,  90 => 39,  86 => 38,  56 => 11,  51 => 9,  45 => 6,  40 => 3,  37 => 2,  11 => 1,);
    }
}
