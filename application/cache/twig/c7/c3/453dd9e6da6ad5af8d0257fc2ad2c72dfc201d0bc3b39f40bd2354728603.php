<?php

/* order/new_order/furnitur.html */
class __TwigTemplate_c7c3453dd9e6da6ad5af8d0257fc2ad2c72dfc201d0bc3b39f40bd2354728603 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t\t\t\t\t<div class=\"col-md-10 col-md-offset-1\" id=\"checkout\">

                        <div class=\"box\">
                            <form method=\"post\" action=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("order/nextstep/ringkasan"), "html", null, true);
        echo "\">

                                <ul class=\"nav nav-pills nav-justified\">
                                    <li><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("order/new_order/umum"), "html", null, true);
        echo "\"><i class=\"fa fa-user\"></i><br>Info Umum</a>
                                    </li>
                                    <li><a href=\"";
        // line 11
        echo twig_escape_filter($this->env, site_url("order/new_order/lokasi"), "html", null, true);
        echo "\"><i class=\"fa fa-map-marker\"></i><br>Lokasi</a>
                                    </li>
                                    <li><a href=\"";
        // line 13
        echo twig_escape_filter($this->env, site_url("order/new_order/harga"), "html", null, true);
        echo "\"><i class=\"fa fa-money\"></i><br>Harga</a>
                                    </li>
                                    <li class=\"active\"><a href=\"#\"><i class=\"fa fa-truck\"></i><br>Furnitur</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-list-alt\"></i><br>Ringkasan</a>
                                    </li>
                                </ul>

                                <div class=\"content\">
                                \t<div class=\"row\">
\t                                \t<div class=\"col-sm-12\">
\t                                \t\t<div class=\"box\">
\t\t\t                                    <div class=\"row\">
\t\t\t                                        <div class=\"col-sm-4\">
\t\t\t                                            <div class=\"form-group\">
\t\t\t                                                <label for=\"firstname\">Jenis Furnitur</label>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<select class=\"form-control\" id=\"appliances\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Pilih jenis perabotan</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["furnitures"]) ? $context["furnitures"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["furniture"]) {
            // line 32
            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["furniture"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["furniture"], "name", array()), "html", null, true);
            echo "</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['furniture'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t                                            </div>
\t\t\t                                        </div>

\t\t\t                                        <div class=\"col-sm-4\">
\t\t\t                                            <div class=\"form-group\">
\t\t\t                                                <label for=\"city\">Ukuran</label>
\t\t\t                                                <select class=\"form-control\" id=\"sizes\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<option value=\"\">Pilih ukuran</option>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</select>
\t\t\t                                            </div>
\t\t\t                                        </div>

\t\t\t                                        <div class=\"col-sm-2\" style=\"padding-top: 3%;\">
\t\t\t                                                <button type=\"button\" id=\"btn-add\" class=\"btn btn-template-main\"><i class=\"fa fa-plus\"></i> Tambah</button>
\t\t\t                                        </div>
\t\t\t                                        <div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"text-right\">Volume tersedia</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td class=\"text-right\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span id=\"total_volume_text\">";
        // line 57
        echo twig_escape_filter($this->env, (isset($context["total_volume"]) ? $context["total_volume"] : null), "html", null, true);
        echo "</span> m<sup>3</sup>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"total_volume\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, (isset($context["total_volume"]) ? $context["total_volume"] : null), "html", null, true);
        echo "\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t                                        </div>
\t\t\t                                    </div>
\t\t\t                                    <!-- /.row -->
\t\t\t                                    <div class=\"row\" id=\"custom-volume\" style=\"display: none;\">
\t\t\t                                    \t<div class=\"col-sm-2\">
\t\t\t                                    \t\t<h5>Masukkan ukuran furnitur</h5>
\t\t\t                                    \t</div>
\t\t\t                                    \t<div class=\"col-sm-2\">
\t\t\t                                    \t\t<div class=\"form-group\">
\t\t\t                                    \t\t\t<label>Panjang</label>
\t\t\t                                    \t\t\t<div class=\"input-group\">
\t\t\t                                    \t\t\t\t<input type=\"number\" class=\"form-control volume-compo\" id=\"l\" value=\"0\" min=\"0\">
\t\t\t                                    \t\t\t\t<span class=\"input-group-addon\">m</span>
\t\t\t                                    \t\t\t</div>
\t\t\t                                    \t\t</div>
\t\t\t                                    \t</div>
\t\t\t                                    \t<div class=\"col-sm-2\">
\t\t\t                                    \t\t<div class=\"form-group\">
\t\t\t                                    \t\t\t<label>Lebar</label>
\t\t\t                                    \t\t\t<div class=\"input-group\">
\t\t\t                                    \t\t\t\t<input type=\"number\" class=\"form-control volume-compo\" id=\"w\" value=\"0\" min=\"0\">
\t\t\t                                    \t\t\t\t<span class=\"input-group-addon\">m</span>
\t\t\t                                    \t\t\t</div>
\t\t\t                                    \t\t</div>
\t\t\t                                    \t</div>
\t\t\t                                    \t<div class=\"col-sm-2\">
\t\t\t                                    \t\t<div class=\"form-group\">
\t\t\t                                    \t\t\t<label>Tinggi</label>
\t\t\t                                    \t\t\t<div class=\"input-group\">
\t\t\t                                    \t\t\t\t<input type=\"number\" class=\"form-control volume-compo\" id=\"h\" value=\"0\" min=\"0\">
\t\t\t                                    \t\t\t\t<span class=\"input-group-addon\">m</span>
\t\t\t                                    \t\t\t</div>
\t\t\t                                    \t\t</div>
\t\t\t                                    \t</div>
\t\t\t                                    \t<div class=\"col-sm-2\">
\t\t\t                                    \t\t<div class=\"form-group\">
\t\t\t                                    \t\t\t<label>Volume</label>
\t\t\t                                    \t\t\t<input type=\"text\" class=\"form-control\" id=\"v\" readonly=\"\" placeholder=\"Volume Furnitur\">
\t\t\t                                    \t\t</div>
\t\t\t                                    \t</div>
\t\t\t                                    </div>
\t\t\t                                </div>

\t\t                                    <div class=\"row\">
\t\t\t\t\t\t\t\t\t\t\t\t<p id=\"no-appliances\" class=\"text-center\">Anda belum menambahkan furnitur.</p>
\t\t\t\t\t\t\t\t\t\t\t\t<table id=\"table-appliances\" class=\"table table-bordered table-hover\" style=\"display: none;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th style=\"width: 30%;\">Jenis Perabotan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th style=\"width: 25%\">Ukuran</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th style=\"width: 25%\">Volume</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th style=\"width: 15%\">Jumlah</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th style=\"width: 5%\">Hapus</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody id=\"appliance-row\">
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t                                    </div>
\t\t                                    <!-- /.row -->
\t\t                                </div>
\t\t\t\t\t\t\t\t\t</div>
                                </div>

                                <div class=\"box-footer\">
                                    <div class=\"pull-left\">
                                        <a href=\"";
        // line 128
        echo twig_escape_filter($this->env, site_url("order/new_order/harga"), "html", null, true);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Kembali ke Harga</a>
                                    </div>
                                    <div class=\"pull-right\">
                                        <button type=\"submit\" class=\"btn btn-template-main\">Lanjutkan ke Ringkasan<i class=\"fa fa-chevron-right\"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col-md-7 -->
";
    }

    // line 142
    public function block_scripts($context, array $blocks = array())
    {
        // line 143
        echo "<script type=\"text/javascript\">
\tfunction countApplianceRow() {
\t\treturn \$('#appliance-row tr').length;
\t}

\tfunction checkDuplicateAppliance(appliance, size) {
\t\tvar applianceExists = false, sizeExists = false;
\t\t\$.each(\$('#appliance-row tr'), function(key, tr) {
\t\t\tvar \$tr = \$(tr);
\t\t\tapplianceExists = \$tr.children()[0].innerHTML == appliance;
\t\t\tsizeExists = \$tr.children()[1].innerHTML == size;
\t\t\tif(applianceExists && sizeExists) return false;
\t\t});
\t\treturn !(applianceExists && sizeExists);
\t}

\t\$('#btn-add').click(function(){
\t\tif(\$('#appliances').val() != '' && \$('#sizes').val() == '0') {
\t\t\tvar appliance = \$(\"#appliances option:selected\").text();
\t\t\tvar size = \"Custom: \" + \$('#l').val() +\"m x \" + \$('#w').val() +\"m x \" + \$('#h').val() +\"m\";
\t\t\tvar appliance_id = \$(\"#appliances\").val();
\t\t\tvar size_id = 0;
\t\t\tvar volume = parseFloat(\$('#l').val()) * parseFloat(\$('#w').val()) * parseFloat(\$('#h').val());

\t\t\t\$('#table-appliances').show();
\t\t\t\$('#no-appliances').hide();
\t\t\t\$('#appliance-row')
\t\t\t.append('<tr><td>' + appliance + '</td><td>' + size + '</td><td>' + volume + 'm<sup>3</sup></td><td>'+
\t\t\t\t'<input type=\"hidden\" name=\"appliances_id[]\" value=\"' + appliance_id + '\"/><input type=\"hidden\" name=\"sizes_id[]\" value=\"' + size_id + '\"/>'+
\t\t\t\t'<input type=\"number\" name=\"appliances[]\" value=\"0\" data-lastvalue=\"0\" data-volume=\"'+ volume +'\" min=\"0\"></input></td>'+
\t\t\t\t'<td><a class=\"btn btn-default btn-delete-row\" title=\"Hapus\"><i class=\"fa fa-trash\"></i></button></td></tr>');

\t\t\tbindNumber();

\t\t\t// delete appliance row
\t\t\t\$('.btn-delete-row').click(function() {
\t\t\t\t\$(this).closest('tr').remove();
\t\t\t\tif(countApplianceRow() == 0){
\t\t\t\t\t\$('#table-appliances').hide();
\t\t\t\t\t\$('#no-appliances').show();
\t\t\t\t}
\t\t\t});

\t\t\t\$('#l').val(0);
\t\t\t\$('#w').val(0);
\t\t\t\$('#h').val(0);
\t\t\t\$(\"#v\").val(\"0 m³\");
\t\t}
\t\telse if(\$('#appliances').val() != '' && \$('#sizes').val() != '') {
\t\t\tvar appliance = \$(\"#appliances option:selected\").text();
\t\t\tvar size = \$(\"#sizes option:selected\").text();
\t\t\tvar appliance_id = \$(\"#appliances\").val();
\t\t\tvar temp = \$(\"#sizes\").val().split('-');
\t\t\tvar size_id = temp[0];
\t\t\tvar volume = temp[1];

\t\t\tif(checkDuplicateAppliance(appliance, size)){
\t\t\t\t\$('#table-appliances').show();
\t\t\t\t\$('#no-appliances').hide();
\t\t\t\t\$('#appliance-row')
\t\t\t\t.append('<tr><td>' + appliance + '</td><td>' + size + '</td><td>' + volume + 'm<sup>3</sup></td><td>'+
\t\t\t\t\t'<input type=\"hidden\" name=\"appliances_id[]\" value=\"' + appliance_id + '\"/><input type=\"hidden\" name=\"sizes_id[]\" value=\"' + size_id + '\"/>'+
\t\t\t\t\t'<input type=\"number\" name=\"appliances[]\" value=\"0\" data-lastvalue=\"0\" data-volume=\"'+ volume +'\" min=\"0\"></input></td>'+
\t\t\t\t\t'<td><a class=\"btn btn-default btn-delete-row\" title=\"Hapus\"><i class=\"fa fa-trash\"></i></button></td></tr>');

\t\t\t\tbindNumber();

\t\t\t\t// delete appliance row
\t\t\t\t\$('.btn-delete-row').click(function() {
\t\t\t\t\t\$(this).closest('tr').remove();
\t\t\t\t\tif(countApplianceRow() == 0){
\t\t\t\t\t\t\$('#table-appliances').hide();
\t\t\t\t\t\t\$('#no-appliances').show();
\t\t\t\t\t}
\t\t\t\t});
\t\t\t}
\t\t\telse {
\t\t\t\talert('Pilihan perabotan dan ukuran tersebut sudah ada.');
\t\t\t}
\t\t}
\t\telse {
\t\t\talert('Harap pilih perabotan dan ukuran terlebih dahulu.');
\t\t}
\t});

\t\$(\"#sizes\").change(function() {
\t\t\$sizes = \$(this);
\t\tif(\$sizes.val() == '0') {
\t\t\t\$('#custom-volume').show();
\t\t}
\t\telse {
\t\t\t\$('#custom-volume').hide();
\t\t}
\t});

\t// get sizes for appliance
\t\$('#appliances').change(function() {
\t\t\$applianceID = \$(this).val();
\t\t\$.ajax({
\t\t\turl: '";
        // line 242
        echo twig_escape_filter($this->env, site_url("order/get_size"), "html", null, true);
        echo "/' + \$applianceID,
\t\t\tsuccess: function(res) {
\t\t\t\tif(res){
\t\t\t\t\t\$('#sizes').empty();
\t\t\t\t\t\$('#sizes').append('<option value=\"\">Pilih ukuran</option>');
\t\t\t\t\tfor (var i = 0; i < res.length; i++) {
\t\t\t\t\t\t\$('#sizes').append('<option value=\"' + res[i].id + '-' + (res[i].length*res[i].width*res[i].height) + '\">' + res[i].name + ' (' + res[i].length + 'm x ' + res[i].width + 'm x ' + res[i].height + 'm)</option>');
\t\t\t\t\t}
\t\t\t\t\t\$('#sizes').append('<option value=\"0\">Pilih ukuran sendiri</option>');
\t\t\t\t}
\t\t\t}
\t\t});
\t});

\t\$(\".volume-compo\").change(function() {
\t\tvar l = \$(\"#l\").val();
\t\tvar w = \$(\"#w\").val();
\t\tvar h = \$(\"#h\").val();

\t\tvar volume = Math.round(l*w*h * 1000) / 1000;
\t\t\$(\"#v\").val(volume+\" m³\");
\t});

\tfunction bindNumber(){
\t\t\$('input[type=\"number\"]').last().change(function () {

\t    \tvar v = parseFloat(\$(this).data('volume'));

\t        if(this.value < \$(this).data('lastvalue')){
\t\t\t\tcountTotalVolume(v, 'add');
\t\t\t}
\t\t\telse if(this.value > \$(this).data('lastvalue')){
\t\t\t\tif(countTotalVolume(v, 'sub') == false){
\t\t\t\t\t\$(this).val(\$(this).data('lastvalue'));
\t\t\t\t}
\t\t\t}

\t        \$(this).data('lastvalue', this.value);
\t\t}).change();
\t}

\tvar total_volume = ";
        // line 283
        echo twig_escape_filter($this->env, (isset($context["total_volume"]) ? $context["total_volume"] : null), "html", null, true);
        echo ";
\tfunction countTotalVolume(v, mode){
\t\tif(mode == 'sub' && total_volume - v < 0) {
\t\t\talert('Tidak bisa menambah furnitur karena volume yang tersedia sudah habis');
\t\t\treturn false;
\t\t}

\t\tif (mode == 'add') total_volume += v;
\t\telse if (mode == 'sub') total_volume -= v;
\t\t\$('#total_volume_text').text(total_volume);
\t\treturn true;
\t}
</script>
";
    }

    public function getTemplateName()
    {
        return "order/new_order/furnitur.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  364 => 283,  320 => 242,  219 => 143,  216 => 142,  199 => 128,  126 => 58,  122 => 57,  97 => 34,  86 => 32,  82 => 31,  61 => 13,  56 => 11,  51 => 9,  45 => 6,  40 => 3,  37 => 2,  11 => 1,);
    }
}
