<?php

/* order/new_order/umum.html */
class __TwigTemplate_081607ca30d02dc28129da546ac183088ec6eb7daa6e13f4051e7f3aace2a5a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t\t\t\t\t<div class=\"col-md-10 col-md-offset-1\" id=\"checkout\">
                        
                        <div class=\"box\">
                            <form method=\"post\" action=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("order/nextstep/lokasi"), "html", null, true);
        echo "\" id=\"formUmum\" enctype=\"multipart/form-data\">

                                <ul class=\"nav nav-pills nav-justified\">
                                    <li class=\"active\"><a href=\"#\"><i class=\"fa fa-user\"></i><br>Info Umum</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-map-marker\"></i><br>Lokasi</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-money\"></i><br>Harga</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-truck\"></i><br>Furnitur</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-list-alt\"></i><br>Ringkasan</a>
                                    </li>
                                </ul>

                                <div class=\"content\">
                                    ";
        // line 22
        $context["error"] = $this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "flashdata", array()), "msg", array());
        // line 23
        echo "                                    ";
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 24
            echo "                                    <div class=\"alert alert-danger alert-dismissible\">
                                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                        <strong>Oops! Terjadi kesalahan: </strong>
                                        ";
            // line 27
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
            echo "
                                    </div>
                                    ";
        }
        // line 30
        echo "                                    <div class=\"row\">
                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"fullname\">Nama Lengkap *</label>
                                                <input type=\"text\" class=\"form-control\" id=\"fullname\" name=\"fullname\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "userdata", array()), "user", array()), "fullname", array()), "html", null, true);
        echo "\" required=\"\" minlength=\"3\" maxlength=\"200\" />
                                            </div>
                                        </div>

                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"move_date\">Tanggal Pindah *</label>
                                                <input type=\"text\" class=\"form-control datepicker\" id=\"move_date\" name=\"move_date\" required=\"\" value=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "userdata", array()), "order", array()), "move_date", array()), "html", null, true);
        echo "\"/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                    <div class=\"row\">
                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"phone\">Nomor Telepon/HP *</label>
                                                <input type=\"text\" class=\"form-control\" id=\"phone\" name=\"phone\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "userdata", array()), "user", array()), "phone", array()), "html", null, true);
        echo "\" />
                                            </div>
                                        </div>

                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"move_time\">Waktu Pindah *</label>
                                                <input type=\"text\" class=\"form-control timepicker\" id=\"move_time\" name=\"move_time\" required=\"\" value=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "userdata", array()), "order", array()), "move_time", array()), "html", null, true);
        echo "\"/>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                    <div class=\"row\">
                                        <div class=\"col-sm-12\">
                                            <div class=\"form-group\">
                                                <label for=\"phone\">Catatan Tambahan</label>
                                                <textarea class=\"form-control\" id=\"notes\" name=\"notes\" placeholder=\"Tambahkan keterangan pindahan seperti kesulitan saat penjemputan, keterangan lokasi dan keterangan area parkir untuk transportasi kami di kolom ini.\">";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "userdata", array()), "order", array()), "notes", array()), "html", null, true);
        echo "</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                    <div class=\"row\">
                                        <div class=\"col-sm-12\">
                                            <div class=\"form-group\">
                                                <label for=\"blueprint\">Denah Ruangan Lokasi Tujuan</label>
                                                <input type=\"file\" id=\"blueprint\" name=\"blueprint\" />
                                                <p class=\"help-block\">Upload file denah ruangan rumah/kost/apartemen tujuan anda sesuai keinginan anda jika anda akan menggunakan jasa penataan ruangan.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <div class=\"box-footer\">
                                    <div class=\"pull-left\">
                                        <a href=\"";
        // line 88
        echo twig_escape_filter($this->env, site_url("order/order_list"), "html", null, true);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Batal pesan</a>
                                    </div>
                                    <div class=\"pull-right\">
                                        <label for=\"submitForm\" class=\"btn btn-template-main\">Lanjutkan ke Lokasi <i class=\"fa fa-chevron-right\"></i>
                                        </label>
                                        <input type=\"submit\" id=\"submitForm\" class=\"hidden\">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->
";
    }

    // line 104
    public function block_scripts($context, array $blocks = array())
    {
        // line 105
        echo "<script>
    var now = new Date();
    \$(function () {
        \$('.datepicker').datetimepicker({
            format: 'L',
            minDate: new Date(now.getFullYear(), now.getMonth(), now.getDate()+1)
        });
        \$('.timepicker').datetimepicker({
            format: 'LT',
            stepping: 30
        });
    });

    \$('#formUmum').validate();
</script>
";
    }

    public function getTemplateName()
    {
        return "order/new_order/umum.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 105,  175 => 104,  155 => 88,  132 => 68,  119 => 58,  109 => 51,  96 => 41,  86 => 34,  80 => 30,  74 => 27,  69 => 24,  66 => 23,  64 => 22,  45 => 6,  40 => 3,  37 => 2,  11 => 1,);
    }
}
