<?php

/* order/new_order/ringkasan.html */
class __TwigTemplate_36b442b76fe4a7c440e7413c49c03e4fd6885f85f16b2f2dcac0c976c2e3f9f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t\t\t\t\t<div class=\"col-md-10 col-md-offset-1\" id=\"checkout\">

                        <div class=\"box\">

                            <ul class=\"nav nav-pills nav-justified\">
                                <li><a href=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("order/new_order/umum"), "html", null, true);
        echo "\"><i class=\"fa fa-user\"></i><br>Info Umum</a>
                                </li>
                                <li><a href=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("order/new_order/lokasi"), "html", null, true);
        echo "\"><i class=\"fa fa-map-marker\"></i><br>Lokasi</a>
                                </li>
                                <li><a href=\"";
        // line 12
        echo twig_escape_filter($this->env, site_url("order/new_order/harga"), "html", null, true);
        echo "\"><i class=\"fa fa-money\"></i><br>Harga</a>
                                </li>
                                <li><a href=\"";
        // line 14
        echo twig_escape_filter($this->env, site_url("order/new_order/furnitur"), "html", null, true);
        echo "\"><i class=\"fa fa-truck\"></i><br>Furnitur</a>
                                </li>
                                <li class=\"active\"><a href=\"#\"><i class=\"fa fa-list-alt\"></i><br>Ringkasan</a>
                                </li>
                            </ul>

                            <div class=\"content\">
                                <div class=\"row\">
                                    <div class=\"col-sm-12\">
                                        <h2>PESANAN PINDAHAN ANDA</h2>
                                        <p class=\"text-muted\">Harap pastikan lagi informasi pindahan anda sudah benar.</p>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class=\"row\">

                                    <div class=\"col-sm-6\">
                                        <div class=\"table-responsive\">
                                            <h4>Info Umum</h4>
                                            <table class=\"table\">
                                                <tbody>
                                                    <tr>
                                                        <td>Nama pemesan</td>
                                                        <th>";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "fullname", array()), "html", null, true);
        echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Nomor HP</td>
                                                        <th>";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "phone", array()), "html", null, true);
        echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Pindahan</td>
                                                        <th>";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "move_date", array()), "html", null, true);
        echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Waktu Pindahan</td>
                                                        <th>";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "move_time", array()), "html", null, true);
        echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Catatan Tambahan</td>
                                                        <th>";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "notes", array()), "html", null, true);
        echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Denah Ruangan Lokasi Tujuan</td>
                                                        <th>
                                                            ";
        // line 58
        if ($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "blueprint", array())) {
            // line 59
            echo "                                                            <a href=\"#\">Lihat gambar di sini</a>
                                                            ";
        } else {
            // line 61
            echo "                                                            Tidak ada file
                                                            ";
        }
        // line 63
        echo "                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td class=\"text-right\"><a href=\"";
        // line 67
        echo twig_escape_filter($this->env, site_url("order/new_order/umum"), "html", null, true);
        echo "\"><i class=\"fa fa-pencil\"></i> Ubah informasi</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class=\"col-sm-6\">
                                        <div class=\"table-responsive\">
                                            <h4>Lokasi</h4>
                                            <table class=\"table\">
                                                <tbody>
                                                    <tr>
                                                        <th>Alamat penjemputan</th>
                                                    </tr>
                                                    <tr>
                                                        <td>";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "start_address", array()), "html", null, true);
        echo "</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Alamat penjemputan</th>
                                                    </tr>
                                                    <tr>
                                                        <td>";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "end_address", array()), "html", null, true);
        echo "</td>
                                                    </tr>
                                                    <tr>
                                                        <td class=\"text-right\"><a href=\"";
        // line 92
        echo twig_escape_filter($this->env, site_url("order/new_order/lokasi"), "html", null, true);
        echo "\"><i class=\"fa fa-pencil\"></i> Ubah informasi</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <hr>
                                <div class=\"row\">
                                    
                                    <div class=\"col-sm-6\">
                                        <div class=\"table-responsive\">
                                            <h4>Furnitur</h4>
                                            <p>Volume tersedia: ";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "total_volume", array()), "html", null, true);
        echo " m<sup>3</sup></p>
                                            <table class=\"table\">
                                                <tbody>
                                                    <tr>
                                                        <th>Jenis</th>
                                                        <th>Ukuran</th>
                                                        <th>Jumlah</th>
                                                    </tr>
                                                    ";
        // line 114
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "appliances", array())) - 1)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 115
            echo "                                                    <tr>
                                                        <td>";
            // line 116
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["furnitures"]) ? $context["furnitures"] : null), $context["i"], array(), "array"), "name", array()), "html", null, true);
            echo "</td>
                                                        <td>";
            // line 117
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sizes"]) ? $context["sizes"] : null), $context["i"], array(), "array"), "name", array()), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sizes"]) ? $context["sizes"] : null), $context["i"], array(), "array"), "length", array()), "html", null, true);
            echo "m x ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sizes"]) ? $context["sizes"] : null), $context["i"], array(), "array"), "width", array()), "html", null, true);
            echo "m x ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sizes"]) ? $context["sizes"] : null), $context["i"], array(), "array"), "height", array()), "html", null, true);
            echo "m)</td>
                                                        <td>";
            // line 118
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "appliances", array()), $context["i"], array(), "array"), "html", null, true);
            echo "</td>
                                                    </tr>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 121
        echo "                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td class=\"text-right\"><a href=\"";
        // line 124
        echo twig_escape_filter($this->env, site_url("order/new_order/furnitur"), "html", null, true);
        echo "\"><i class=\"fa fa-pencil\"></i> Ubah informasi</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class=\"col-sm-6\">
                                        <div class=\"table-responsive\">
                                            <h4>Harga</h4>
                                            <table class=\"table\">
                                                ";
        // line 135
        if (($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package_id", array()) != 0)) {
            // line 136
            echo "                                                ";
            $context["order_dist"] = ($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "distance", array()) / 1000);
            // line 137
            echo "                                                ";
            $context["paid_dist"] = ((isset($context["order_dist"]) ? $context["order_dist"] : null) - $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "distance", array()));
            // line 138
            echo "                                                <tbody>
                                                    <tr>
                                                        <td>Pilihan Harga</td>
                                                        <th>Paket / <del>Custom</del></th>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenis Paket</td>
                                                        <th>";
            // line 145
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "name", array()), "html", null, true);
            echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Transportasi</td>
                                                        <th>";
            // line 149
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "transportation", array()), "html", null, true);
            echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Pekerja</td>
                                                        <th>";
            // line 153
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "employee", array()), "html", null, true);
            echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Kardus</td>
                                                        <th>";
            // line 157
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "box", array()), "html", null, true);
            echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Jarak Tempuh</td>
                                                        <th>";
            // line 161
            echo twig_escape_filter($this->env, (isset($context["order_dist"]) ? $context["order_dist"] : null), "html", null, true);
            echo " km (gratis ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "distance", array()), "html", null, true);
            echo " km)</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Biaya Ongkos (Rp 10.000,-)</td>
                                                        <th>Rp ";
            // line 165
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["paid_dist"]) ? $context["paid_dist"] : null) * 10000), 0, "", "."), "html", null, true);
            echo ",- <span class=\"text-muted\">(";
            echo twig_escape_filter($this->env, (isset($context["paid_dist"]) ? $context["paid_dist"] : null), "html", null, true);
            echo " km)</span></th>
                                                    </tr>
                                                    <tr>
                                                        <td>Biaya Paket</td>
                                                        <th>Rp ";
            // line 169
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "price", array()), 0, "", "."), "html", null, true);
            echo ",-</th>
                                                    </tr>
                                                    <tr class=\"total\">
                                                        <td>Total Dibayarkan</td>
                                                        <th>Rp ";
            // line 173
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "price", array()) + ((isset($context["paid_dist"]) ? $context["paid_dist"] : null) * 10000)), 0, "", "."), "html", null, true);
            echo ",-</th>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td class=\"text-right\"><a href=\"";
            // line 177
            echo twig_escape_filter($this->env, site_url("order/new_order/harga"), "html", null, true);
            echo "\"><i class=\"fa fa-pencil\"></i> Ganti paket</a></td>
                                                    </tr>
                                                </tbody>
                                                ";
        }
        // line 181
        echo "                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class=\"box-footer\">
                                <div class=\"pull-left\">
                                    <a href=\"";
        // line 189
        echo twig_escape_filter($this->env, site_url("order/new_order/furnitur"), "html", null, true);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Kembali ke Furnitur</a>
                                </div>
                                <div class=\"pull-right\">
                                    <a href=\"";
        // line 192
        echo twig_escape_filter($this->env, site_url("order/checkout"), "html", null, true);
        echo "\" class=\"btn btn-template-main\" onclick=\"return confirm('Apakah anda yakin untuk melakukan pembayaran?')\">Lanjutkan ke Pembayaran<i class=\"fa fa-chevron-right\"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->

";
    }

    // line 205
    public function block_scripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "order/new_order/ringkasan.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  367 => 205,  351 => 192,  345 => 189,  335 => 181,  328 => 177,  321 => 173,  314 => 169,  305 => 165,  296 => 161,  289 => 157,  282 => 153,  275 => 149,  268 => 145,  259 => 138,  256 => 137,  253 => 136,  251 => 135,  237 => 124,  232 => 121,  223 => 118,  213 => 117,  209 => 116,  206 => 115,  202 => 114,  191 => 106,  174 => 92,  168 => 89,  159 => 83,  140 => 67,  134 => 63,  130 => 61,  126 => 59,  124 => 58,  116 => 53,  109 => 49,  102 => 45,  95 => 41,  88 => 37,  62 => 14,  57 => 12,  52 => 10,  47 => 8,  40 => 3,  37 => 2,  11 => 1,);
    }
}
