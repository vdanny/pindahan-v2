<?php

/* account/register.html */
class __TwigTemplate_36488d9ed266be2185469d0dd169532ab65cbcf3d30f87b44ceb7f27ea7cb728 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/landing.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/landing.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "        <div id=\"content\">
            <div class=\"container\">

                <div class=\"row\">
                    <div class=\"col-md-8\" style=\"border-right: 1px solid #ccc\">
                        <div class=\"box\">
                            <h2 class=\"text-uppercase\">Buat Akun Baru</h2>

                            <p class=\"lead\">Anda belum terdaftar?</p>
                            <p>Dengan mendaftar di Pindahan.com, dapatkan berbagai kemudahan dan kenyamanan saat pindahan.</p>

                            <hr>

                            ";
        // line 16
        $context["error"] = $this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "flashdata", array()), "msg_register", array());
        // line 17
        echo "                            ";
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 18
            echo "                            <div class=\"alert alert-danger alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <strong>Oops! Terjadi kesalahan:</strong>
                                <ul>
                                ";
            // line 22
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["error"]) ? $context["error"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
                // line 23
                echo "                                    <li>";
                echo twig_escape_filter($this->env, $context["e"], "html", null, true);
                echo "</li>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "                                </ul>
                            </div>
                            ";
        }
        // line 28
        echo "
                            <form action=\"";
        // line 29
        echo twig_escape_filter($this->env, site_url("account/do_register"), "html", null, true);
        echo "\" method=\"post\">
                                <div class=\"form-group\">
                                    <label for=\"name-login\">Nama Lengkap</label>
                                    <input type=\"text\" class=\"form-control\" id=\"name-login\" name=\"fullname\" required=\"\" value=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["old_value"]) ? $context["old_value"] : null), "fullname", array()), "html", null, true);
        echo "\">
                                </div>
                                <div class=\"form-group\">
                                    <label for=\"dob\">Tanggal Lahir</label>
                                    <input type=\"text\" class=\"form-control datetimepicker\" id=\"dob\" name=\"dob\" required=\"\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["old_value"]) ? $context["old_value"] : null), "dob", array()), "html", null, true);
        echo "\">
                                </div>
                                <div class=\"form-group\">
                                    <label for=\"email-login\">Email</label>
                                    <input type=\"email\" class=\"form-control\" id=\"email-login\" name=\"email\" required=\"\" value=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["old_value"]) ? $context["old_value"] : null), "email", array()), "html", null, true);
        echo "\">
                                </div>
                                <div class=\"form-group\">
                                    <label for=\"gender\">Gender</label>
                                    <select class=\"form-control\" name=\"gender\" id=\"gender\" required=\"\">
                                        <option value=\"0\">Pilih salah satu</option>
                                        <option value=\"1\" ";
        // line 46
        echo ((($this->getAttribute((isset($context["old_value"]) ? $context["old_value"] : null), "gender", array()) == 1)) ? ("selected") : (""));
        echo ">Pria</option>
                                        <option value=\"2\" ";
        // line 47
        echo ((($this->getAttribute((isset($context["old_value"]) ? $context["old_value"] : null), "gender", array()) == 2)) ? ("selected") : (""));
        echo ">Wanita</option>
                                    </select>
                                </div>
                                <div class=\"form-group\">
                                    <label for=\"password-login\">Password</label>
                                    <input type=\"password\" class=\"form-control\" id=\"password-login\" name=\"password\" required=\"\">
                                </div>
                                <div class=\"form-group\">
                                    <label for=\"password-login\">Ulangi Password</label>
                                    <input type=\"password\" class=\"form-control\" id=\"password-login\" name=\"password-confirm\" required=\"\">
                                </div>
                                <div class=\"text-center\">
                                    <button type=\"submit\" class=\"btn btn-template-main\"><i class=\"fa fa-user-md\"></i> Daftar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class=\"col-md-4\">
                        ";
        // line 65
        $this->env->loadTemplate("partials/login_form.html")->display($context);
        // line 66
        echo "                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
";
    }

    // line 75
    public function block_scripts($context, array $blocks = array())
    {
        // line 76
        echo "<script>
    \$(function () {
        \$('.datetimepicker').datetimepicker({
            format: 'L',
            viewMode: 'years'
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "account/register.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 76,  155 => 75,  143 => 66,  141 => 65,  120 => 47,  116 => 46,  107 => 40,  100 => 36,  93 => 32,  87 => 29,  84 => 28,  79 => 25,  70 => 23,  66 => 22,  60 => 18,  57 => 17,  55 => 16,  40 => 3,  37 => 2,  11 => 1,);
    }
}
