<?php

/* order/new_order/lokasi.html */
class __TwigTemplate_054784c228f5d9e65cc884a4783e089203f07a4d03434eeaa2f455388eff5e4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t\t\t\t\t<div class=\"col-md-10 col-md-offset-1\" id=\"checkout\">

                        <div class=\"box\">
                            <form method=\"post\" action=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("order/nextstep/harga"), "html", null, true);
        echo "\" id=\"formLokasi\">

                                <ul class=\"nav nav-pills nav-justified\">
                                    <li><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("order/new_order/umum"), "html", null, true);
        echo "\"><i class=\"fa fa-user\"></i><br>Info Umum</a>
                                    </li>
                                    <li class=\"active\"><a href=\"#\"><i class=\"fa fa-map-marker\"></i><br>Lokasi</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-money\"></i><br>Harga</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-truck\"></i><br>Furnitur</a>
                                    </li>
                                    <li class=\"disabled\"><a href=\"#\"><i class=\"fa fa-list-alt\"></i><br>Ringkasan</a>
                                    </li>
                                </ul>

                                <div class=\"content\">
                                    <div class=\"row\">
                                        <div class=\"form-group\">
\t\t\t\t\t\t\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t\t\t\t\t\t\t<input id=\"origin-input\" name=\"pickup_address\" class=\"controls\" type=\"text\" placeholder=\"Lokasi penjemputan\" onkeydown=\"if (event.keyCode == 13) return false\">
\t\t\t\t\t\t\t\t\t\t\t    <input id=\"destination-input\" name=\"destination_address\" class=\"controls\" type=\"text\" placeholder=\"Lokasi tujuan\"  onkeydown=\"if (event.keyCode == 13) return false\">

\t\t\t\t\t\t\t\t\t\t\t\t<div id=\"map\"></div>
\t\t\t\t\t\t\t\t\t\t\t\t<h5 id=\"distance-approx\">Anda belum memasukkan lokasi penjemputan dan tujuan.</h5>
                                                <input type=\"hidden\" id=\"distance\" name=\"distance\" value=\"\" required=\"\"></input>
                                                <input type=\"hidden\" id=\"start_location\" name=\"start_location\" value=\"\" required=\"\"></input>
\t\t\t\t\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"end_location\" name=\"end_location\" value=\"\" required=\"\"></input>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
                                    </div>
                                    <!-- /.row -->
                                    <div class=\"row\">
\t\t\t\t                        <div class=\"col-md-12\">
\t\t\t\t                            <div class=\"heading\">
\t\t\t\t                                <h3>Alamat Detail</h3>
\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"leads muted\">Masukkan nama jalan dan nomor rumah serta keterangan tambahan yang diperlukan.</p>
\t\t\t\t                            </div>
\t\t\t\t                        </div>
\t\t\t\t                    </div>
                                    <div class=\"row\">
                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"start_address\">Alamat penjemputan</label>
                                                <textarea class=\"form-control\" id=\"start_address\" name=\"start_address\" required=\"\"></textarea>
                                            </div>
                                        </div>

                                        <div class=\"col-sm-6\">
                                            <div class=\"form-group\">
                                                <label for=\"end_address\">Alamat tujuan</label>
                                                <textarea class=\"form-control\" id=\"end_address\" name=\"end_address\" required=\"\"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>

                                <div class=\"box-footer\">
                                    <div class=\"pull-left\">
                                        <a href=\"";
        // line 65
        echo twig_escape_filter($this->env, site_url("order/new_order/umum"), "html", null, true);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-chevron-left\"></i>Kembali ke Info Umum</a>
                                    </div>
                                    <div class=\"pull-right\">
                                        <button type=\"submit\" class=\"btn btn-template-main\">Lanjutkan ke Harga<i class=\"fa fa-chevron-right\"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->
";
    }

    // line 81
    public function block_scripts($context, array $blocks = array())
    {
        // line 82
        echo "
    <!-- gmaps -->

    <script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBBX9fsSNoG8GGq2lPmgw7Vw8ZU0hkjTGU&libraries=places&callback=initMap\"
        async defer></script>
\t<script src=\"";
        // line 87
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "frontend/js/maps.js\"></script>

    <!-- gmaps end -->

    <script>
        // jquery validate
        \$('#formLokasi').validate({
            ignore: [],
            messages: {
                distance: {
                    required: 'Harap isi prakiraan jarak terlebih dahulu.<br/>'
                },
                start_location: {
                    required: 'Harap pilih lokasi penjemputan pada peta.<br/>'
                },
                end_location: {
                    required: 'Harap pilih lokasi tujuan pada peta.<br/>'
                }
            }
        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "order/new_order/lokasi.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 87,  132 => 82,  129 => 81,  110 => 65,  51 => 9,  45 => 6,  40 => 3,  37 => 2,  11 => 1,);
    }
}
