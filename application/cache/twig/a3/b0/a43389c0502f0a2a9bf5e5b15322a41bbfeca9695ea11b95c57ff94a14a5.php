<?php

/* order/order_list.html */
class __TwigTemplate_a3b0a43389c0502f0a2a9bf5e5b15322a41bbfeca9695ea11b95c57ff94a14a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "                    <div class=\"col-md-9 clearfix\" id=\"customer-account\">
                        <p class=\"text-muted lead\">Ingin pindahan dalam waktu dekat? Segera <a href=\"";
        // line 4
        echo twig_escape_filter($this->env, site_url("order/new_order"), "html", null, true);
        echo "\">pesan pindahan</a> dari sekarang!</p>

                        <div class=\"box\">
                            ";
        // line 7
        $context["message"] = $this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "flashdata", array()), "msg", array());
        // line 8
        echo "                            ";
        if ((isset($context["message"]) ? $context["message"] : null)) {
            // line 9
            echo "                            <div class=\"alert alert-success alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\"><span>&times;</span></button>
                                ";
            // line 11
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
            echo "
                            </div>
                            ";
        }
        // line 14
        echo "
                            <div class=\"table-responsive\">
                                <table class=\"table table-hover\">
                                    <thead>
                                        <tr>
                                            <th class=\"text-center\">Pesanan</th>
                                            <th class=\"text-center\">Tanggal dan Waktu</th>
                                            <th class=\"text-center\">Total</th>
                                            <th class=\"text-center\">Status</th>
                                            <th class=\"text-center\">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["orders"]) ? $context["orders"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
            // line 28
            echo "                                        <tr>
                                            <th class=\"text-center\"># ";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "id", array()), "html", null, true);
            echo "</th>
                                            <td class=\"text-center\">";
            // line 30
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["order"], "move_datetime", array()), "d/m/Y - H:i"), "html", null, true);
            echo "</td>
                                            <td class=\"text-center\">Rp ";
            // line 31
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["order"], "price", array()), 0, "", "."), "html", null, true);
            echo ",-</td>
                                            <td class=\"text-center\">
                                                ";
            // line 33
            if (($this->getAttribute($context["order"], "payment_status", array()) == 0)) {
                // line 34
                echo "                                                <span class=\"label label-warning\">Tunggu konfirmasi DP</span>
                                                ";
            } elseif (($this->getAttribute(            // line 35
$context["order"], "payment_status", array()) == 1)) {
                // line 36
                echo "                                                <span class=\"label label-warning\">Telah konfirmasi DP</span>
                                                ";
            } elseif (($this->getAttribute(            // line 37
$context["order"], "payment_status", array()) == 2)) {
                // line 38
                echo "                                                <span class=\"label label-info\">DP Lunas</span>
                                                ";
            } elseif (($this->getAttribute(            // line 39
$context["order"], "payment_status", array()) == 3)) {
                // line 40
                echo "                                                <span class=\"label label-success\">Selesai</span>
                                                ";
            } elseif (($this->getAttribute(            // line 41
$context["order"], "payment_status", array()) == 4)) {
                // line 42
                echo "                                                <span class=\"label label-danger\">Dibatalkan</span>
                                                ";
            }
            // line 44
            echo "                                            </td>
                                            <td class=\"text-right\">
                                                ";
            // line 46
            if (($this->getAttribute($context["order"], "payment_status", array()) == 0)) {
                // line 47
                echo "                                                <a href=\"";
                echo twig_escape_filter($this->env, site_url("order/confirmation"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "id", array()), "html", null, true);
                echo "\" class=\"btn btn-template-main btn-sm\" title=\"Konfirmasi pembayaran\"><i class=\"fa fa-dollar\"></i></a>
                                                ";
            }
            // line 49
            echo "                                                <a href=\"";
            echo twig_escape_filter($this->env, site_url("order/detail"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-template-main btn-sm\" title=\"Detail Pesanan\"><i class=\"fa fa-list\"></i></a>
                                                ";
            // line 50
            if ((($this->getAttribute($context["order"], "payment_status", array()) == 3) && ($this->getAttribute($context["order"], "has_reviewed", array()) == 0))) {
                // line 51
                echo "                                                <a href=\"#\" data-toggle=\"modal\" data-target=\"#review-modal\" data-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "id", array()), "html", null, true);
                echo "\" class=\"btn btn-info btn-sm btn-review\" title=\"Beri penilaian dan tanggapan\"><i class=\"fa fa-comment-o\"></i></a>
                                                ";
            }
            // line 53
            echo "                                                ";
            if (($this->getAttribute($context["order"], "payment_status", array()) != 4)) {
                // line 54
                echo "                                                <a href=\"#\" data-toggle=\"modal\" data-target=\"#cancel-modal\" data-id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "id", array()), "html", null, true);
                echo "\" class=\"btn btn-danger btn-sm btn-cancel\"><i class=\"fa fa-times\"></i></a>
                                                ";
            }
            // line 56
            echo "                                            </td>
                                        </tr>
                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 59
        echo "                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->

                            <p class=\"text-muted lead\">Jika anda memiliki pertanyaan tentang pemesanan pindahan, harap hubungi kami.</p>
                        </div>

                    </div>
                    <!-- /.col-md-9 -->


    ";
        // line 71
        echo twig_include($this->env, $context, "partials/review_modal.html");
        echo "
    ";
        // line 72
        echo twig_include($this->env, $context, "partials/cancel_modal.html");
        echo "
";
    }

    // line 75
    public function block_scripts($context, array $blocks = array())
    {
        // line 76
        echo "<script type=\"text/javascript\">
    \$(document).ready(function(){
        \$('.btn-cancel').click(function(){
            var orderId = \$(this).data('id');
            \$('#order_id_cancel').val(orderId);
        });

        \$('.btn-review').click(function(){
            var orderId = \$(this).data('id');
            \$('#order_id_review').val(orderId);
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "order/order_list.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 76,  195 => 75,  189 => 72,  185 => 71,  171 => 59,  163 => 56,  157 => 54,  154 => 53,  148 => 51,  146 => 50,  139 => 49,  131 => 47,  129 => 46,  125 => 44,  121 => 42,  119 => 41,  116 => 40,  114 => 39,  111 => 38,  109 => 37,  106 => 36,  104 => 35,  101 => 34,  99 => 33,  94 => 31,  90 => 30,  86 => 29,  83 => 28,  79 => 27,  64 => 14,  58 => 11,  54 => 9,  51 => 8,  49 => 7,  43 => 4,  40 => 3,  37 => 2,  11 => 1,);
    }
}
