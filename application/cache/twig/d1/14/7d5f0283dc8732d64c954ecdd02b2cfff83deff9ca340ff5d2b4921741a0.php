<?php

/* partials/styles.html */
class __TwigTemplate_d1147d5f0283dc8732d64c954ecdd02b2cfff83deff9ca340ff5d2b4921741a0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css\">
    <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css\">

    <!-- Css animations  -->
    <link href=\"";
        // line 9
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/css/animate.css\" rel=\"stylesheet\">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/css/style.blue.css\" rel=\"stylesheet\" id=\"theme-stylesheet\">

    <!-- Custom stylesheet - for your changes -->
    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/css/custom.css\" rel=\"stylesheet\">

    <!-- Responsivity for older IE -->
    <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
<![endif]-->

    <!-- Favicon and apple touch icons-->
    <link rel=\"shortcut icon\" href=\"img/favicon.ico\" type=\"image/x-icon\" />
    <link rel=\"apple-touch-icon\" href=\"img/apple-touch-icon.png\" />
    <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"img/apple-touch-icon-57x57.png\" />
    <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"img/apple-touch-icon-72x72.png\" />
    <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"img/apple-touch-icon-76x76.png\" />
    <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"img/apple-touch-icon-114x114.png\" />
    <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"img/apple-touch-icon-120x120.png\" />
    <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"img/apple-touch-icon-144x144.png\" />
    <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"img/apple-touch-icon-152x152.png\" />
    <!-- owl carousel css -->

    <link href=\"";
        // line 35
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/css/owl.carousel.css\" rel=\"stylesheet\">
    <link href=\"";
        // line 36
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/css/owl.theme.css\" rel=\"stylesheet\">

    <!-- Bootstrap Datetime Picker -->
    <link rel=\"stylesheet\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, bower_url(), "html", null, true);
        echo "/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css\" />";
    }

    public function getTemplateName()
    {
        return "partials/styles.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 39,  68 => 36,  64 => 35,  41 => 15,  35 => 12,  29 => 9,  19 => 1,);
    }
}
