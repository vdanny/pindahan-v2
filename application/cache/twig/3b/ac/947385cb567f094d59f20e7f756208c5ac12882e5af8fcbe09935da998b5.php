<?php

/* home/index.html */
class __TwigTemplate_3bac947385cb567f094d59f20e7f756208c5ac12882e5af8fcbe09935da998b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/landing.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/landing.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "        <section>
            <!-- *** HOMEPAGE CAROUSEL ***
            }
 _________________________________________________________ -->

            <div class=\"home-carousel\" id=\"home\">

                <div class=\"dark-mask\"></div>

                <div class=\"container\">
                    <div class=\"homepage owl-carousel\">
                        <div class=\"item\">
                            <div class=\"row\">
                                <div class=\"col-sm-5 right\">
                                    <h1>Layanan pindahan terpercaya untuk</h1>
                                    <ul class=\"list-style-none\">
                                        <li>Kos</li>
                                    </ul>
                                </div>
                                <div class=\"col-sm-7\">
                                    <img class=\"img-responsive\" src=\"";
        // line 23
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/img/template-homepage.png\" alt=\"\">
                                </div>
                            </div>
                        </div>
                        <div class=\"item\">
                            <div class=\"row\">

                                <div class=\"col-sm-7 text-center\">
                                    <img class=\"img-responsive\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/img/template-mac.png\" alt=\"\">
                                </div>

                                <div class=\"col-sm-5\">
                                    <h2>Mengapa pakai Pindahan.com?</h2>
                                    <ul class=\"list-style-none\">
                                        <li>Waktu pindahan yang fleksibel</li>
                                        <li>Harga pindahan yang terjangkau</li>
                                        <li>Pindahan dengan aman dan nyaman</li>
                                        <li>Pemesanan online yang praktis dan informatif</li>
                                        <li>Pekerja yang handal dan terampil</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class=\"item\">
                            <div class=\"row\">
                                <div class=\"col-sm-5 right\">
                                    <h1>Daftar sekarang juga</h1>
                                    <a class=\"btn btn-lg btn-template-transparent-primary\" href=\"";
        // line 51
        echo twig_escape_filter($this->env, site_url("account/register"), "html", null, true);
        echo "\">Daftar</a>
                                </div>
                                <div class=\"col-sm-7\">
                                    <img class=\"img-responsive\" src=\"";
        // line 54
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/img/template-easy-customize.png\" alt=\"\">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.project owl-slider -->
                </div>
            </div>

            <!-- *** HOMEPAGE CAROUSEL END *** -->
        </section>

        <!-- *** SERVICE SECTION ***
 _________________________________________________________ -->

        <section class=\"bar background-white\" id=\"service\">
            <div class=\"container\">
                <div class=\"col-md-12\">

                    <div class=\"heading text-center\">
                        <h2>Layanan Kami</h2>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-4\">
                            <div class=\"box-simple\">
                                <div class=\"icon\">
                                    <i class=\"fa fa-truck fa-2x\"></i>
                                </div>
                                <h3>Transportasi</h3>
                                <p>Layanan transportasi menuju tempat baru yang Anda tuju.</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"box-simple\">
                                <div class=\"icon\">
                                    <i class=\"fa fa-cube fa-2x\"></i>
                                </div>
                                <h3>Packing</h3>
                                <p>Bantuan pengemasan barang-barang yang akan dipindahkan oleh tenaga professional.</p>
                            </div>
                        </div>
                        <div class=\"col-md-4\">
                            <div class=\"box-simple\">
                                <div class=\"icon\">
                                    <i class=\"fa fa-dropbox fa-2x\"></i>
                                </div>
                                <h3>Unpacking & Penataan</h3>
                                <p>Bantuan dalam menata kembali barang-barang Anda di tempat yang baru.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- *** SERVICE SECTION END ***
 _________________________________________________________ -->
        <!-- *** VIDEO SECTION ***
 _________________________________________________________ -->

        <section class=\"bar background-gray\" id=\"service\">
            <div class=\"container\">
                <div class=\"col-md-12\">

                    <div class=\"heading text-center\">
                        <h2>cara pesan pindahan</h2>
                    </div>

                    <div class=\"row\">
                        <div class=\"col-md-6 text-center\">
                            <iframe style=\"width:100%;\" src=\"http://www.youtube.com/embed/YGWdQ5BtyVU\">
                            </iframe> 
                        </div>
                        <div class=\"col-md-6 text-center\">
                            <iframe style=\"width:100%;\" src=\"http://www.youtube.com/embed/YGWdQ5BtyVU\">
                            </iframe> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- *** VIDEO SECTION END ***
 _________________________________________________________ -->

        <!-- *** PACKAGES SECTION ***
 _________________________________________________________ -->

        <section class=\"bar background-white no-mb\" id=\"price\">
                <div class=\"container\">
                    <div class=\"col-md-12\">
                        <div class=\"heading text-center\">
                            <h2>Paket Harga Kami</h2>
                        </div>
                        <p class=\"lead text-center\">Pilihan dari berbagai paket dengan harga terbaik</p>

                        <div class=\"row packages\">

                            <div class=\"row packages\" id=\"package\">
                                    ";
        // line 152
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["packages"]) ? $context["packages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["package"]) {
            // line 153
            echo "                                        ";
            // line 154
            echo "                                        ";
            $context["divBest"] = "<div class=\"best-value\">";
            // line 155
            echo "                                        ";
            $context["divBestClose"] = "</div>";
            // line 156
            echo "                                        ";
            $context["metaBest"] = "<div class=\"meta-text\">Best Value</div>";
            // line 157
            echo "                                        ";
            $context["notavail"] = "<i class=\"fa fa-times\"></i>";
            // line 158
            echo "                                        <div class=\"col-md-3\">
                                            ";
            // line 159
            echo (($this->getAttribute($context["package"], "is_best_offer", array())) ? ((isset($context["divBest"]) ? $context["divBest"] : null)) : (""));
            echo "
                                            <div class=\"package \">
                                                <div class=\"package-header\">
                                                    <h5>";
            // line 162
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "name", array()), "html", null, true);
            echo "</h5>
                                                    ";
            // line 163
            echo (($this->getAttribute($context["package"], "is_best_offer", array())) ? ((isset($context["metaBest"]) ? $context["metaBest"] : null)) : (""));
            echo "
                                                </div>
                                                <div class=\"price\">
                                                    <div class=\"price-container\">
                                                        <h4>Rp ";
            // line 167
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($context["package"], "price", array()), 0, "", "."), "html", null, true);
            echo "</h4>
                                                    </div>
                                                </div>
                                                <ul>
                                                    <li><i class=\"fa fa-check\"></i>";
            // line 171
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "transportation", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["package"], "vehicle", array()), "name", array()), "html", null, true);
            echo "</li>
                                                    <li><i class=\"fa fa-check\"></i>Gratis ";
            // line 172
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "distance", array()), "html", null, true);
            echo " km pertama</li>
                                                    <li>";
            // line 173
            echo ((($this->getAttribute($context["package"], "employee", array()) > 0)) ? ($this->getAttribute($context["package"], "employee", array())) : ((isset($context["notavail"]) ? $context["notavail"] : null)));
            echo " Tenaga kerja</li>
                                                    <li>";
            // line 174
            echo ((($this->getAttribute($context["package"], "box", array()) > 0)) ? ($this->getAttribute($context["package"], "box", array())) : ((isset($context["notavail"]) ? $context["notavail"] : null)));
            echo " Kardus</li>
                                                </ul>
                                                <a class=\"btn btn-template-main\" id=\"btn-";
            // line 176
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
            echo "\" onclick=\"selectPackage(";
            echo twig_escape_filter($this->env, $this->getAttribute($context["package"], "id", array()), "html", null, true);
            echo ")\">Pilih</a>
                                            </div>
                                            ";
            // line 178
            echo (($this->getAttribute($context["package"], "is_best_offer", array())) ? ((isset($context["divBestClose"]) ? $context["divBestClose"] : null)) : (""));
            echo "
                                        </div>
                                    ";
            // line 181
            echo "                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['package'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 182
        echo "                            </div>
                        </div>
                    </div>
                </div>
        </section>

        <!-- *** PACKAGES SECTION END ***
 _________________________________________________________ -->


        <!-- *** CUSTOM SECTION ***
 _________________________________________________________ -->

        <section class=\"bar background-image-fixed-2 no-mb color-white text-center\" id=\"custom\">
            <div class=\"dark-mask\"></div>
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <div class=\"icon icon-lg\"><i class=\"fa fa-gears\"></i>
                        </div>
                        <h3 class=\"text-uppercase\">Paket tidak sesuai kebutuhan?</h3>
                        <p class=\"lead\">Pilih kendaraan dan jumlah pekerja sesuai kebutuhan dan keinginan anda.</p>
                        <p class=\"text-center\">
                            <a href=\"";
        // line 205
        echo twig_escape_filter($this->env, site_url("home/custom"), "html", null, true);
        echo "\" class=\"btn btn-template-transparent-black btn-lg\">Buat Harga Sendiri</a>
                        </p>
                    </div>

                </div>
            </div>
        </section>
        <!-- *** CUSTOM SECTION END ***
 _________________________________________________________ -->


        <!-- *** PROMO SECTION ***
 _________________________________________________________ -->

        <section class=\"bar background-white no-mb\" id=\"price\">
                <div class=\"container\">
                    <div class=\"col-md-12\">
                        <div class=\"heading text-center\">
                            <h2>Promosi</h2>
                        </div>

                        
                        <div class=\"promotions owl-carousel\">
                            ";
        // line 228
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promotions"]) ? $context["promotions"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["promotion"]) {
            // line 229
            echo "                            <img src=\"";
            echo twig_escape_filter($this->env, upload_url("promo"), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["promotion"], "image_path", array()), "html", null, true);
            echo "\" alt=\"image not found\" style=\"width: 100%;\">
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promotion'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 231
        echo "                        </div>
                    </div>
                </div>
        </section>

        <!-- *** PROMO SECTION END ***
 _________________________________________________________ -->

        <section class=\"bar background-pentagon no-mb\" id=\"testimonial\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <div class=\"heading text-center\">
                            <h2>Testimonial</h2>
                        </div>

                        <p class=\"lead\">Kami telah bekerja dengan banyak klien dan kami selalu ingin mendengar pindahan mereka selesai dengan memuaskan.</p>


                        <!-- *** TESTIMONIALS CAROUSEL ***
 _________________________________________________________ -->

                        <ul class=\"owl-carousel testimonials same-height-row\">
                            ";
        // line 254
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["testimonials"]) ? $context["testimonials"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["testimonial"]) {
            // line 255
            echo "                            <li class=\"item\">
                                <div class=\"testimonial same-height-always\">
                                    <div class=\"text\">
                                        <p>";
            // line 258
            echo twig_escape_filter($this->env, $this->getAttribute($context["testimonial"], "comment", array()), "html", null, true);
            echo "</p>
                                    </div>
                                    <div class=\"bottom\">
                                        <div class=\"icon\"><i class=\"fa fa-quote-left\"></i>
                                        </div>
                                        <div class=\"name-picture\">
                                            <h5>&nbsp;</h5>
                                            <h5>";
            // line 265
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["testimonial"], "user", array()), "fullname", array()), "html", null, true);
            echo "</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['testimonial'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 271
        echo "                            <!-- <li class=\"item\">
                                <div class=\"testimonial same-height-always\">
                                    <div class=\"text\">
                                        <p>The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. \"What's happened to
                                            me? \" he thought. It wasn't a dream.</p>
                                    </div>
                                    <div class=\"bottom\">
                                        <div class=\"icon\"><i class=\"fa fa-quote-left\"></i>
                                        </div>
                                        <div class=\"name-picture\">
                                            <h5>&nbsp;</h5>
                                            <h5>John McIntyre</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class=\"item\">
                                <div class=\"testimonial same-height-always\">
                                    <div class=\"text\">
                                        <p>His room, a proper human room although a little too small, lay peacefully between its four familiar walls.</p>

                                        <p>A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded
                                            frame.</p>
                                    </div>
                                    <div class=\"bottom\">
                                        <div class=\"icon\"><i class=\"fa fa-quote-left\"></i>
                                        </div>
                                        <div class=\"name-picture\">
                                            <h5>&nbsp;</h5>
                                            <h5>John McIntyre</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class=\"item\">
                                <div class=\"testimonial same-height-always\">
                                    <div class=\"text\">
                                        <p>It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull
                                            weather. Drops of rain could be heard hitting the pane, which made him feel quite sad.</p>
                                    </div>

                                    <div class=\"bottom\">
                                        <div class=\"icon\"><i class=\"fa fa-quote-left\"></i>
                                        </div>
                                        <div class=\"name-picture\">
                                            <h5>&nbsp;</h5>
                                            <h5>John McIntyre</h5>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class=\"item\">
                                <div class=\"testimonial same-height-always\">
                                    <div class=\"text\">
                                        <p>It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull
                                            weather. Drops of rain could be heard hitting the pane, which made him feel quite sad. Gregor then turned to look out the window at the dull weather. Drops of rain could be heard hitting the pane, which made
                                            him feel quite sad.</p>
                                    </div>

                                    <div class=\"bottom\">
                                        <div class=\"icon\"><i class=\"fa fa-quote-left\"></i>
                                        </div>
                                        <div class=\"name-picture\">
                                            <h5>&nbsp;</h5>
                                            <h5>John McIntyre</h5>
                                        </div>
                                    </div>
                                </div>
                            </li> -->
                        </ul>
                        <!-- /.owl-carousel -->

                        <!-- *** TESTIMONIALS CAROUSEL END *** -->
                    </div>

                </div>
            </div>
        </section>
        <!-- /.bar -->

        <!-- *** COMPANIES SECTION ***
_________________________________________________________ -->
        <section class=\"bar background-gray no-mb\" id=\"partner\">
            <div class=\"container\">

                <div class=\"row\">
                    <div class=\"col-md-12\">
                        <div class=\"heading text-center\">
                            <h2>Partner Kami</h2>
                        </div>
                        <ul class=\"owl-carousel customers\">
                            <li class=\"item\">
                                <img src=\"";
        // line 363
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "frontend/img/BCA logo.png\" alt=\"\" class=\"img-responsive\">
                            </li>
                            <li class=\"item\">
                                <img src=\"";
        // line 366
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "frontend/img/BNI logo.png\" alt=\"\" class=\"img-responsive\">
                            </li>
                            <li class=\"item\">
                                <img src=\"";
        // line 369
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "frontend/img/mandiri logo.png\" alt=\"\" class=\"img-responsive\">
                            </li>
                        </ul>
                        <!-- /.owl-carousel -->
                    </div>

                </div>
            </div>
        </section>
        <!-- *** COMPANIES SECTION END ***
_________________________________________________________ -->

        <!-- *** GET IT ***
_________________________________________________________ -->

        <div id=\"get-it\">
            <div class=\"container\">
                <div class=\"col-md-8 col-sm-12\">
                    <h3>Ingin pindahan sekarang juga?</h3>
                </div>
                <div class=\"col-md-4 col-sm-12\">
                    <a href=\"#\" class=\"btn btn-lg btn-template-transparent-primary\">Daftar Sekarang</a>
                </div>
            </div>
        </div>
";
    }

    // line 395
    public function block_scripts($context, array $blocks = array())
    {
        // line 396
        echo "<script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "frontend/js/fluid-youtube.js\"></script>
<script type=\"text/javascript\">
    \$(document).ready(function(){
        \$('.promotions').owlCarousel();
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "home/index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  546 => 396,  543 => 395,  513 => 369,  507 => 366,  501 => 363,  407 => 271,  395 => 265,  385 => 258,  380 => 255,  376 => 254,  351 => 231,  340 => 229,  336 => 228,  310 => 205,  285 => 182,  279 => 181,  274 => 178,  267 => 176,  262 => 174,  258 => 173,  254 => 172,  248 => 171,  241 => 167,  234 => 163,  230 => 162,  224 => 159,  221 => 158,  218 => 157,  215 => 156,  212 => 155,  209 => 154,  207 => 153,  203 => 152,  102 => 54,  96 => 51,  73 => 31,  62 => 23,  40 => 3,  37 => 2,  11 => 1,);
    }
}
