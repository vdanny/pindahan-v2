<?php

/* order/new_order/checkout.html */
class __TwigTemplate_3fc6703b68c5bc8646d496ae077b01ff0c08e522024e949061ea078e755caca5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t\t\t\t\t<div class=\"col-md-10 col-md-offset-1\" id=\"checkout\">

                        <div class=\"box\">

                            <div class=\"content\">
                                <div class=\"row text-center\">
                                    <div class=\"col-sm-12\">
                                        <h2>PEMBAYARAN PESANAN PINDAHAN</h2>
                                        <p class=\"text-muted\">Harap melakukan pembayaran DP sebesar 50% selama kurang dari 24 jam ke rekening bank berikut.</p>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class=\"row\">
                                    <div class=\"col-sm-6 col-sm-offset-3\">
                                        <div class=\"table-responsive\">
                                            <table class=\"table\">
                                                <tr class=\"total\">
                                                    <th>Total Transfer (DP 50%)</th>
                                                    <th>Rp ";
        // line 21
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["dp"]) ? $context["dp"] : null), 0, "", "."), "html", null, true);
        echo ",-</th>
                                                </tr>
                                                <tr>
                                                    <td>Batas waktu</td>
                                                    <th>";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["max_pay_date"]) ? $context["max_pay_date"] : null), "html", null, true);
        echo "</th>
                                                </tr>
                                                <tr>
                                                    <td colspan=\"2\">Hubungi kontak kami jika anda mempunyai pertanyaan atau mengalami masalah saat melakukan pembayaran.</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class=\"row text-center\">
                                    <h3>Silahkan transfer ke salah satu dari bank yang tersedia.</h3>
                                    ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banks"]) ? $context["banks"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["bank"]) {
            // line 38
            echo "                                    <div class=\"col-sm-4\">
                                        <table class=\"table\">
                                            <tr>
                                                <td><b>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($context["bank"], "bank_name", array()), "html", null, true);
            echo "</b></td>
                                            </tr>
                                            <tr>
                                                <td><b>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["bank"], "account_name", array()), "html", null, true);
            echo "</b></td>
                                            </tr>
                                            <tr>
                                                <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["bank"], "account_no", array()), "html", null, true);
            echo "</td>
                                            </tr>
                                        </table>
                                    </div>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['bank'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                                </div>
                            </div>

                            <div class=\"box-footer\">
                                <div class=\"text-center\">
                                    <a href=\"";
        // line 57
        echo twig_escape_filter($this->env, site_url("order/confirmation"), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["order_id"]) ? $context["order_id"] : null), "html", null, true);
        echo "\" class=\"btn btn-template-main text-center\"><i class=\"fa fa-dollar\"></i> Konfirmasi Pembayaran</a>
                                    <a href=\"";
        // line 58
        echo twig_escape_filter($this->env, site_url("order/order_list"), "html", null, true);
        echo "\" class=\"btn btn-template-main text-center\"><i class=\"fa fa-home\"></i> Kembali ke Daftar Pesanan</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->

";
    }

    // line 70
    public function block_scripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "order/new_order/checkout.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 70,  127 => 58,  121 => 57,  114 => 52,  103 => 47,  97 => 44,  91 => 41,  86 => 38,  82 => 37,  67 => 25,  60 => 21,  40 => 3,  37 => 2,  11 => 1,);
    }
}
