<?php

/* order/detail.html */
class __TwigTemplate_a614c9bc929e1c56ba6adb97ee561601213f9d650531f73d0bcb39a8d2d77383 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t\t\t\t\t<div class=\"col-md-9\">

                        <div class=\"box\">

                            <div class=\"content\">
                                <div class=\"row\">
                                    <div class=\"col-sm-12\">
                                        <h2>DETAIL PESANAN ANDA</h2>
                                        <p class=\"text-muted\">Informasi detail mengenai pesanan pindahan anda.</p>
                                        <h4>
                                            Status: 
                                            ";
        // line 14
        if (($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "payment_status", array()) == 0)) {
            // line 15
            echo "                                                Tunggu konfirmasi DP
                                            ";
        } elseif (($this->getAttribute(        // line 16
(isset($context["order"]) ? $context["order"] : null), "payment_status", array()) == 1)) {
            // line 17
            echo "                                                Telah konfirmasi DP
                                            ";
        } elseif (($this->getAttribute(        // line 18
(isset($context["order"]) ? $context["order"] : null), "payment_status", array()) == 2)) {
            // line 19
            echo "                                                DP Lunas
                                            ";
        } elseif (($this->getAttribute(        // line 20
(isset($context["order"]) ? $context["order"] : null), "payment_status", array()) == 3)) {
            // line 21
            echo "                                                Selesai
                                            ";
        } elseif (($this->getAttribute(        // line 22
(isset($context["order"]) ? $context["order"] : null), "payment_status", array()) == 4)) {
            // line 23
            echo "                                                Dibatalkan
                                            ";
        }
        // line 25
        echo "                                            <div class=\"pull-right\">
                                                <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, site_url("order/order_list"), "html", null, true);
        echo "\" class=\"btn\"><i class=\"fa fa-chevron-left\"></i>Kembali</a>
                                            </div>
                                        </h4>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class=\"row\">

                                    <div class=\"col-sm-6\">
                                        <div class=\"table-responsive\">
                                            <h4>Info Umum</h4>
                                            <table class=\"table\">
                                                <tbody>
                                                    <tr>
                                                        <td>Nama pemesan</td>
                                                        <th>";
        // line 41
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "fullname", array()), "html", null, true);
        echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Nomor HP</td>
                                                        <th>";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "phone", array()), "html", null, true);
        echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Pindahan</td>
                                                        <th>";
        // line 49
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "move_datetime", array()), "j F Y"), "html", null, true);
        echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Waktu Pindahan</td>
                                                        <th>";
        // line 53
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "move_datetime", array()), "H:i"), "html", null, true);
        echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Catatan Tambahan</td>
                                                        <th>";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "note", array()), "html", null, true);
        echo "</th>
                                                    </tr>
                                                    ";
        // line 59
        if ($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "blueprint", array())) {
            // line 60
            echo "                                                    <tr>
                                                        <td>Denah Ruangan Lokasi Tujuan</td>
                                                        <th>
                                                            <a href=\"";
            // line 63
            echo twig_escape_filter($this->env, upload_url(("temp" . $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "blueprint", array()))), "html", null, true);
            echo "\">Lihat gambar di sini</a>
                                                        </th>
                                                    </tr>
                                                    ";
        }
        // line 67
        echo "                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class=\"col-sm-6\">
                                        <div class=\"table-responsive\">
                                            <h4>Lokasi</h4>
                                            <table class=\"table\">
                                                <tbody>
                                                    <tr>
                                                        <th>Alamat penjemputan</th>
                                                    </tr>
                                                    <tr>
                                                        <td>";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "start_address", array()), "html", null, true);
        echo "</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Alamat penjemputan</th>
                                                    </tr>
                                                    <tr>
                                                        <td>";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "end_address", array()), "html", null, true);
        echo "</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                                <hr>
                                <div class=\"row\">
                                    
                                    <div class=\"col-sm-6\">
                                        <div class=\"table-responsive\">
                                            <h4>Furnitur</h4>
                                            <p>Volume total: ";
        // line 101
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "total_volume", array()), "html", null, true);
        echo " m<sup>3</sup></p>
                                            <table class=\"table\">
                                                <tbody>
                                                    <tr>
                                                        <th>Jenis</th>
                                                        <th>Ukuran</th>
                                                        <th>Jumlah</th>
                                                    </tr>
                                                    ";
        // line 109
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "furnitures", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["furniture"]) {
            // line 110
            echo "                                                    <tr>
                                                        <td>";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute($context["furniture"], "name", array()), "html", null, true);
            echo "</td>
                                                        <td>
                                                            ";
            // line 113
            if (($this->getAttribute($context["furniture"], "size_id", array()) != 0)) {
                // line 114
                echo "                                                                ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["furniture"], "size", array()), "html", null, true);
                echo "
                                                            ";
            } else {
                // line 116
                echo "                                                                Custom (";
                echo twig_escape_filter($this->env, $this->getAttribute($context["furniture"], "description", array()), "html", null, true);
                echo ")
                                                            ";
            }
            // line 118
            echo "                                                        </td>
                                                        <td>";
            // line 119
            echo twig_escape_filter($this->env, $this->getAttribute($context["furniture"], "qty", array()), "html", null, true);
            echo "</td>
                                                    </tr>
                                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['furniture'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 122
        echo "                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class=\"col-sm-6\">
                                        <div class=\"table-responsive\">
                                            <h4>Harga</h4>
                                            <table class=\"table\">
                                            ";
        // line 131
        if (($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package_id", array()) != 0)) {
            // line 132
            echo "                                                ";
            $context["order_dist"] = ($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "distance", array()) / 1000);
            // line 133
            echo "                                                ";
            $context["paid_dist"] = ((((isset($context["order_dist"]) ? $context["order_dist"] : null) > ($this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "distance", array()) * 1000))) ? (((isset($context["order_dist"]) ? $context["order_dist"] : null) - ($this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "distance", array()) * 1000))) : (0));
            // line 134
            echo "                                                <tbody>
                                                    <tr>
                                                        <td>Pilihan Harga</td>
                                                        <th>Paket / <del>Custom</del></th>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenis Paket</td>
                                                        <th>";
            // line 141
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "name", array()), "html", null, true);
            echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Transportasi</td>
                                                        <th>";
            // line 145
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "transportation", array()), "html", null, true);
            echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Pekerja</td>
                                                        <th>";
            // line 149
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "employee", array()), "html", null, true);
            echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Kardus</td>
                                                        <th>";
            // line 153
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "box", array()), "html", null, true);
            echo "</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Jarak Tempuh</td>
                                                        <th>";
            // line 157
            echo twig_escape_filter($this->env, (isset($context["order_dist"]) ? $context["order_dist"] : null), "html", null, true);
            echo " km (gratis ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "distance", array()), "html", null, true);
            echo " km)</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Estimasi waktu</td>
                                                        <th>";
            // line 161
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "duration", array()), "html", null, true);
            echo " jam</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Biaya Ongkos (Rp ";
            // line 164
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["transport_fee"]) ? $context["transport_fee"] : null), 0, "", "."), "html", null, true);
            echo ",-)</td>
                                                        <th>Rp ";
            // line 165
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["paid_dist"]) ? $context["paid_dist"] : null) * (isset($context["transport_fee"]) ? $context["transport_fee"] : null)), 0, "", "."), "html", null, true);
            echo ",- <span class=\"text-muted\">(";
            echo twig_escape_filter($this->env, (isset($context["paid_dist"]) ? $context["paid_dist"] : null), "html", null, true);
            echo " km)</span></th>
                                                    </tr>
                                                    <tr>
                                                        <td>Biaya Paket</td>
                                                        <th>Rp ";
            // line 169
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "package", array()), "price", array()), 0, "", "."), "html", null, true);
            echo ",-</th>
                                                    </tr>
                                                    <tr class=\"total\">
                                                        <td>Total Dibayarkan</td>
                                                        <th>Rp ";
            // line 173
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "price", array()) + ((isset($context["paid_dist"]) ? $context["paid_dist"] : null) * (isset($context["transport_fee"]) ? $context["transport_fee"] : null))), 0, "", "."), "html", null, true);
            echo ",-</th>
                                                    </tr>
                                                    ";
            // line 175
            if ((isset($context["higherPrice"]) ? $context["higherPrice"] : null)) {
                // line 176
                echo "                                                    <tr>
                                                        <td colspan=\"2\"><strong>Perhatian </strong>Total biaya anda dikenakan charga 50%</td>
                                                    </tr>
                                                    ";
            }
            // line 180
            echo "                                                </tbody>
                                            ";
        } else {
            // line 182
            echo "                                                ";
            $context["order_dist"] = ($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "distance", array()) / 1000);
            // line 183
            echo "                                                ";
            $context["paid_dist"] = (isset($context["order_dist"]) ? $context["order_dist"] : null);
            // line 184
            echo "                                                <tbody>
                                                    <tr>
                                                        <td>Pilihan Harga</td>
                                                        <th><del>Paket</del> / Custom</th>
                                                    </tr>
                                                    ";
            // line 189
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "customs", array())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 190
                echo "                                                    <tr>
                                                        <td>";
                // line 191
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "customs", array()), $context["i"], array(), "array"), "name", array()), "html", null, true);
                echo "</td>
                                                        <td>";
                // line 192
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "customs", array()), $context["i"], array(), "array"), "qty", array()), "html", null, true);
                echo "</td>
                                                    </tr>
                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 195
            echo "                                                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "extras", array())) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 196
                echo "                                                    <tr>
                                                        <td>";
                // line 197
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "extras", array()), $context["i"], array(), "array"), "name", array()), "html", null, true);
                echo "</td>
                                                        <td>";
                // line 198
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "extras", array()), $context["i"], array(), "array"), "qty", array()), "html", null, true);
                echo "</td>
                                                    </tr>
                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 201
            echo "                                                    <tr>
                                                        <td>Jarak Tempuh</td>
                                                        <th>";
            // line 203
            echo twig_escape_filter($this->env, (isset($context["order_dist"]) ? $context["order_dist"] : null), "html", null, true);
            echo " km</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Estimasi waktu</td>
                                                        <th>";
            // line 207
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "duration", array()), "html", null, true);
            echo " jam</th>
                                                    </tr>
                                                    <tr>
                                                        <td>Biaya Ongkos (Rp ";
            // line 210
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["transport_fee"]) ? $context["transport_fee"] : null), 0, "", "."), "html", null, true);
            echo ",-)</td>
                                                        <th>Rp ";
            // line 211
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["paid_dist"]) ? $context["paid_dist"] : null) * (isset($context["transport_fee"]) ? $context["transport_fee"] : null)), 0, "", "."), "html", null, true);
            echo ",- <span class=\"text-muted\">(";
            echo twig_escape_filter($this->env, (isset($context["paid_dist"]) ? $context["paid_dist"] : null), "html", null, true);
            echo " km)</span></th>
                                                    </tr>
                                                    <tr>
                                                        <td>Biaya Custom</td>
                                                        <th>Rp ";
            // line 215
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($this->getAttribute((isset($context["order"]) ? $context["order"] : null), "price", array()) - ((isset($context["paid_dist"]) ? $context["paid_dist"] : null) * (isset($context["transport_fee"]) ? $context["transport_fee"] : null))), 0, "", "."), "html", null, true);
            echo ",-</th>
                                                    </tr>
                                                    <tr class=\"total\">
                                                        <td>Total Dibayarkan</td>
                                                        <th>Rp ";
            // line 219
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["order"]) ? $context["order"] : null), "price", array()), 0, "", "."), "html", null, true);
            echo ",-</th>
                                                    </tr>
                                                    ";
            // line 221
            if ((isset($context["higherPrice"]) ? $context["higherPrice"] : null)) {
                // line 222
                echo "                                                    <tr>
                                                        <td colspan=\"2\"><strong>Perhatian </strong>Total biaya anda dikenakan charga 50%</td>
                                                    </tr>
                                                    ";
            }
            // line 226
            echo "                                                </tbody>
                                            ";
        }
        // line 228
        echo "                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class=\"box-footer\">
                                <div class=\"pull-left\">
                                    <a href=\"";
        // line 236
        echo twig_escape_filter($this->env, site_url("order/order_list"), "html", null, true);
        echo "\" class=\"btn\"><i class=\"fa fa-chevron-left\"></i>Kembali</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->

";
    }

    // line 248
    public function block_scripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "order/detail.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  479 => 248,  464 => 236,  454 => 228,  450 => 226,  444 => 222,  442 => 221,  437 => 219,  430 => 215,  421 => 211,  417 => 210,  411 => 207,  404 => 203,  400 => 201,  391 => 198,  387 => 197,  384 => 196,  379 => 195,  370 => 192,  366 => 191,  363 => 190,  359 => 189,  352 => 184,  349 => 183,  346 => 182,  342 => 180,  336 => 176,  334 => 175,  329 => 173,  322 => 169,  313 => 165,  309 => 164,  303 => 161,  294 => 157,  287 => 153,  280 => 149,  273 => 145,  266 => 141,  257 => 134,  254 => 133,  251 => 132,  249 => 131,  238 => 122,  229 => 119,  226 => 118,  220 => 116,  214 => 114,  212 => 113,  207 => 111,  204 => 110,  200 => 109,  189 => 101,  172 => 87,  163 => 81,  147 => 67,  140 => 63,  135 => 60,  133 => 59,  128 => 57,  121 => 53,  114 => 49,  107 => 45,  100 => 41,  82 => 26,  79 => 25,  75 => 23,  73 => 22,  70 => 21,  68 => 20,  65 => 19,  63 => 18,  60 => 17,  58 => 16,  55 => 15,  53 => 14,  40 => 3,  37 => 2,  11 => 1,);
    }
}
