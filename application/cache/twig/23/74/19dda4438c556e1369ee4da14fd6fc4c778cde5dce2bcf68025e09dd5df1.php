<?php

/* order/new_order/confirmation.html */
class __TwigTemplate_237419dda4438c556e1369ee4da14fd6fc4c778cde5dce2bcf68025e09dd5df1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "\t\t\t\t\t<div class=\"col-md-10 col-md-offset-1\" id=\"checkout\">

                        <div class=\"box\">

                            <div class=\"content\">
                                <div class=\"row text-center\">
                                    <div class=\"col-sm-12\">
                                        <h2>KONFIRMASI PEMBAYARAN PESANAN PINDAHAN</h2>
                                        <p class=\"text-muted\">Harap melakukan konfirmasi pembayaran DP dengan memasukkan informasi transfer dengan benar.</p>
                                    </div>
                                </div>
                                <!-- /.row -->
                                <div class=\"row\">
                                    <form action=\"";
        // line 16
        echo twig_escape_filter($this->env, site_url("order/do_confirm"), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, (isset($context["order_id"]) ? $context["order_id"] : null), "html", null, true);
        echo "\" method=\"post\">
                                    <div class=\"col-sm-8 col-sm-offset-2\">
                                        <div class=\"table-responsive\">
                                            <table class=\"table\">
                                                <tr>
                                                    <td>Total Transfer (DP 50%)</th>
                                                    <th>Rp ";
        // line 22
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["dp"]) ? $context["dp"] : null), 0, "", "."), "html", null, true);
        echo ",-</th>
                                                </tr>
                                                <tr>
                                                    <td>Batas waktu</td>
                                                    <th>";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["max_pay_date"]) ? $context["max_pay_date"] : null), "html", null, true);
        echo "</th>
                                                </tr>
                                                <tr>
                                                    <td>Bank yang ditransfer</td>
                                                    <td>
                                                        <select name=\"bank_id\" class=\"form-control\" required=\"\">
                                                            ";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banks"]) ? $context["banks"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["bank"]) {
            // line 33
            echo "                                                            <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["bank"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["bank"], "bank_name", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["bank"], "account_no", array()), "html", null, true);
            echo "</option>
                                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['bank'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Bank</td>
                                                    <td>
                                                        <input type=\"text\" class=\"form-control\" name=\"bank_name\" style=\"width: 100%; text-align: left\" placeholder=\"contoh: BCA, Mandiri, BNI\" required=\"\">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Rekening</td>
                                                    <td>
                                                        <input type=\"text\" class=\"form-control\" name=\"account_name\" style=\"width: 100%; text-align: left\" placeholder=\"Nama rekening anda\" required=\"\">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah Dibayarkan</td>
                                                    <td>
                                                        <input type=\"text\" class=\"form-control\" name=\"amount\" style=\"width: 100%; text-align: left\" placeholder=\"Jumlah uang yang anda transfer\" value=\"";
        // line 53
        echo twig_escape_filter($this->env, (isset($context["dp"]) ? $context["dp"] : null), "html", null, true);
        echo "\" readonly=\"\">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Catatan / Keterangan</td>
                                                    <td>
                                                        <textarea class=\"form-control\" name=\"note\" style=\"width: 100%; text-align: left\"></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan=\"2\" class=\"text-center\">
                                                        <button type=\"submit\" class=\"btn btn-template-main\">Konfirmasi</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan=\"2\">Hubungi kontak kami jika anda mempunyai pertanyaan atau mengalami masalah saat melakukan pembayaran.</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    </form>
                                </div>

                            </div>

                            <div class=\"box-footer\">
                                <div class=\"pull-left\">
                                    <a href=\"";
        // line 80
        echo twig_escape_filter($this->env, site_url("order/order_list"), "html", null, true);
        echo "\" class=\"btn btn-main text-center\"><i class=\"fa fa-chevron-left\"></i> Kembali ke Daftar Pesanan</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->

";
    }

    // line 92
    public function block_scripts($context, array $blocks = array())
    {
        // line 93
        echo "<script type=\"text/javascript\">
    var temp = '";
        // line 94
        echo twig_escape_filter($this->env, (isset($context["max_pay_date"]) ? $context["max_pay_date"] : null), "html", null, true);
        echo "';
    var t = temp.split(' - ');
    var now = new Date();
    var max = new Date(t[0] + ' ' + t[1]);

    if(now > max){
        alert('Periode pembayaran konfirmasi telah melewati 24 jam, harap pesan ulang kembali.');
        location.href = '";
        // line 101
        echo twig_escape_filter($this->env, site_url("order/order_list"), "html", null, true);
        echo "';
    }
</script>
";
    }

    public function getTemplateName()
    {
        return "order/new_order/confirmation.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 101,  170 => 94,  167 => 93,  164 => 92,  149 => 80,  119 => 53,  99 => 35,  86 => 33,  82 => 32,  73 => 26,  66 => 22,  55 => 16,  40 => 3,  37 => 2,  11 => 1,);
    }
}
