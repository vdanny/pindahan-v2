<?php

/* partials/login_modal.html */
class __TwigTemplate_87a4d5a12207fbdf92172dab7a0801672508de78471ccd5d187d78bcd2299bde extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
        <!-- *** LOGIN MODAL ***
_________________________________________________________ -->

        <div class=\"modal fade\" id=\"login-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"Login\" aria-hidden=\"true\">
            <div class=\"modal-dialog modal-sm\">

                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                        <h4 class=\"modal-title text-center\" id=\"Login\">Customer login</h4>
                    </div>
                    <div class=\"modal-body\">
                        <form action=\"";
        // line 14
        echo twig_escape_filter($this->env, site_url("account/do_login"), "html", null, true);
        echo "\" method=\"post\">
                            <div class=\"form-group\">
                                <input type=\"text\" class=\"form-control\" id=\"email_modal\" placeholder=\"Email\" name=\"username\" required=\"\">
                            </div>
                            <div class=\"form-group\">
                                <input type=\"password\" class=\"form-control\" id=\"password_modal\" placeholder=\"Password\" name=\"password\" required=\"\">
                            </div>

                            <p class=\"text-center\">
                                <button class=\"btn btn-template-main\"><i class=\"fa fa-sign-in\"></i> Masuk</button>
                            </p>

                        </form>

                        <p class=\"text-center text-muted\">Belum terdaftar?</p>
                        <p class=\"text-center text-muted\"><a href=\"";
        // line 29
        echo twig_escape_filter($this->env, site_url("account/register"), "html", null, true);
        echo "\"><strong>Daftar sekarang</strong></a> cuma 1 menit!</p>

                    </div>
                </div>
            </div>
        </div>

        <!-- *** LOGIN MODAL END *** -->
";
    }

    public function getTemplateName()
    {
        return "partials/login_modal.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 29,  34 => 14,  19 => 1,);
    }
}
