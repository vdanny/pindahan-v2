<?php

/* home/login_modal.html */
class __TwigTemplate_3dc6f7b16a3f89e555bc26f26e07d8d9a46d2fdd326933affa48098c093a5531 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
        <!-- *** LOGIN MODAL ***
_________________________________________________________ -->

        <div class=\"modal fade\" id=\"login-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"Login\" aria-hidden=\"true\">
            <div class=\"modal-dialog modal-sm\">

                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                        <h4 class=\"modal-title text-center\" id=\"Login\">Customer login</h4>
                    </div>
                    <div class=\"modal-body\">
                        <form action=\"customer-orders.html\" method=\"post\">
                            <div class=\"form-group\">
                                <input type=\"text\" class=\"form-control\" id=\"email_modal\" placeholder=\"Email\">
                            </div>
                            <div class=\"form-group\">
                                <input type=\"password\" class=\"form-control\" id=\"password_modal\" placeholder=\"Password\">
                            </div>

                            <p class=\"text-center\">
                                <button class=\"btn btn-template-main\"><i class=\"fa fa-sign-in\"></i> Masuk</button>
                            </p>

                        </form>

                        <p class=\"text-center text-muted\">Belum terdaftar?</p>
                        <p class=\"text-center text-muted\"><a href=\"customer-register.html\"><strong>Daftar sekarang</strong></a> cuma 1 menit!</p>

                    </div>
                </div>
            </div>
        </div>

        <!-- *** LOGIN MODAL END *** -->
";
    }

    public function getTemplateName()
    {
        return "home/login_modal.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
