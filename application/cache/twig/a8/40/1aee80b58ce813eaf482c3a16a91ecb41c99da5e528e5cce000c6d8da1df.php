<?php

/* partials/scripts.html */
class __TwigTemplate_a8401aee80b58ce813eaf482c3a16a91ecb41c99da5e528e5cce000c6d8da1df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
    <!-- #### JAVASCRIPT FILES ### -->

    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js\"></script>
    <script>
        window.jQuery || document.write('<script src=\"js/jquery-1.11.0.min.js\"><\\/script>')
    </script>
    <script src=\"http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js\"></script>

    <script src=\"";
        // line 10
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/js/jquery.cookie.js\"></script>
    <script src=\"";
        // line 11
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/js/waypoints.min.js\"></script>
    <script src=\"";
        // line 12
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/js/jquery.counterup.min.js\"></script>
    <script src=\"";
        // line 13
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/js/jquery.parallax-1.1.3.js\"></script>
    <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/js/front.js\"></script>
    <script src=\"";
        // line 15
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/js/custom.js\"></script>

    <!-- JQuery Validation -->
    <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/js/jquery.validate/jquery.validate.min.js\"></script>
    <script src=\"";
        // line 19
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/js/jquery.validate/localization/messages_id.min.js\"></script>

    <!-- owl carousel -->
    <script src=\"";
        // line 22
        echo twig_escape_filter($this->env, asset_url(), "html", null, true);
        echo "/frontend/js/owl.carousel.min.js\"></script>
    <!-- Scroll to top -->
    <script type=\"text/javascript\" src=\"http://arrow.scrolltotop.com/arrow88.js\"></script>

    <!-- Bootstrap Datetime Picker -->
    <script type=\"text/javascript\" src=\"";
        // line 27
        echo twig_escape_filter($this->env, bower_url(), "html", null, true);
        echo "moment/min/moment.min.js\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 28
        echo twig_escape_filter($this->env, bower_url(), "html", null, true);
        echo "eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js\"></script>

";
    }

    public function getTemplateName()
    {
        return "partials/scripts.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 28,  74 => 27,  66 => 22,  60 => 19,  56 => 18,  50 => 15,  46 => 14,  42 => 13,  38 => 12,  34 => 11,  30 => 10,  19 => 1,);
    }
}
