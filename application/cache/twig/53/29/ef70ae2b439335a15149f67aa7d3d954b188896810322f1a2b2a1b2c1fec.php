<?php

/* account/profile.html */
class __TwigTemplate_5329ef70ae2b439335a15149f67aa7d3d954b188896810322f1a2b2a1b2c1fec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("masterpages/layout.html");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "masterpages/layout.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "                    <div class=\"col-md-9 clearfix\" id=\"customer-account\">
                        <div class=\"box\">
                            <div class=\"heading\">
                                <h3 class=\"text-uppercase\">Informasi profil</h3>
                            </div>

                            ";
        // line 9
        $context["error"] = $this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "flashdata", array()), "msg", array());
        // line 10
        echo "                            ";
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 11
            echo "                            <div class=\"alert alert-danger alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <strong>Oops! Terjadi kesalahan:</strong>
                                <ul>
                                ";
            // line 15
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["error"]) ? $context["error"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["e"]) {
                // line 16
                echo "                                    <li>";
                echo twig_escape_filter($this->env, $context["e"], "html", null, true);
                echo "</li>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['e'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "                                </ul>
                            </div>
                            ";
        }
        // line 21
        echo "
                            ";
        // line 22
        $context["success"] = $this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "flashdata", array()), "msg_success", array());
        // line 23
        echo "                            ";
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 24
            echo "                            <div class=\"alert alert-success alert-dismissible\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                                <strong>";
            // line 26
            echo twig_escape_filter($this->env, (isset($context["success"]) ? $context["success"] : null), "html", null, true);
            echo "</strong>
                            </div>
                            ";
        }
        // line 29
        echo "
                            <form action=\"";
        // line 30
        echo twig_escape_filter($this->env, site_url("account/update_profile"), "html", null, true);
        echo "\" method=\"post\">
                                <div class=\"row\">
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <label for=\"fullname\">Nama Lengkap</label>
                                            <input type=\"text\" class=\"form-control\" id=\"fullname\" name=\"fullname\" value=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "fullname", array()), "html", null, true);
        echo "\">
                                            <input type=\"hidden\" name=\"id\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "userdata", array()), "user", array()), "id", array()), "html", null, true);
        echo "\">
                                        </div>
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <label for=\"gender\">Jenis Kelamin</label>
                                            <input type=\"text\" class=\"form-control\" id=\"gender\" readonly value=\"";
        // line 44
        echo ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "gender", array()) == 1)) ? ("Pria") : ("Wanita"));
        echo "\">
                                        </div>
                                    </div>
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <label for=\"dob\">Tanggal Lahir</label>
                                            <input type=\"text\" class=\"form-control datetimepicker\" id=\"dob\" value=\"";
        // line 50
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "dob", array()), "m/d/Y"), "html", null, true);
        echo "\" disabled=\"\">
                                        </div>
                                    </div>
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <label for=\"email_account\">Email</label>
                                            <input type=\"text\" class=\"form-control\" id=\"email_account\" readonly value=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
        echo "\">
                                        </div>
                                    </div>
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <label for=\"phone\">Nomor HP</label>
                                            <input type=\"text\" class=\"form-control\" id=\"phone\" name=\"phone\" value=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "phone", array()), "html", null, true);
        echo "\">
                                        </div>
                                    </div>
                                    <div class=\"col-sm-12\">
                                        <div class=\"form-group\">
                                            <label for=\"address\">Alamat Lengkap</label>
                                            <textarea class=\"form-control\" id=\"address\" name=\"address\">";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "address", array()), "html", null, true);
        echo "</textarea>
                                        </div>
                                    </div>
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <label for=\"is_merchant\">Apakah anda seorang merchant?</label>
                                            <div class=\"radio\">
                                              <label><input type=\"radio\" name=\"is_merchant\" ";
        // line 75
        echo ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "is_merchant", array()) == 1)) ? ("checked") : (""));
        echo " value=\"1\">Ya</label>
                                            </div>
                                            <div class=\"radio\">
                                              <label><input type=\"radio\" name=\"is_merchant\" ";
        // line 78
        echo ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "is_merchant", array()) == 0)) ? ("checked") : (""));
        echo " value=\"0\">Tidak</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <label for=\"company_name\">Nama Toko</label>
                                            <input type=\"text\" class=\"form-control\" name=\"company_name\" id=\"company_name\" ";
        // line 85
        echo ((($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "is_merchant", array()) == 0)) ? ("disabled") : (""));
        echo " value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "company_name", array()), "html", null, true);
        echo "\">
                                        </div>
                                    </div>
                                    <div class=\"col-sm-12 text-center\">
                                        <button type=\"submit\" class=\"btn btn-template-main\"><i class=\"fa fa-save\"></i> Simpan perubahan</button>

                                    </div>

                                </div>

                            </form>

                        </div>
                        <div class=\"box clearfix\">

                            <div class=\"heading\">
                                <h3 class=\"text-uppercase\">Ubah password</h3>
                            </div>

                            <form action=\"";
        // line 104
        echo twig_escape_filter($this->env, site_url("account/change_password"), "html", null, true);
        echo "\" method=\"post\">
                                <div class=\"row\">
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <label for=\"password_old\">Password lama</label>
                                            <input type=\"password\" class=\"form-control\" id=\"password_old\" name=\"password-old\">
                                            <input type=\"hidden\" name=\"id\" value=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["session"]) ? $context["session"] : null), "userdata", array()), "user", array()), "id", array()), "html", null, true);
        echo "\">
                                        </div>
                                    </div>
                                </div>
                                <div class=\"row\">
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <label for=\"password_1\">Password baru</label>
                                            <input type=\"password\" class=\"form-control\" id=\"password_1\" name=\"password\">
                                        </div>
                                    </div>
                                    <div class=\"col-sm-6\">
                                        <div class=\"form-group\">
                                            <label for=\"password_2\">Ulangi password baru</label>
                                            <input type=\"password\" class=\"form-control\" id=\"password_2\" name=\"password-confirm\">
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class=\"text-center\">
                                    <button type=\"submit\" class=\"btn btn-template-main\"><i class=\"fa fa-save\"></i> Simpan password baru</button>
                                </div>
                            </form>

                        </div>
                        <!-- /.box -->


                        

                    </div>
                    <!-- /.col-md-9 -->
";
    }

    // line 144
    public function block_scripts($context, array $blocks = array())
    {
        // line 145
        echo "<script type=\"text/javascript\">
    var company = \"";
        // line 146
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "company_name", array()), "html", null, true);
        echo "\";
    \$(\"input[type=radio]\").change(function(){
        var \$this = \$(this);
        if(\$this.val() == 'y'){
            \$('#merchant_name').removeAttr('disabled');
            \$('#merchant_name').val(company);
        }
        else{
            \$('#merchant_name').val('');
            \$('#merchant_name').attr('disabled', 'disabled');
        }
    });

    \$(function () {
        \$('.datetimepicker').datetimepicker({
            format: 'L',
            defaultDate: \"";
        // line 162
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "dob", array()), "m/d/Y"), "html", null, true);
        echo "\",
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "account/profile.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  279 => 162,  260 => 146,  257 => 145,  254 => 144,  216 => 110,  207 => 104,  183 => 85,  173 => 78,  167 => 75,  157 => 68,  148 => 62,  139 => 56,  130 => 50,  121 => 44,  110 => 36,  106 => 35,  98 => 30,  95 => 29,  89 => 26,  85 => 24,  82 => 23,  80 => 22,  77 => 21,  72 => 18,  63 => 16,  59 => 15,  53 => 11,  50 => 10,  48 => 9,  40 => 3,  37 => 2,  11 => 1,);
    }
}
