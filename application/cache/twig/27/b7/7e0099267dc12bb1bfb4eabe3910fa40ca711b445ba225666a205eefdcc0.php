<?php

/* layout.html */
class __TwigTemplate_27b77e0099267dc12bb1bfb4eabe3910fa40ca711b445ba225666a205eefdcc0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"utf-8\">
    <meta name=\"robots\" content=\"all,follow\">
    <meta name=\"googlebot\" content=\"index,follow,snippet,archive\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

    <title>Pindahan | Solusi Pindahan Mudah</title>

    <meta name=\"keywords\" content=\"\">
    ";
        // line 13
        echo twig_include($this->env, $context, "partials/styles.html");
        echo "
</head>

<body>


    <div id=\"all\">
        <header>

            <!-- *** TOP ***
_________________________________________________________ -->
            <div id=\"top\">
                <div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-xs-5 contact\">
                            <p class=\"hidden-sm hidden-xs\">Hubungi kami: (021)-5512345 atau hello@pindahan.com</p>
                            <p class=\"hidden-md hidden-lg\"><a href=\"#\" data-animate-hover=\"pulse\"><i class=\"fa fa-phone\"></i></a>  <a href=\"#\" data-animate-hover=\"pulse\"><i class=\"fa fa-envelope\"></i></a>
                            </p>
                        </div>
                        <div class=\"col-xs-7\">
                            <!-- <div class=\"social\">
                                <a href=\"#\" class=\"external facebook\" data-animate-hover=\"pulse\"><i class=\"fa fa-facebook\"></i></a>
                                <a href=\"#\" class=\"external gplus\" data-animate-hover=\"pulse\"><i class=\"fa fa-google-plus\"></i></a>
                                <a href=\"#\" class=\"external twitter\" data-animate-hover=\"pulse\"><i class=\"fa fa-twitter\"></i></a>
                                <a href=\"#\" class=\"email\" data-animate-hover=\"pulse\"><i class=\"fa fa-envelope\"></i></a>
                            </div> -->

                        </div>
                    </div>
                </div>
            </div>

            <!-- *** TOP END *** -->

            <!-- *** NAVBAR ***
    _________________________________________________________ -->

            <div class=\"navbar-affixed-top\" data-spy=\"affix\" data-offset-top=\"200\">

                <div class=\"navbar navbar-default yamm\" role=\"navigation\" id=\"navbar\">

                    <div class=\"container\">
                        <div class=\"navbar-header\">

                            <a class=\"navbar-brand home\" href=\"";
        // line 57
        echo twig_escape_filter($this->env, site_url(""), "html", null, true);
        echo "\" style=\"padding-top: 20px; color: #467FBF; font-size: 35px;\">
                                <!-- <img src=\"img/logo.png\" alt=\"Universal logo\" class=\"hidden-xs hidden-sm\">
                                <img src=\"img/logo-small.png\" alt=\"Universal logo\" class=\"visible-xs visible-sm\"><span class=\"sr-only\">Pindahan - go to homepage</span> -->
                                Pindahan
                            </a>
                            <div class=\"navbar-buttons\">
                                <button type=\"button\" class=\"navbar-toggle btn-template-main\" data-toggle=\"collapse\" data-target=\"#navigation\">
                                    <span class=\"sr-only\">Toggle navigation</span>
                                    <i class=\"fa fa-align-justify\"></i>
                                </button>
                            </div>
                        </div>
                        <!--/.navbar-header -->

                        <div class=\"navbar-collapse collapse\" id=\"navigation\">

                            <ul class=\"nav navbar-nav navbar-right\">
                                <li class=\"active\"><a href=\"";
        // line 74
        echo twig_escape_filter($this->env, site_url("home/index"), "html", null, true);
        echo "#home\">Home</a></li>
                                <li><a href=\"";
        // line 75
        echo twig_escape_filter($this->env, site_url("home/index"), "html", null, true);
        echo "#service\">Layanan</a></li>
                                <li><a href=\"";
        // line 76
        echo twig_escape_filter($this->env, site_url("home/index"), "html", null, true);
        echo "#price\">Harga</a></li>
                                <li><a href=\"";
        // line 77
        echo twig_escape_filter($this->env, site_url("home/index"), "html", null, true);
        echo "#custom\">Custom</a></li>
                                <li><a href=\"";
        // line 78
        echo twig_escape_filter($this->env, site_url("home/index"), "html", null, true);
        echo "#testimonial\">Testimonial</a></li>
                                <li class=\"dropdown\">
                                    <a href=\"javascript: void(0)\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Kontak <b class=\"caret\"></b></a>
                                    <ul class=\"dropdown-menu\">
                                        <li><a href=\"#\"><i class=\"fa fa-phone\"></i>(021)-5512345</a>
                                        </li>
                                        <li><a href=\"#\"><i class=\"fa fa-envelope\"></i>hello@pindahan.com</a>
                                        </li>

                                    </ul>
                                </li>
                            </ul>

                        </div>
                        <!--/.nav-collapse -->



                        <div class=\"collapse clearfix\" id=\"search\">

                            <form class=\"navbar-form\" role=\"search\">
                                <div class=\"input-group\">
                                    <input type=\"text\" class=\"form-control\" placeholder=\"Search\">
                                    <span class=\"input-group-btn\">

                    <button type=\"submit\" class=\"btn btn-template-main\"><i class=\"fa fa-search\"></i></button>

                </span>
                                </div>
                            </form>

                        </div>
                        <!--/.nav-collapse -->

                    </div>


                </div>
                <!-- /#navbar -->

            </div>

            <!-- *** NAVBAR END *** -->

        </header>

        <div id=\"heading-breadcrumbs\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-md-7\">
                        <h1>";
        // line 128
        echo twig_escape_filter($this->env, (isset($context["title_page"]) ? $context["title_page"] : null), "html", null, true);
        echo "</h1>
                    </div>
                    <div class=\"col-md-5\">
                        <ul class=\"breadcrumb\">

                            <li><a href=\"";
        // line 133
        echo twig_escape_filter($this->env, site_url("home/index"), "html", null, true);
        echo "\">Home</a>
                            </li>
                            <li>";
        // line 135
        echo twig_escape_filter($this->env, (isset($context["title_page"]) ? $context["title_page"] : null), "html", null, true);
        echo "</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div id=\"content\" class=\"clearfix\">

            <div class=\"container\">

                <div class=\"row\">


                    ";
        // line 149
        if ((isset($context["has_menu"]) ? $context["has_menu"] : null)) {
            // line 150
            echo "                    <!-- *** LEFT COLUMN ***
             _________________________________________________________ -->

                    <div class=\"col-md-3\">
                        <!-- *** CUSTOMER MENU ***
 _________________________________________________________ -->
                        <div class=\"panel panel-default sidebar-menu\">

                            <div class=\"panel-heading\">
                                <h3 class=\"panel-title\">Menu</h3>
                            </div>

                            <div class=\"panel-body\">

                                <ul class=\"nav nav-pills nav-stacked\">
                                    <li class=\"active\">
                                        <a href=\"";
            // line 166
            echo twig_escape_filter($this->env, site_url("/order/new_order"), "html", null, true);
            echo "\"><i class=\"fa fa-plus\"></i> Pesan pindahan</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 169
            echo twig_escape_filter($this->env, site_url("/order/order_list"), "html", null, true);
            echo "\"><i class=\"fa fa-list\"></i> Pesanan saya</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 172
            echo twig_escape_filter($this->env, site_url("/account/profile"), "html", null, true);
            echo "\"><i class=\"fa fa-user\"></i> Akun saya</a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 175
            echo twig_escape_filter($this->env, site_url("/account/logout"), "html", null, true);
            echo "\"><i class=\"fa fa-sign-out\"></i> Keluar</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                    </div>

                    <!-- *** LEFT COLUMN END *** -->
                ";
        }
        // line 188
        echo "                    <!-- *** RIGHT COLUMN ***
                
\t\t\t _________________________________________________________ -->
                    ";
        // line 191
        $this->displayBlock('body', $context, $blocks);
        // line 192
        echo "                    <!-- *** RIGHT COLUMN END *** -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

        ";
        // line 202
        echo twig_include($this->env, $context, "partials/footer.html");
        echo "

    </div>
    <!-- /#all -->


    ";
        // line 208
        echo twig_include($this->env, $context, "partials/scripts.html");
        echo "
    ";
        // line 209
        $this->displayBlock('scripts', $context, $blocks);
        // line 210
        echo "</body>

</html>";
    }

    // line 191
    public function block_body($context, array $blocks = array())
    {
    }

    // line 209
    public function block_scripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  300 => 209,  295 => 191,  289 => 210,  287 => 209,  283 => 208,  274 => 202,  262 => 192,  260 => 191,  255 => 188,  239 => 175,  233 => 172,  227 => 169,  221 => 166,  203 => 150,  201 => 149,  184 => 135,  179 => 133,  171 => 128,  118 => 78,  114 => 77,  110 => 76,  106 => 75,  102 => 74,  82 => 57,  35 => 13,  21 => 1,);
    }
}
