<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('asset_url()'))
{
	function asset_url()
	{
		return base_url().'assets/';
	}
}

if ( ! function_exists('bower_url()'))
{
	function bower_url()
	{
		return base_url().'bower_components/';
	}
}

if ( ! function_exists('upload_url()'))
{
	function upload_url($path = '')
	{
		return base_url().'uploads/' . $path;
	}
}