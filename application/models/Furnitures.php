<?php 

class Furnitures extends CI_Model
{
	private $table = 'furnitures';
	private $active = 'deleted_at IS NULL';

	public function __construct()
	{
		parent::__construct();
	}

	public function find_id($id){
		if(gettype($id) == 'array'){
			$this->db->where_in('id', $id);
		}
		else if(gettype($id) == 'integer'){
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function all($columns = '')
	{
		if($columns != ''){
			$this->db->select($columns);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	///
	/// BACKEND METHODS
	///
	function get($id = 0) 
	{
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function create($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table, $data);
	}

	function update($data)
	{
		return $this->db->update($this->table, $data, array('id' => $data['id']));
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}
}


?>