<?php 

class Orders extends CI_Model
{
	private $table = 'orders';
	private $tbl_custom = 'order_customs';
	private $tbl_extra = 'order_extras';
	private $tbl_furniture = 'order_furnitures';
	private $active = 'deleted_at IS NULL';

	public function __construct()
	{
		parent::__construct();
	}

	public function find_id($id)
	{
		if(gettype($id) == 'array'){
			$this->db->where_in('id', $id);
		}
		else if(gettype($id) == 'integer' || gettype($id) == 'string'){
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function find_userid($id, $order='')
	{
		$this->db->where('user_id', $id);
		$this->db->where($this->active);
		$this->db->order_by($order);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function get_customs($order_id)
	{
		$query = $this->db->query("SELECT a.*, b.name, b.volume, b.price, b.is_vehicle FROM $this->tbl_custom a JOIN customs b ON b.id = a.custom_id WHERE order_id = $order_id AND a.deleted_at IS NULL");

		return $query->result();
	}

	public function get_extras($order_id)
	{
		$query = $this->db->query("SELECT a.*, b.name, b.price FROM $this->tbl_extra a JOIN extras b ON b.id = a.extra_id WHERE order_id = $order_id AND a.deleted_at IS NULL");

		return $query->result();
	}

	public function get_furnitures($order_id)
	{
		$q = "SELECT a.*, b.name, c.name as size, c.length, c.width, c.height FROM $this->tbl_furniture a JOIN furnitures b ON b.id = a.furniture_id JOIN sizes c ON c.id = a.size_id WHERE order_id = $order_id AND a.deleted_at IS NULL";
		$query = $this->db->query($q);
		return $query->result();
	}

	public function save($order)
	{
		$data['user_id'] = $this->session->userdata('user')->id;
		$data['fullname'] = $order['fullname'];
		$data['phone'] = $order['phone'];
		
		//move datetime
		$data['move_datetime'] = date('Y-m-d H:i:s', strtotime($order['move_date'] . ' ' . $order['move_time']));

		$data['distance']       = $order['distance'];
		$data['duration']       = $order['duration']; #todo
		$data['start_address']  = $order['start_address'];
		$data['end_address']    = $order['end_address'];
		$data['start_location'] = $order['start_location'];
		$data['end_location']   = $order['end_location'];
		$data['package_id']     = $order['package_id'];
		$data['price']          = $order['price']; #todo
		$data['blueprint']      = isset($order['blueprint'])? $order['blueprint']['file_name'] : '';
		$data['note']     		= $order['notes'];
		$data['payment_status'] = 0;
		$this->db->insert($this->table, $data);

		$this->db->select('id');
		$this->db->where($this->active);
		$this->db->order_by('created_at', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get($this->table);
		$order_id = $query->row()->id;

		// if using custom
		if($order['package_id'] == 0){
			// insert to order_customs
			for ($i=0; $i < count($order['custom_id']); $i++) { 
				$custom_id = $order['custom_id'][$i];
				$custom_qty = $order['custom'][$i];
				if ($custom_qty == 0) continue;

				$det = array(
					"order_id"  => $order_id,
					"custom_id" => $custom_id,
					"qty"       => $custom_qty
				);
				$this->db->insert($this->tbl_custom, $det);
			}

			// insert to order_extras
			for ($i=0; $i < count($order['extra_id']); $i++) { 
				$extra_id = $order['extra_id'][$i];
				$extra_qty = $order['extra'][$i];
				if ($extra_qty == 0) continue;

				$extra = array(
					"order_id" => $order_id,
					"extra_id" => $extra_id,
					"qty"      => $extra_qty
				);
				$this->db->insert($this->tbl_extra, $extra);
			}
		}


		// insert to order_furnitures
		if(isset($order['appliances_id'])){
			for ($i=0; $i < count($order['appliances_id']); $i++) { 
				$appliances_id = $order['appliances_id'][$i];
				$sizes_id = $order['sizes_id'][$i];
				$appliances_qty = $order['appliances'][$i];
				$description = $order['description'][$i];
				if ($appliances_qty == 0) continue;

				$furn = array(
					"order_id"     => $order_id,
					"furniture_id" => $appliances_id,
					"size_id"      => $sizes_id,
					"qty"          => $appliances_qty,
					"description"  => $description
				);
				$this->db->insert($this->tbl_furniture, $furn);
			}
		}

		return $order_id;
	}

	function update_status($id, $status)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, array('payment_status' => $status));
	}

	function update_review_status($id, $status)
	{
		$this->db->where('id', $id);
		$this->db->update($this->table, array('has_reviewed' => $status));
	}

	function is_available_time($start, $interval = 7)
	{
		$end = $start + ($interval * 60 * 60) - 60 * 60;

		if($start > strtotime(date('Y-m-d ', $start) . '20:00:00') || $start < strtotime(date('Y-m-d ', $start) . '07:00:00')) return false;

		$sql = 'SELECT * FROM '.$this->table.' WHERE \''.
			date('Y-m-d H:i:s', $start).'\' BETWEEN move_datetime AND DATE_ADD(move_datetime, INTERVAL '.$interval.' HOUR) OR \''.
			date('Y-m-d H:i:s', $end).'\' BETWEEN move_datetime AND DATE_ADD(move_datetime, INTERVAL '.$interval.' HOUR) AND '.$this->active;
		$query = $this->db->query($sql);
		return count($query->result()) == 0;
	}

	///
	/// BACKEND METHODS
	///
	function get($id = 0) 
	{
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function create($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table, $data);
	}

	function create_furniture($data)
	{
		return $this->db->insert($this->tbl_furniture, $data);
	}

	function delete_furniture($o, $f)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where(array('id' => $f));
		return $this->db->update($this->tbl_furniture);
	}

	function update_custom($order_id, $custom_id, $qty)
	{
		$this->db->set('qty', $qty);
		$this->db->where(array('order_id' => $order_id, 'custom_id' => $custom_id));
		return $this->db->update($this->tbl_custom);
	}

	function update_extra($order_id, $extra_id, $qty)
	{
		$this->db->set('qty', $qty);
		$this->db->where(array('order_id' => $order_id, 'extra_id' => $extra_id));
		return $this->db->update($this->tbl_extra);
	}

	function create_schedule($data)
	{
		return $this->db->insert('schedules', $data);
	}

	function get_worker_amount($order_id, $package_id)
	{
		if($package_id == 0)
		{
			$worker = 0;
			$customs = $this->get_customs($order_id);
			foreach ($customs as $custom) {
				if(!$custom->is_vehicle){
					$worker += $custom->qty;
				}
			}
			return $worker;
		}
		else
		{
			$package = $this->packages->find(array('id' => $package_id))[0];
			return $package->employee;
		}
	}

	function get_workers($order_id)
	{
		$q = "SELECT a.order_id, a.worker_id, b.fullname FROM schedules a JOIN administrators b ON b.id = a.worker_id WHERE a.order_id = $order_id";
		$query = $this->db->query($q);
		return $query->result();
	}

	function update($data)
	{
		return $this->db->update($this->table, $data, array('id' => $data['id']));
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}

	function get_report($start, $end)
	{
		$q = "SELECT * FROM orders WHERE move_datetime BETWEEN '$start' and '$end' AND payment_status = 3 AND deleted_at IS NULL";
		$query = $this->db->query($q);
		return $query->result();
	}
}


?>