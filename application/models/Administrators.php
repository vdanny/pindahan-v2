<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Administrators extends CI_Model {

	private $table = 'administrators';
	private $active = 'deleted_at IS NULL';

	function __construct()
	{
		parent::__construct();
	}

	function login($username, $password) 
	{
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	function get($id = 0) 
	{
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function get_available_worker($starttime, $endtime) 
	{
		$q = "SELECT *
		FROM administrators a
		WHERE a.id NOT IN(
			SELECT b.worker_id
		    FROM schedules b
		    JOIN orders c ON c.id = b.order_id
		    WHERE c.move_datetime BETWEEN '$starttime' AND '$endtime'
		    AND c.deleted_at IS NULL
		)
		AND a.is_super = 0
		AND a.deleted_at IS NULL";
		
		$query = $this->db->query($q);
		return $query->result();
	}

	function create($data)
	{
		return $this->db->insert($this->table, $data);
	}

	function update($data)
	{
		$this->db->set('username', $data['username']);
		$this->db->set('fullname', $data['fullname']);
		if(isset($data['password']) && $data['password'] != '') $this->db->set('password', $data['password']);
		$this->db->set('is_super', isset($data['is_super'])? $data['is_super'] : 0);
		$this->db->where('id', $data['id']);
		return $this->db->update($this->table);
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}
}