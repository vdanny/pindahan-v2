<?php 

class Vehicles extends CI_Model
{
	private $table = 'customs';
	private $vehicle = 'is_vehicle = 1';
	private $active = 'deleted_at IS NULL';

	public function __construct()
	{
		parent::__construct();
	}

	public function find($id, $columns = '')
	{
		if($columns != ''){
			$this->db->select($columns);
		}
		if($id != 0){
			$this->db->where('id', $id);
		}
		$this->db->where($this->vehicle);
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id != 0? $query->row() : $query->result();
	}
}


?>