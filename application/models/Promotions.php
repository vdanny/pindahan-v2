<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Promotions extends CI_Model {

	private $table = 'promotions';
	private $fields = 'id, image_path, created_at, updated_at';

	function __construct()
	{
		parent::__construct();
	}

	function get($id = 0) 
	{
		$this->db->select($this->fields);
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function create($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table, $data);
	}

	function update($data)
	{
		$this->db->set('image_path', $data['image_path']);
		$this->db->set('modified_by', $data['modified_by']);
		$this->db->where('id', $data['id']);
		return $this->db->update($this->table);
	}

	function delete($id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($this->table);
	}

}