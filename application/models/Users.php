<?php 
class Users extends CI_Model
{
	private $table = 'users';
	private $active = 'deleted_at IS NULL';

	public function __construct()
	{
		parent::__construct();
	}

	public function find($where = array(), $columns = '')
	{
		if($columns != ''){
			$this->db->select($columns);
		}

		$this->db->where($this->active);
		if(count($where) == 0){
			$query = $this->db->get($this->table);
		}
		else{
			$query = $this->db->get_where($this->table, $where);
		}
		return $query->row();
	}

	public function register()
	{
		$fields = array(
			"fullname"   => $this->input->post('fullname'),
			"email"      => $this->input->post('email'),
			"password"   => md5($this->input->post('password')),
			"gender"     => $this->input->post('gender'),
			"dob"        => date('Y-m-d H:i:s', strtotime($this->input->post('dob'))),
			"created_at" => date('Y-m-d H:i:s')
		);
		$this->db->insert($this->table, $fields);
	}

	public function update()
	{
		$fields = array(
			"fullname"     => $this->input->post('fullname'),
			"phone"        => $this->input->post('phone'),
			"address"      => $this->input->post('address'),
			"is_merchant"  => $this->input->post('is_merchant'),
			"company_name" => $this->input->post('company_name')
		);
		$this->db->update($this->table, $fields, array('id' => $this->input->post('id')));
	}

	public function email_confirm($user_id)
	{
		$fields = array(
			"email_confirm" => 1
		);
		$this->db->update($this->table, $fields, array('id' => $user_id));
	}

	public function change_password()
	{
		$fields = array(
			"password" => md5($this->input->post('password'))
		);
		$this->db->update($this->table, $fields, array('id' => $this->input->post('id')));
	}

	public function login($email, $password) 
	{
		$this->db->where('email', $email);
		$this->db->where('password', md5($password));
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $query->row();
	}

	///
	/// BACKEND FUNCTIONS
	///
	function get($id = 0) 
	{
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function create($data)
	{
		$data['password'] = md5($data['password']);
		$data['dob'] = date('Y-m-d', strtotime($data['dob']));
		if(isset($data['is_merchant']) == false) $data['is_merchant'] = 0;
		if($data['is_merchant'] == 0) $data['company_name'] = '';

		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table, $data);
	}

	function update_be($data)
	{
		$data['password'] = md5($data['password']);
		$data['dob'] = date('Y-m-d', strtotime($data['dob']));
		if(isset($data['is_merchant']) == false) $data['is_merchant'] = 0;
		if($data['is_merchant'] == 0) $data['company_name'] = '';

		return $this->db->update($this->table, $data, array('id' => $data['id']));
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}
}

?>