<?php 

class Cancellations extends CI_Model
{
	private $table = 'order_cancellations';
	private $active = 'deleted_at IS NULL';

	public function __construct()
	{
		parent::__construct();
	}

	public function create($order_id, $reason)
	{
		$data = array(
			"order_id" => $order_id,
			"reason"   =>$reason
		);
		$this->db->insert($this->table, $data);
	}

	public function get($order_id)
	{
		$this->db->where('order_id', $order_id);
		$query = $this->db->get($this->table);
		return $query->row();
	}

}


?>