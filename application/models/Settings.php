<?php 

class Settings extends CI_Model
{
	private $table = 'settings';
	private $active = 'deleted_at IS NULL';

	public function __construct()
	{
		parent::__construct();
	}

	public function all($columns = '')
	{
		if($columns != ''){
			$this->db->select($columns);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function get($name)
	{
		$this->db->where($this->active);
		$this->db->where('name', $name);
		$query = $this->db->get($this->table);
		return $query->row()->value;
	}

	public function update($key, $value)
	{
		$this->db->where('name', $key);
		$this->db->set('value', $value);
		return $this->db->update($this->table);
	}
}


?>