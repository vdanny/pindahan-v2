<?php 

class Testimonials extends CI_Model
{
	private $table = 'testimonials';
	private $active = 'deleted_at IS NULL';

	public function __construct()
	{
		parent::__construct();
	}

	public function create($order_id, $rate, $comment)
	{
		$data = array(
			"order_id" => $order_id,
			"rate"     => $rate,
			"comment"  => $comment
		);
		$this->db->insert($this->table, $data);
	}

	function show_home()
	{
		$this->db->where('is_show', 1);
		$this->db->where($this->active);
		$result = $this->db->get($this->table)->result();
		foreach ($result as $row) {
			$order = $this->orders->get($row->order_id);
			$row->user = $this->users->get($order->user_id);
		}

		return $result;
	}

	///
	/// BACKEND METHODS
	///
	function get($id = 0) 
	{
		if($id != 0) 
		{
			$this->db->where('id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}

	function update($data)
	{
		return $this->db->update($this->table, $data, array('id' => $data['id']));
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}

}


?>