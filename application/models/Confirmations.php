<?php 

class Confirmations extends CI_Model
{
	private $table = 'confirmations';
	private $active = 'deleted_at IS NULL';

	public function __construct()
	{
		parent::__construct();
	}

	public function save($data)
	{
		$d = array(
			"order_id"     => $data['order_id'],
			"bank_id"      => $data['bank_id'],
			"bank_name"    => $data['bank_name'],
			"account_name" => $data['account_name'],
			"amount"       => $data['amount'],
			"note"         => $data['note'],
		);
		$this->db->insert($this->table, $d);
	}

	function get($id = 0) 
	{
		if($id != 0) 
		{
			$this->db->where('order_id', $id);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		return $id == 0 ? $query->result() : $query->result()[0];
	}
}


?>