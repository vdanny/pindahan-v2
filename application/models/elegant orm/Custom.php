<?php

use Elegant\Model as Timex;

class Custom extends Timex {
	protected $table = "customs";

	function setAttrUpdatedAt($value)
	{
		return date('Y-m-d H:i:s');
	}

}