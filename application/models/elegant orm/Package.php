<?php

use Elegant\Model as Timex;

class Package extends Timex {
	protected $table = "packages";

	function setAttrUpdatedAt($value)
	{
		return date('Y-m-d H:i:s');
	}

}