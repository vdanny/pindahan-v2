<?php

use Elegant\Model as Timex;

class Size extends Timex {
	protected $table = "sizes";

	function setAttrUpdatedAt($value)
	{
		return date('Y-m-d H:i:s');
	}

}