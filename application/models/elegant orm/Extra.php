<?php

use Elegant\Model as Timex;

class Extra extends Timex {
	protected $table = "extras";

	function setAttrUpdatedAt($value)
	{
		return date('Y-m-d H:i:s');
	}

}