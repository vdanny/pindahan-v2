<?php

use Elegant\Model as Timex;

class User extends Timex {
	protected $table = "users";

	function setAttrUpdatedAt($value)
	{
		return date('Y-m-d H:i:s');
	}

	function dob_str()
	{
		return date('d-m-Y', $this->dob);
	}
}