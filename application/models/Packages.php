<?php 

class Packages extends CI_Model
{
	private $table = 'packages';
	private $active = 'deleted_at IS NULL';

	public function __construct()
	{
		parent::__construct();
	}

	public function all($columns = '')
	{
		if($columns != ''){
			$this->db->select($columns);
		}
		$this->db->where($this->active);
		$query = $this->db->get($this->table);
		$result = array();
		foreach($query->result() as $row)
		{
			$row->vehicle = $this->vehicle($row->transportation_id);
			array_push($result, $row);
		}
		return $result;
	}

	public function find($where = array(), $columns = '')
	{
		if($columns != ''){
			$this->db->select($columns);
		}

		$this->db->where($this->active);
		$this->db->where($where);
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function vehicle($id)
	{
		$this->db->where('is_vehicle', 1);
		$this->db->where('id', $id);
		$this->db->where($this->active);
		$query = $this->db->get('customs');
		return $query->row();
	}

	public function get_volume($package_id)
	{
		$package = $this->find(array('id' => $package_id));
		$vehicle = $this->vehicle($package->transportation_id);
		return $vehicle->volume;
	}

	///
	/// BACKEND METHODS
	///
	function create($data)
	{
		$data['created_at'] = date('Y-m-d H:i:s');
		return $this->db->insert($this->table, $data);
	}

	function update($data)
	{
		return $this->db->update($this->table, $data, array('id' => $data['id']));
	}

	function delete($id)
	{
		$this->db->set('deleted_at', date('Y-m-d H:i:s'));
		$this->db->where('id', $id);
		return $this->db->update($this->table);
	}
}


?>